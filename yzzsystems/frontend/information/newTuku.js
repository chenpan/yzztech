﻿/**
 * Created by tao on 2015/6/4.
 */
$(function(){
    function checkEleNum(ele){
        var eleArr = ele.find(".menu-li");
        var length = eleArr.length;
        ele.find(".menu-li-more").removeClass('down').children(".menu-a-more").html("查看全部");
        if(length>16){
            eleArr.hide().slice(0,15).show();
            ele.find(".menu-li-more").show();
        }else{
            ele.find(".menu-li-more").hide();
        }
    }
    checkEleNum($('.nav-show-menu').eq(0));
    $(".menu-li-more").click(function(){
        if($(this).hasClass("down")){
            checkEleNum($(this).parents('.nav-show-menu'));
            $(this).removeClass('down').children(".menu-a-more").html("查看全部");
        }else{
            $(this).addClass("down").siblings(".menu-li").show();
            $(this).children(".menu-a-more").html("收起");
        }
    });
    $(".menu-a").click(function(){
        var href = $(this).attr('href');
        var timer = null;
        $(this).parent().addClass("cur").siblings().removeClass('cur');
        timer = setTimeout(function(){
            //locationSkipComFn(href);
            window.location.href=href;
            clearTimeout();
            timer=null;
        },10);
        return false;
    });
    $(".tuku-images-nav .nav-li").click(function(){
      $(".tuku-images-nav").css({"z-index":12});
        var index = $(".tuku-images-nav .nav-li").index(this);
        $(this).siblings().removeClass("cur");
        var ele = $(this).parent(".nav-ul").siblings(".nav-show-menu");
        ele.eq(index).siblings(".nav-show-menu").css('height',0);
        checkEleNum(ele.eq(index));
        if(ele.eq(index).height()===0){
            $(this).addClass("cur");
            $(".popmask").css({'z-index':9,'position':'fixed'}).show();
            $(".header").css('z-index',10);
            ele.eq(index).css({'height':'auto'});
        }else{
            height = 0;
            $(this).removeClass("cur");
            $(".popmask").hide();
            ele.eq(index).css({'height':0});
        }
    });
    if($(".tuku-images-list").length>0){
 var headerHeight =$(".tuku-images-nav").length>0?$(".tuku-images-nav").offset().top:0;
 
        $(window).on("scroll",function(){
            var scrollTop = $(this).scrollTop();
            var fixedtop;
            if($(".tuku-tag-top").length>0){
                fixedtop=$(".settled-header").height();
            }else{
                fixedtop=0;
            }
            if(scrollTop>=headerHeight){
                $(".tuku-images-nav").css({'position':'fixed','top':fixedtop});
            }else{
                $(".tuku-images-nav").css({'position':'absolute','top':fixedtop});
            }
            var winHeight = $(this).height();
            var bodyHeight = $('body').height();
            if(scrollTop + winHeight >= bodyHeight){
                //$(".tuku-loading").show();
                //$.ajax({
                //    url:'//m.jia.com',
                //    type:'get',
                //    dateType:'json',
                //    success:function(msg){//请求的数据分成2部分放入2个容器内
                //        $(".tuku-loading").hide();
                //        $('.list-ul').eq(0).append(msg[0]);
                //        $('.list-ul').eq(1).append(msg[1]);
                //    },
                //    error:function(){
                //        $(".tuku-loading").hide();
                //        showMaskEdit({
                //            text:"请求失败",
                //            duration:2000
                //        });
                //    }
                //});
            }
        });
    }

    $(".popmask,.popup-price .close").click(function(){
        $(".tuku-images-nav .nav-li").removeClass("cur");
        $(".nav-show-menu").css('height',0);
        $(".popup-price").hide();
        $('.popmask,.popup-common').hide();
    });
	$(".tk-search-input,.tk-select").focus(function(){
	$(this).parents(".tk-search-box").addClass("cur");
    });	
	$(".tk-search-input,.tk-select").blur(function(){
	$(this).parents(".tk-search-box").removeClass("cur");
    });	
     $(".wap_check").click(function(){
        $(".tuku-images-nav").css({"z-index":8});
     
      });
	
    //图库详情
    if($('#anliImgBox').length>0){
        new screenSwipe(document.querySelector("#anliImgBox"), {
            speed: 300,
            auto: 0,
            callback: function (e,index,ele) {
                $(".tuku-more-header .imgNum-start").html(index + 1);
            }
        });
        //$('.tuku-more-footer .foot-btn').click(function(){
        //    $(".popup-price").show();
        //    $(".popmask").show();
        //});
        //function checkForm(){
        //    var nameValue = $('[name=phone-name]').val();
        //    var phoneValue = $("[name=phone-number]").val();
        //    var codeValue = $('[name=phone-code]').val();
        //    var phoneReg = /^1[3|4|5|7|8]\d{9}$/;
        //    if(nameValue==""){
        //        showMaskEdit({
        //            text:"请输入姓名",
        //            duration:2000
        //        });
        //    }else if (!phoneReg.test(phoneValue)){
        //        showMaskEdit({
        //            text:"请输入正确的手机号码",
        //            duration:2000
        //        });
        //    }else if(!/^\d{4,10}$/.test(codeValue)){
        //        showMaskEdit({
        //            text:"请输入正确的验证码",
        //            duration:2000
        //        });
        //    }else{
        //        $(".tuku-form").submit();
        //    }
        //}
        //$(".get-price").click(function(){
        //    checkForm();
        //});
    }
    $(".backtotop_new").tap(function(){
        window.scrollTo(0,0);
    });
    $(".house-ul li").click(function(){
	 	if(!$(this).hasClass("cur")){
	 	$(this).addClass("cur");
	 	}else{
	 		$(this).removeClass("cur");
	 	}	 	
	});
	//点击提交，至少填一项
	$("#design_btn").click(function(){
	if($(".input-box1").val()==""&&$(".tk-select").find("option:selected").val()=="0"&&(!$(".house-ul li").hasClass("cur"))){		 	
	         $(".popup-maskEdit").text("至少填写一项才能提交哦").show();
	         setTimeout('$(".popup-maskEdit").hide()',2000);           
	        }
		});
		//点击报名
	$("#tg_list .tg_product_dl").click(function(){
		showBm_new();
	});
		//取消报名
	$(document).on("click", ".popmask, .prd_submit[data-type='cancel']",function(){
	    $("#popupMain_prd, #popupMain_bm, .popmask").hide();
	});
	
});
//倒计时
function timing()
{
    $(".timer").each(function(){
        var endtime = new Date($(this).attr("rel")).getTime();//取结束日期(毫秒值)
        var endtime = $(this).attr("end");//取结束日期(毫秒值)
        var nowtime = new Date(); //今天的日期(毫秒值)
        var youtime = 0;
        youtime = endtime - nowtime;

        var seconds = youtime/1000;
        var minutes = Math.floor(seconds/60);
        var hours = Math.floor(minutes/60);
        var day = Math.floor(hours/24);
        var CHour= hours % 24;
        var CMinute= minutes % 60;
        var CSecond= Math.floor(seconds%60);//"%"是取余运算，可以理解为60进一后取余数，然后只要余数。
        if(endtime<=nowtime){
            $(this).html("已过期")//如果结束日期小于当前日期就提示过期啦
        }else{
            var time_str = '';
            if(day>0) {
                time_str += day + '天';
            }
            if(CHour<10) {
                time_str += '0' + CHour + '时';
            } else {
                time_str += CHour + '时';
            }
            if(CMinute<10) {
                time_str += '0' + CMinute + '分';
            } else {
                time_str += CMinute + '分';
            }
            if(CSecond<10) {
                time_str += '0' + CSecond + '秒';
            } else {
                time_str += CSecond + '秒';
            }
            $(this).html(time_str);          //输出没有天数的数据
        }
    });
    setTimeout("timing()",1000);
};
function showBm_new(){
    $("#popupMain_bm").show();
    $(".popmask").css({'height':$(document).height()}).show();
    $("#pdinput_bm").focus();
}