function load(url, str, fun, dtype) {
    $.ajax({
        type: "POST",
        url: url,
        data: str,
        beforeSend: function() {
            showL();
        },
        success: fun,
        complete: function() {
            hideL();
        },
        error:function(){
            hideL();
        },
        dataType: dtype
    });
}

//报名补全接口
function public_zx_apply_expand(json , callback)
{
    var arr = {fj_num:0, kt_num:0, wsj_num:0, cfj_num:0, yt_num:0, memo:""};
    $.each(arr,function(k, v){
        if(json[k]) {
            arr[k] = json[k];
        }
    })
    if(typeof callback == "function") {
        load('/JiaZhuangxiu/AjaxSaveNewShopApplyUserGuide', arr, callback);
    } else {
        load('/JiaZhuangxiu/AjaxSaveNewShopApplyUserGuide', arr);
    }
   
}

//预算结果 接口
function public_zx_yusuan_result(json , callback)
{
    var arr = {area: 0 , pro:'', areaname:''};
    $.each(arr,function(k, v){
        if(json[k]) {
            arr[k] = json[k];
        }
    })
    if(typeof callback == "function") {
        load('/JiaZhuangxiu/qOpenYusuan/', arr, callback);
    } else {
        load('/JiaZhuangxiu/qOpenYusuan/', arr);
    }
   
}

function showL(){
    $("#load_div").show();
    $("#load_icon").show();
}

function hideL(){
    $("#load_div").hide();
    $("#load_icon").hide();
}

function trimString(str)
{
    if(str == undefined || str.length<1){
        return '';
    }
    return str.replace(/(^\s*)|(\s*$)/g, "");
}

//add by lou
//格式化日期函数
function getNowDate() {
    var nowtime = new Date();
    var year = nowtime.getFullYear();
    var month = padleft0(nowtime.getMonth() + 1);
    var day = padleft0(nowtime.getDate());
    var hour = padleft0(nowtime.getHours());
    var minute = padleft0(nowtime.getMinutes());
    var second = padleft0(nowtime.getSeconds());
    var millisecond = nowtime.getMilliseconds(); millisecond = millisecond.toString().length == 1 ? "00" + millisecond : millisecond.toString().length == 2 ? "0" + millisecond : millisecond;
    return year + "-" + month + "-" + day;
}
//补齐两位数
function padleft0(obj) {
    return obj.toString().replace(/^[0-9]{1}$/, "0" + obj);
}


     

