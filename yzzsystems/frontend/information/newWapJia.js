function taoSwiper(tBox,args) {//手指滑动多少，指定容器滑动多少
	var tPoint={
		self:tBox,
		count:0,
		speed:300,
		iniL:30,
		iniT:30,
		iniAngle:30,
		touch:false,
		mutiTouch:false,
		setAttr:function(name,value){
			tPoint[name]=value;
		}
	};
	
	// init初始化
	(function(){	
		_this=tPoint.self;
		for(var o in args){
			tPoint[o]=args[o];
		}
		var _offset=_this.offset();
		tPoint.bL=_offset.left;
		tPoint.bT=_offset.top;
		tPoint.bW=_this.width();
		tPoint.bH=_this.height();
		tPoint.bRb=tPoint.bL+tPoint.bW; //右边界
		tPoint.bBb=tPoint.bT+tPoint.bH; //下边界
		tPoint.total=_this.children().children().length;		
	})();
	
  	// 浏览器特性检测
  	tPoint.vendor = (/webkit/i).test(navigator.appVersion) ? 'webkit' :
  		(/firefox/i).test(navigator.userAgent) ? 'Moz' :
  		'opera' in window ? 'O' : '';
  	tPoint.has3d = 'WebKitCSSMatrix' in window && 'm11' in new WebKitCSSMatrix();
  	tPoint.hasTouch = 'ontouchstart' in window;
  	tPoint.hasTransform = tPoint.vendor + 'Transform' in document.documentElement.style;
	
	// 方向检测(左移距离，上移距离)
	function directionDetect(l,t){
		if(Math.abs(t)<tPoint.iniT&&Math.abs(l)>tPoint.iniL){
			var rStr=l<0?'left':'right';
			return rStr;
		}
		return false;
	}
	
	// 边界检测
	function borderDetect(x,y){
		return (x<tPoint.bL||x>tPoint.bRb||y<tPoint.bT||y>tPoint.bBb);	
	}
	
	// 获取夹角(通过arctant反三角函数)
	function getAngle(n){
		return Math.atan(n)*180/Math.PI;
	}
	
	// 获取距离(通过两边计算第三边)
	function getDis(xLen,yLen){
		return Math.sqrt(Math.pow(xLen,2)+Math.pow(yLen,2));
	}

	// 设置tPoint的改变数据（默认设置改变数据，如果setList存在，则遍历setList设置属性）
	function setPointData(point,setList){
		if(setList){
			for(var o in setList){
				tPoint.setAttr(o,setList[o]);
			}
		}else{
			var t = new Date();
			tPoint.endX=point.pageX;
			tPoint.endY=point.pageY;
			tPoint.endTime=t.getTime();
			tPoint.duration=tPoint.endTime-tPoint.startTime;
			tPoint.mX=point.pageX-tPoint.startX;//滑动距离
			tPoint.mY=point.pageY-tPoint.startY;
			tPoint.direction=directionDetect(tPoint.mX,tPoint.mY);		
			tPoint.angle=getAngle(tPoint.mY/tPoint.mX);
		}
	}
	
	// 多点触摸检测(只检测两点触摸)
	function multiTouchDetect(e){
		tPoint.tLen=e.touches.length;
		if(tPoint.tLen>1){
			var point0=e.touches[0],
				point1=e.touches[1],
				xLen=point1.pageX-point0.pageX,
				yLen=point1.pageY-point0.pageY,
				angle=getAngle(yLen/xLen),
				gDis=getDis(xLen,yLen);
			if(!tPoint.mutiTouch){
				tPoint.gStartDis=gDis;
				tPoint.gStartAngle=angle;
			}else{
				tPoint.gEndDis=gDis;
				tPoint.gEndAngle=angle;
				tPoint.scale=gDis/tPoint.gStartDis;
				tPoint.rotation=angle-tPoint.gStartAngle;
			}
			tPoint.mutiTouch=true;
		}else{
			tPoint.mutiTouch=false;
		};	
	}
	
	function startFun(e){
		var point = e.touches[0],
			t = new Date();
		var setList={
			startX:point.pageX,
			startY:point.pageY,
			startTime:t.getTime(),
			identifier:point.identifier
		}
		setPointData(point,setList);
		tPoint.touch=true;
		if(typeof tPoint.sCallback=="function"){
			tPoint.sCallback(tPoint);
		}
	}
	
	function moveFun(e){
		var point = e.touches[0];
		if(borderDetect(point.pageX,point.pageY)){
			tPoint.touch=false;
			return false;
		}
		if(tPoint.touch){
			setPointData(point);
			multiTouchDetect(e);// Muti touch detect
			if(typeof tPoint.mCallback=="function"){
				tPoint.mCallback(tPoint);
			}
		}
		if(Math.abs(tPoint.angle)<tPoint.iniAngle){
			e.preventDefault();	
		}
	}
	
	function endFun(e){
		tPoint.touch=false;
		tPoint.mutiTouch=false;
		if(typeof tPoint.eCallback=="function"){
			tPoint.eCallback(tPoint);
		}
	}

	// 实例化之前的回调函数
	if(typeof tPoint.beforeCallback=="function"){
		tPoint.beforeCallback(tPoint);
	}
	
	// Touch事件监听
	_this.die("touchstart,touchmove,touchend");
	_this.get(0).addEventListener('touchstart',startFun);
	_this.get(0).addEventListener('touchmove',moveFun);
	_this.get(0).addEventListener('touchend',endFun);
	
	// 实例化之后的回调函数
	if(typeof tPoint.afterCallback=="function"){
		tPoint.afterCallback(tPoint);
	}	
};

window.screenSwipe = function(element, options) {//整屏的滑动
    if (!element) return null;
    var _this = this;
    this.options = options || {};
    this.index = this.options.startSlide || 0;
    this.speed = this.options.speed || 300;
    this.callback = this.options.callback || function() {};
    this.delay = this.options.auto || 0;
    this.unresize = this.options.unresize;
    this.container = element;
    this.element = this.container.children[0];
    this.element.style.listStyle = 'none';
    this.setup();
    this.begin();
    if (this.element.addEventListener) {
        this.element.addEventListener('mousedown', this, false);
        this.element.addEventListener('touchstart', this, false);
        this.element.addEventListener('touchmove', this, false);
        this.element.addEventListener('touchend', this, false);
        this.element.addEventListener('webkitTransitionEnd', this, false);
        this.element.addEventListener('msTransitionEnd', this, false);
        this.element.addEventListener('oTransitionEnd', this, false);
        this.element.addEventListener('transitionend', this, false);
        if(!this.unresize){
            window.addEventListener('resize', this, false);
        }
    }
};
screenSwipe.prototype = {
    setup: function() {
        this.slides = this.element.children;
        this.length = this.slides.length;
        if (this.length < 1) return null;
        this.width = this.container.getBoundingClientRect().width || this.width; //anjey
        if (!this.width) return null;
        this.container.style.visibility = 'hidden';
        this.element.style.width = (this.slides.length * this.width) + 'px';
        var index = this.slides.length;
        while (index--) {
            var el = this.slides[index];
            el.style.width = this.width + 'px';
            el.style.display = 'table-cell';
            el.style.verticalAlign = 'top';
        }
        this.slide(this.index, 0);
        this.container.style.visibility = 'visible';
    },
    slide: function(index, duration) {
        var style = this.element.style;
        if (duration == undefined) {
            duration = this.speed;
        }
        style.webkitTransitionDuration = style.MozTransitionDuration = style.msTransitionDuration = style.OTransitionDuration = style.transitionDuration = duration + 'ms';
        style.MozTransform = style.webkitTransform = 'translate3d(' + -(index * this.width) + 'px,0,0)';
        style.msTransform = style.OTransform = 'translateX(' + -(index * this.width) + 'px)';
        this.index = index;
    },
    getPos: function() {
        return this.index;
    },
    prev: function(delay) {
        this.delay = delay || 0;
        clearTimeout(this.interval);
        if (this.index) this.slide(this.index-1, this.speed);
    },
    next: function(delay) {
        this.delay = delay || 0;
        clearTimeout(this.interval);
        if (this.index < this.length - 1) this.slide(this.index+1, this.speed);
        else this.slide(0, this.speed);
    },
    begin: function() {
        var _this = this;
        this.interval = (this.delay)
            ? setTimeout(function() {
            _this.next(_this.delay);
        }, this.delay)
            : 0;
    },
    stop: function() {
        this.delay = 0;
        clearTimeout(this.interval);
    },
    resume: function() {
        this.delay = this.options.auto || 0;
        this.begin();
    },
    handleEvent: function(e) {
        var that = this;
        if(!e.touches){
            e.touches = new Array(e);
            e.scale = false;
        }
        switch (e.type) {
            case 'mousedown': (function(){
                that.element.addEventListener('mousemove', that, false);
                that.element.addEventListener('mouseup', that, false);
                that.element.addEventListener('mouseout', that, false);
                that.onTouchStart(e);
            })(); break;
            case 'mousemove': this.onTouchMove(e); break;
            case 'mouseup': (function(){
                that.element.removeEventListener('mousemove', that, false);
                that.element.removeEventListener('mouseup', that, false);
                that.element.removeEventListener('mouseout', that, false);
                that.onTouchEnd(e);
            })(); break;
            case 'mouseout': (function(){
                that.element.removeEventListener('mousemove', that, false);
                that.element.removeEventListener('mouseup', that, false);
                that.element.removeEventListener('mouseout', that, false);
                that.onTouchEnd(e);
            })(); break;
            case 'touchstart': this.onTouchStart(e); break;
            case 'touchmove': this.onTouchMove(e); break;
            case 'touchend': this.onTouchEnd(e); break;
            case 'webkitTransitionEnd':
            case 'msTransitionEnd':
            case 'oTransitionEnd':
            case 'transitionend': this.transitionEnd(e); break;
            case 'resize': this.setup(); break;
        }
    },
    transitionEnd: function(e) {
        e.preventDefault();
        if (this.delay) this.begin();
        this.callback(e, this.index, this.slides[this.index]);
    },
    onTouchStart: function(e) {
        this.start = {
            pageX: e.touches[0].pageX,
            pageY: e.touches[0].pageY,
            time: Number( new Date() )
        };
        this.isScrolling = undefined;
        this.deltaX = 0;
        this.element.style.MozTransitionDuration = this.element.style.webkitTransitionDuration = 0;
    },
    onTouchMove: function(e) {
        if(e.touches.length > 1 || e.scale && e.scale !== 1) return;
        this.deltaX = e.touches[0].pageX - this.start.pageX;
        if ( typeof this.isScrolling == 'undefined') {
            this.isScrolling = !!( this.isScrolling || Math.abs(this.deltaX) < Math.abs(e.touches[0].pageY - this.start.pageY) );
        }
        if (!this.isScrolling) {
            e.preventDefault();
            clearTimeout(this.interval);
            this.deltaX =
                this.deltaX /
                ( (!this.index && this.deltaX > 0
                || this.index == this.length - 1
                && this.deltaX < 0
                ) ?
                    ( Math.abs(this.deltaX) / this.width + 1 )
                    : 1 );
            this.element.style.MozTransform = this.element.style.webkitTransform = 'translate3d(' + (this.deltaX - this.index * this.width) + 'px,0,0)';
        }
    },
    onTouchEnd: function(e) {
        var isValidSlide =
                Number(new Date()) - this.start.time < 250
                && Math.abs(this.deltaX) > 20
                || Math.abs(this.deltaX) > this.width/2,
            isPastBounds =
                !this.index && this.deltaX > 0
                || this.index == this.length - 1 && this.deltaX < 0;
        if (!this.isScrolling) {
            this.slide( this.index + ( isValidSlide && !isPastBounds ? (this.deltaX < 0 ? 1 : -1) : 0 ), this.speed );
        }
    }
};


function showMaskEdit(args){
	var options={
		width:"auto",
		classNames:"popup-maskEdit",
		text:"请输入弹框内容",
		whiteSpace:"nowrap",
		duration:1000
	};
	for(var o in args){
		options[o]=args[o];
	}
	var timer = null;
	clearInterval(timer);
	var msg = "<div class='"+options.classNames+"'>"+options.text+"</div>";
	if($("."+options.classNames).length<1){
		$("body").append(msg);
	}else{
		$("."+options.classNames).html(options.text);
	}
	var _this = $("."+options.classNames);
	_this.css({"z-index":9999});
	if(options.width){
		_this.css("width",options.width);
	}
	if(options.whiteSpace){
		_this.css("white-space",options.whiteSpace);
	}
	if(window.jQuery){
		_this.show("fast",function(){
		timer = setTimeout(function(){
				_this.fadeOut();
			},options.duration);
		});
	}else if(window.Zepto){
		_this.animate({display:"block"},"fast");
		timer = setTimeout(function(){
			_this.animate({display:"none"},"fast");
		},options.duration);
	}
};
function checkTextareaNum(options){
	var defaults = {
		areaEle:$("#refundArea"),//textarea框容器
		numEle:$("#textareaNum"),//倒数容器
		charNumber:true,//是否按字符数计数，默认按字符计数
		backCount:true,//是否倒数，默认倒数
		maxLen:200//默认最大数200
	};
	for( var a in options){
		defaults[a] = options[a];
	}
	var maxLen = defaults.maxLen;//最大数
	var len = 0;//字符数
	function getLen(){
		len = 0;
		var realLen = defaults.areaEle.val().length;
		if(defaults.charNumber){
			var val = defaults.areaEle.val();
			for(var i=0;i<realLen;i++){
				if(val.charCodeAt(i)<0||val.charCodeAt(i)>255){
					len+=2;
				}else{
					len+=1;
				}
			}
		}else{
			len = realLen;
		}
		if(len>maxLen){
			defaults.numEle.addClass("error");
		}else{
			defaults.numEle.removeClass("error");
		}
	}
	if(defaults.backCount){
		defaults.numEle.html("还可输入<label class='num'>"+ maxLen +"</label>个字");
	}else{
		defaults.numEle.html("已输入<label class='num'>"+ 0 +"</label>个字");
	}
	defaults.areaEle.on('input',function(){
		getLen();
		if(defaults.backCount){
			defaults.numEle.html("还可输入<label class='num'>"+ (maxLen-len) +"</label>个字");
		}else{
			if (len>maxLen) {
				defaults.numEle.html("已超出<label class='num'>"+ (len-maxLen) +"</label>个字");
			}else{
				defaults.numEle.html("已输入<label class='num'>"+ len +"</label>个字");
			}
		}
	});
}

var scrollMove = function(e) {
	e.preventDefault && e.preventDefault();
	e.returnValue = false;
	e.stopPropagation && e.stopPropagation();
	return false;
}
function autoScroll() {
	window.removeEventListener('touchmove', scrollMove);
}
function noScroll() {
	window.addEventListener('touchmove', scrollMove);
}


if (window.jQuery || window.Zepto) { (function(a) {
		a.fn.taoSwiper = function(b) {
			return this.each(function() {
				a(this).data("taoSwiper", new taoSwiper(a(this), b))
			})
		};
		a.showMaskEdit = function(a){
			showMaskEdit(a);
		};
		a.checkTextareaNum = function(a){
			checkTextareaNum(a);
		}
	})(window.jQuery || window.Zepto)
};




var storageInfo=(function(){
	return {
		set:function(name,value,expires,path,domain){
			if(typeof expires=="undefined"){
				expires=new Date(new Date().getTime()+(1000*60*60*24*30));
			}
			document.cookie=name+"="+encodeURIComponent(value)+((expires)?";expires="+expires.toGMTString():"")+((path)?";path="+path:";path=/")+((domain)?";domain="+domain:"");
		},
		get:function(name){
			var arr=document.cookie.match(new RegExp("(^| )"+name+"=([^;]*)(;|$)"));
			if(arr!=null){return decodeURIComponent(arr[2]);}
			return null;
		},
		clear:function(name,path,domain){
			if(this.get(name)){
				document.cookie=name+"="+((path)?"; path="+path:"; path=/")+((domain)?"; domain="+domain:"")+";expires=Fri, 02-Jan-1970 00:00:00 GMT";
			}
		}
	}
})();
//storageInfo.set("test","123");
//storageInfo.get("test");


/*图片预览*/
var imagesPopup = function(ele,parentsName,options){
    if(!ele){ return false;}//ele是当前点击的元素里面的img
    var className = $(ele).attr('class');
    this.options = options || {};
    this.index = this.options.index || 0;
    this.parentsName = parentsName;
    this.callback = this.options.callback || function() {};
    this.ele = $(ele).parents('.'+this.parentsName).find('.'+className);
    this.init();
}
imagesPopup.prototype = {
    init:function(){
        var _this = this;
        var eleArr = this.ele;
        var msg = "";
        var infoArr = [];
        for(var i =0; i<eleArr.length;i++){
            var obj = {};
            obj.bigSrc = eleArr.eq(i).attr('bigSrc') || eleArr.eq(i).attr('src');
            console.log(eleArr.eq(i).attr('src'));
            infoArr.push(obj);
        }
        this.infoArr = infoArr;
        msg += "<section class=\"popup-swiper\">";
        msg += "<div class=\"swiper-container\">";
        msg += "<div class=\"swiper-wrapper\">";
        msg += "</div>";
        msg += "</div>";
        msg += "<div class=\"num-box\">";
        msg += "<span class=\"current\" id=\"curImgNum\">1</span><span class=\"separate\">/</span><span class=\"all\" id=\"allImgNum\">1</span>";
        msg += "</div>";
        msg += "</section>";
        if($('.popup-swiper').length>0) {
            $('.popup-swiper').remove();
        }
        $('body').append(msg);
        var msgEle = "";
        for(var i =0; i<eleArr.length;i++){
            msgEle += "<div class=\"swiper-slide\">";
            msgEle += "<img src=\""+infoArr[i].bigSrc+"\" />";
            msgEle += "</div>";
        }
        $('.popup-swiper .swiper-wrapper').append(msgEle);
        this.activeIndex = this.index;
        this.mySwiper = new Swiper('.swiper-container',{
            onInit:function(swipe){
                $('#allImgNum').html(swipe.slides.length);
                $('#curImgNum').html(_this.index + 1);
            },
            onSlideChangeStart:function(swipe){
                $('#curImgNum').html(swipe.activeIndex + 1);
                _this.activeIndex = swipe.activeIndex;
            },
            initialSlide:_this.index
        });
        this.callback();
        $('.popup-swiper').off('click').click(function(){
            $('.popup-swiper').hide();
        });
    }
};

function countDownFn(options){
    var obj = {
        ele:'',//ele--string,'.class' '#id' 目标元素
        endTime:'',//endTime -- string '2015/5/5 8:00' or number 1440050716249 结束时间
        timeCheck:false,//timeCheck -- boolean 默认false 是否从服务器拿当前时间
        showType:'txt',//string 'txt' -- '5天2小时3分3秒' 'time' -- '5天 02:03:03'
        //支持回调函数,参数为callback
        moreEleSync:false//是否多个class同步倒计时,默认false,只有class时候才生效
    };
    for(var i in options){
        obj[i] = options[i];
    }
    var eleArr;

    if(obj.moreEleSync && ($.trim(obj.ele).indexOf('.') == 0)){
        eleArr = Array.prototype.slice.call(document.querySelectorAll(obj.ele));
    }else{
        eleArr = [];
        document.querySelector(obj.ele)?eleArr.push(document.querySelector(obj.ele)):void(0);
    }
    var startTime = obj.timeCheck == true?getServerTime():new Date().getTime();
    var endTime = new Date(obj.endTime);
    var countTime = Math.floor((endTime - startTime)/1000);
    obj.timer = window.setInterval(function(){
        countTime--;
        if(countTime<0){
            window.clearInterval(obj.timer);
            obj.timer = null;
            eleArr.forEach(function(ele){
                ele.innerHTML = '还剩0小时0分0秒';
                if(typeof obj.callback == 'function'){
                    obj.callback.call(ele);
                }
            });
            return false;
        }
        var day = Math.floor(countTime/(3600*24));
        var hour = Math.floor((countTime - day*3600*24)/3600);
        var minutes = Math.floor((countTime - day*3600*24 - hour*3600)/60);
        minutes < 10 ? minutes = '0'+ minutes : minutes;
        var second = Math.floor(countTime - day*3600*24 - hour*3600 - minutes*60);
        second < 10 ? second = '0'+ second : second;
        var msg = '';
        if(obj.showType == 'time'){
            if(day!=0){
                msg = day + '天 ' + hour + ' : ' + minutes + ' : ' + second;
            }else{
                msg = hour + ' : ' + minutes + ' : ' + second;
            }
        }else{
            if(day!=0){
                msg = '还剩' + day + '天'+ hour + '小时'+ minutes + '分' + second + '秒';
            }else{
                msg = '还剩' + hour + '小时'+ minutes + '分' + second + '秒';
            }
        }
        eleArr.forEach(function(ele){
            ele.innerHTML = msg;
        });
    },1000);
}


//loading.js
$(function(){
    var cur_city=storageInfo.get("city_id");
    $("#spec").attr("href","//m.jia.com/tg/"+cur_city);
    $(".menu").off('click').click(function(){
        if($("[name='other_main']").is(":hidden")){
            $("[name='other_main']").show();
            $(".header").css({'z-index':101});
            $(".popmask").css({height:$(document).height(),'display':'block'});
        }else{
            $("[name='other_main']").hide();
            $(".header").css({'z-index':'auto'});
            $(".popmask").hide();
        }
    });
    $("[name='head_main'],.backtohis").off('click').click(function(){
        $("[name='other_main']").hide();
        $(".popmask").hide();
    });
    $(".popmask").click(function(){
        $("[name='other_main']").hide();
        $(".popmask,.loader-popup").hide();
    });
    $("[name='backtotop']").off('click').click(function(){
        $("html, body").scrollTop(0);
    });

   if($(".backtohis").attr("href")!="javascript:history.back(-1);"){			
		$(".backtohis").off("click").click(function(){
		  window.history.go(-1);
       });
	}
});
function popupFn(dom){//弹窗显示
    $(".popmask").css({'height':$(document).height()}).show();
    dom.show();
    var windowHeight = $(window).height();
    var scrollTop=$(window).scrollTop();
    var popupHeight = dom.height();
    var posTop = scrollTop+(windowHeight - popupHeight)/2;
    dom.css({'top':posTop,'bottom':'auto'});
}
