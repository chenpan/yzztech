$(document).ready(function($){
	setTimeout(function(){
		var check_login = JSON.parse(window.sessionStorage.getItem('check_login'));
		var weixin = is_weixin();
		var jia_user_name = getCookie('www_jia_user_name') == null ? null : decodeURIComponent(getCookie('www_jia_user_name'));
		if(check_login != null){
			if(((jia_user_name != null || check_login.user_name != undefined) && (jia_user_name != check_login.user_name)) || getCookie('LOGIN_AREAFLAG') != check_login.areaflag){
				window.sessionStorage.removeItem('check_login_expire');
				window.sessionStorage.removeItem('check_login');
				check_login = null;
			}
		}
		if(Date.parse(new Date())/1000 > window.sessionStorage.getItem('check_login_expire')){
			window.sessionStorage.removeItem('check_login_expire');
			window.sessionStorage.removeItem('check_login')
		} 
		if(check_login != null){
			dataRender(check_login);
		}else{
			$.getJSON('/UserApi/checkLogin?callback=?', function(response){
				if((response.user_name == undefined || response.user_name == '') && getCookie('USER_ID') != null){
					var iframe = document.createElement("iframe");
					iframe.src = "/UserApi/iframeLogin";
					iframe.style.display = 'none';
					console.log(iframe);
					iframe.onload = function(){
						$.getJSON('/UserApi/checkLogin?callback=?', function(data){
							//alert(data.user_name);
							dataRender(data);
						});
					};
					$('body').append(iframe);	
				}else{
					dataRender(response);
				}
				window.sessionStorage.setItem('check_login_expire', Date.parse(new Date())/1000+600);
				window.sessionStorage.setItem('check_login', JSON.stringify({areaflag:response.areaflag, areaname:response.areaname, user_name:response.user_name}));
				
				
			});
			if(window.location.host == 'm.jia.com' && weixin == true){
				var url = 'passport.jia.com';
				$.getJSON('https://'+url+'/cas/login/user?r='+Math.random()+'&callback=?');
			}
			
			
		}
	}, 800);
});


function dataRender(response){
	$('.newFooter-classfiy a:first').attr('href', 'http://'+window.location.host+'/'+response.areaflag);
	if(response.areaflag == 'shanghai')
		$(".newFooter-classfiy li").eq(2).find("a").attr('href', 'Tel:4006607700');
	$('.citys').html(response.areaname);
	if(response.user_name){
		$('.loginin').html(response.user_name);
		$(".newFooter-classfiy li").eq(1).find("a").attr('href', 'http://'+window.location.host+'/i/');
	}
}

$(".newFooter-classfiy li").eq(1).find("a").attr('href', 'http://'+window.location.host+'/login/');
var getLoginUser = function(response){
	if(response.name && getCookie('USER_ID') != null){
		$('.loginin').html(response.name);
		$(".newFooter-classfiy li").eq(1).find("a").attr('href', 'http://'+window.location.host+'/i/');
	}else{
		$(".newFooter-classfiy li").eq(1).find("a").attr('href', 'http://'+window.location.host+'/login/');
	}
};

function getCookie(name){
    var arr,reg=new RegExp("(^| )"+name+"=([^;]*)(;|$)");
    if(arr=document.cookie.match(reg)){
        //return unescape(arr[2]);
		return arr[2];
    }else{
        return null;
    }
}

function setCookie(name, value){
    var time = arguments[2] ? arguments[2] : 30*24*3600;
    var domain = arguments[3] ? arguments[3] : '';
    var exp = new Date();
    exp.setTime(exp.getTime() + time*1000);
    //document.cookie = name + "="+ escape (value) + ";expires=" + exp.toGMTString();
   	if(domain)
   		document.cookie = name + "="+value+";expires=" + exp.toGMTString() + ";domain="+domain;
   	else
   		document.cookie = name + "="+value+";expires=" + exp.toGMTString();
}

function is_weixin(){
	var ua = navigator.userAgent.toLowerCase();
	if(ua.match(/MicroMessenger/i)=="micromessenger") {
		return true;
 	} else {
		return false;
	}
}

