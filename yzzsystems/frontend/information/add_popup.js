//公用样式
var cssText = '<style type="text/css">'
        + '    .bottom-apply{ box-sizing: border-box;padding: .06rem .12rem;background: #fff;position: fixed;bottom: 0; left: 0;right: 0;z-index:999; border-top:.005rem solid #ddd}'
        + '    .bottom-apply .bottom-apply-btn{display: block;width: 100%;height: .33rem;line-height: .33rem;background: #dd0000;'
        + '    color: #fff;font-size: .14rem;text-align: center;border-radius: 4px;}'
        + '    .popup-mask{position: fixed;display: none;top: 0;left: 0;right: 0; bottom: 0; z-index: 1000;width: 100%; background: rgba(0,0,0,.4);}'
        // +'    .popup-bg{display: none;position: fixed;top: 0;left: 0; width: 100%;z-index: 1001;}'
        + '    .popup-content{display: none;background: #fff;position: fixed;width: 77.8%;left: 50%;margin-left: -38.9%;top: 50%; border-radius: 4px;'
        + '    z-index: 1002;padding: 0 6%;box-sizing:border-box; -webkit-box-sizing:border-box;}'
        // +'    .popup-fd{position: absolute;left: 0;top: 0;width: 100%;height: 100%;z-index: 1002;}'
        + '    .popup-content .title{ font-size: .14rem;color: #333;text-align: center;padding-top: .178rem;}'
        + '    .title-tip{font-size: .107rem;color: #666;text-align: center;padding-top: .089rem;}'
        + '    .sign-apply-form .sign-item-par{border-bottom: .01rem solid #df1111;position: relative;padding: .1rem .06rem;'
        + '    font-size: .12rem;margin-bottom: .03rem;}'
        + '    .sign-apply-form .sign-item-par input{ width: 100%;}'
        + '    .sign-apply-form{padding-top: .13rem;}'
        + '    .sign-apply-form .sign-item-par:before{content: "";display: block; width: .01rem;height: .04rem;'
        + '    background: #df1111;position: absolute;left: 0;bottom: 0;}'
        + '    .sign-apply-form .sign-item-par:after{content: "";display: block;width: .01rem;height: .04rem;background: #df1111;'
        + '    position: absolute;right: 0;bottom: 0;}'
        + '    .apply-btn{display: block;width: 100%;text-align: center;height: .3rem;line-height: .3rem;color: #fff;background: #dd0000;'
        + '        border-radius: 4px;}'
        + '    .sign-btn-par{padding: .178rem 0;}'
        + '</style>';
$(function () {
    $('body').append(cssText);
    $(".L_order_btn").live("click", function () {
        $(".bottom-apply").hide()
    });
})



/*
 弹层   
 使用页面--工地直播列表页，装修图库列表页，装修公司列表页
 */
function add_popup() {

    var htmlText = '<div style="height:.5rem"><div class="bottom-apply">'
            + '    <a class="bottom-apply-btn" href="javascript:void(0);" >免费预约装修</a>'
            //+ '    <a class="new-apply-btn" href="/JiaZhuangxiu/zbDesigApply/" >免费预约装修</a>'
            + '</div><div class="popup-mask"></div>'
            // +'<div class="popup-bg">'
            // +'    <div class="popup-fd"></div>'
            + '    <div class="popup-content">'
            + '        <h3 class="title">对装修设计费说NO</h3>'
            + '        <p class="title-tip">免费出2-3套定制设计方案，供您对比选择</p>'
            + '        <div class="sign-apply-form">'
            + '            <p class="sign-item-par">'
            + '                <input class="phone-num telcheck_20160531" type="number" placeholder="您的手机号" id="pop_mobile"/>'
            + '            </p>'
            + '            <p class="sign-btn-par">'
            + '                <a class="apply-btn" href="javascript:;" id="pop_assign_">免费获取</a>'
            + '            </p>'
            + '        </div>'
            + '    </div></div>';
    // +'</div>';
    $('body').append(htmlText);

    //显示弹层
    //var screenHeight = window.screen.height;
    //$(".popup-bg").height(screenHeight);
    $(".bottom-apply-btn").click(function () {
        $(".bottom-apply").hide();
        $(".popup-mask,.popup-content").show();
        $(".popup-content").css("margin-top", -$(".popup-content").height() / 2);
    });

    //关闭弹层
    $(".popup-mask").click(function () {
        $(".popup-mask,.popup-content").hide();
        $(".bottom-apply").show();
    });

}


/*
 按钮，定位
 使用页面 ---施工包，388主材包，588主材包
 */
function fiexd_botton() {

    var path = window.location.pathname;
    var pd_message = path.split("/")[3];

    //按钮文案
    var text = "一键预约标准施工包";
    if (pd_message == "sgb") {
        text = "一键预约标准施工包";
    } else if (pd_message == "388zcb") {
        text = "一键预约388主材包";
    } else if (pd_message == "588zcb") {
        text = "一键预约688主材包";
    }

    //添加文本
    var htmlText = '<div class="bottom-apply">'
            + '    <a class="bottom-apply-btn" href="#dregister">' + text + '</a>';
    +'</div>';

    $('body').append(htmlText);



    //按钮 显示/隐藏
    var top = $("#dregister").offset().top - $("#dregister").height();
    $(window).scroll(function () {
        var scrollTop = $(window).scrollTop();
            if (scrollTop >= top) {
                $(".bottom-apply").hide();
            } else {
                $(".bottom-apply").show();
                if ($(".zx-popup-common").length > 0){
                      if (!$(".zx-popup-common").is(":hidden")) {
                         $(".bottom-apply").hide();
                       }
                }
               
            }
    });

}
