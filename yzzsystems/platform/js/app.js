
//BACKGROUND CHANGER

$(function() {
    $("#button-bg").click(function() {
        $("body").css({
            "background": "url('././platform/img/bg5.jpg')repeat fixed"
        });
        $(".order-detail-title").css({
            "background-color": "#85BDA6", "color": "#1B5606"
        });
    });
    $("#button-bg2").click(function() {
        $("body").css({
            "background": "url('././platform/img/bg2.jpg')repeat fixed"
        });
        $(".order-detail-title").css({
            "background-color": "rgba(154, 109, 44, 0.7)", "color": "#653E05"
        });
    });


    $("#button-bg3").click(function() {
        $("body").css({
            "background": "url('././platform/img/bg.png') repeat fixed"
        });
         $(".order-detail-title").css({
            "background-color": "rgba(6, 92, 218, 0.52)", "color": "#264794"
        });
    });

    $("#button-bg5").click(function() {
        $("body").css({
            "background": "url('././platform/img/giftly.png')repeat fixed"
        });
        $(".order-detail-title").css({
            "background-color": "rgba(35, 32, 28, 0.65)", "color": "rgba(0, 0, 0, 0.95)"
        });
    });

    $("#button-bg6").click(function() {
        $("body").css({
            "background": "url('././platform/img/b_login.jpg')repeat fixed"
        });
        $(".order-detail-title").css({
            "background-color": "rgba(123, 156, 179, 0.65)", "color": "rgba(53, 83, 101, 0.95)"
        });
    });

    $("#button-bg7").click(function() {
        $("body").css({
            "background": "url('././platform/img/bg3.png')repeat fixed"
        });
        $(".order-detail-title").css({
             "background-color": "rgba(12, 12, 12, 0.76)", "color": "rgb(0, 0, 0)"
        });
    });
    $("#button-bg8").click(function() {
        $("body").css({
            "background": "url('././platform/img/bg8.jpg')repeat fixed"
        });
        $(".order-detail-title").css({
            "background-color": "rgba(165, 105, 173, 0.76)", "color": "rgb(160, 15, 193)"
        });
    });
    $("#button-bg9").click(function() {
        $("body").css({
            "background": "url('././platform/img/bg9.jpg')repeat fixed"
        });
        $(".order-detail-title").css({
            "background-color": "rgba(111, 111, 111, 0.74)", "color": "#4A4545"
        });
    });

    $("#button-bg10").click(function() {
        $("body").css({
            "background": "url('././platform/img/bg10.jpg')repeat fixed"
        });
        $(".order-detail-title").css({
            "background-color": "rgba(101, 173, 34, 0.68)", "color": "#557D05"
        });
    });
    $("#button-bg11").click(function() {
        $("body").css({
            "background": "url('././platform/img/bg11.jpg')repeat fixed"
        });
        $(".order-detail-title").css({
            "background-color": "rgba(218, 144, 18, 0.68)", "color": "#7D7005"
        });
    });
    $("#button-bg12").click(function() {
        $("body").css({
            "background": "url('././platform/img/bg12.jpg')repeat fixed"
        });
        $(".order-detail-title").css({
            "background-color": "rgba(33, 9, 228, 0.4)", "color": "#49127D"
        });
    });

    $("#button-bg13").click(function() {
        $("body").css({
            "background": "url('././platform/img/bg13.jpg')repeat fixed"
        });
         $(".order-detail-title").css({
            "background-color": "rgba(8, 144, 212, 0.33)", "color": "#3176AD"
        });
    });
    /**
     * Background Changer end
     */
});

//TOGGLE CLOSE
$('.nav-toggle').click(function() {
    //get collapse content selector
    var collapse_content_selector = $(this).attr('href');

    //make the collapse content to be shown or hide
    var toggle_switch = $(this);
    $(collapse_content_selector).slideToggle(function() {
        if ($(this).css('display') == 'block') {
            //change the button label to be 'Show'
            toggle_switch.html('<span class="entypo-minus-squared"></span>');
        } else {
            //change the button label to be 'Hide'
            toggle_switch.html('<span class="entypo-plus-squared"></span>');
        }
    });
});


$('.nav-toggle-alt').click(function() {
    //get collapse content selector
    var collapse_content_selector = $(this).attr('href');

    //make the collapse content to be shown or hide
    var toggle_switch = $(this);
    $(collapse_content_selector).slideToggle(function() {
        if ($(this).css('display') == 'block') {
            //change the button label to be 'Show'
            toggle_switch.html('<span class="entypo-up-open"></span>');
        } else {
            //change the button label to be 'Hide'
            toggle_switch.html('<span class="entypo-down-open"></span>');
        }
    });
    return false;
});
//CLOSE ELEMENT
$(".gone").click(function() {
    var collapse_content_close = $(this).attr('href');
    $(collapse_content_close).hide();



});

//tooltip
$('.tooltitle').tooltip();
 