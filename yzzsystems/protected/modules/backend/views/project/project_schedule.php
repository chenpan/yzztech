<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <title><?php echo Yii::app()->session['website_name']; ?></title> 
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta name="description" content="">
        <meta name="author" content="">
        <?php echo $recommend; ?>
        <style type="text/css">
            canvas#canvas4 {
                position: relative;
                top: 20px;
            }
            input[type="search"]{
                padding-top:5px;
                padding-bottom: 5px;
            }

            .content-wrap{
                background-color: #FFF;
                margin: 10px;
                padding-top: 20px;
                padding-bottom: 20px;
                border-radius: 3px;
            }
            .menulist{
                margin-top: 25px;
            }
            #alreadytable_wrapper section{
                border:1px #f5f5f5  solid;
                outline:none;

            }
        </style>
        <script type="text/javascript">
            $(function() {
                var project_scheduletable = $('#project_scheduletable').dataTable({
                    stateSave: true,
                    "pagingType": "input",
                    "language": {
                        "lengthMenu": "每页 _MENU_ 条记录",
                        "zeroRecords": "没有找到记录",
                        "info": "第 _PAGE_ 页 ( 总共 _PAGES_ 页 )",
                        "infoEmpty": "无记录",
                        "infoFiltered": "(从 _MAX_ 条记录过滤)",
                        "search": ""
                    }
                });
                $("#project-open").css("display", "block");

                $("#logout").click(function() {
                    if (confirm("确定退出？")) {
                        window.location.href = "./index.php?r=backend/admin/Logout";
                    }
                });

                if ('<?php echo $change_project_step; ?>' == "hidden") {
                    $("#changestep").hide();
                }

                $(".step").focus(function() {
                    $(this).css({border: "1px solid #9EA7B3"});
                });

                $(".step").blur(function() {
                    $(this).css({border: "none"});
                });
                //更新顺序保存
                $("#change_project_schedule_step").click(function() {
                    var step = project_scheduletable.$('input').serialize();
                    step = step.replace("+", "");
                    //先去掉step=                       
                    var step_arr = step.split("&");

                    var nary = step_arr.sort();

                    //判断是否有重复值  并且判断是否全为数字
                    var status = false;
                    var is_num_status = true;
                    for (var i = 0; i < step_arr.length; i++) {
                        if (nary[i] == nary[i + 1]) {
                            status = true;
                            break;
                        }
                        if (isNaN(step_arr[i].replace("step=", ""))) {
                            is_num_status = false;
                            break;
                        }
                    }

                    if (status) {
                        alert("节点顺序不能有重复值！");
                        return;
                    }
                    if (!is_num_status) {
                        alert("节点顺序必须是数字！");
                        return;
                    }

                    if (confirm("确认更新节点顺序?")) {
                        $.post("./index.php?r=backend/project/change_project_schedule_step", {project_id:<?php echo $project_id; ?>, step: step}, function(datainfo) {
                            var data = eval("(" + datainfo + ")");
                            if (data.data == "success") {
                                alert("更新成功！");
                                window.location.href = "./index.php?r=backend/project/project_schedule&project_id=<?php echo $project_id; ?>";
                            } else
                                alert("更新失败！");
                        });
                    }
                });


            });

            function editproject_schedule(project_schedule_id) {
                if ('<?php echo $edit_project_schedule; ?>' == "") {
                    window.location.href = './index.php?r=backend/project/project_schedule_info&project_schedule_id=' + project_schedule_id;
                } else if ('<?php echo $edit_project_schedule; ?>' == "hidden") {
                    window.location.href = './index.php?r=backend/nonPrivilege/index';
                }
            }

            function deleteproject_schedule(project_schedule_id, schedule_name) {
                if ('<?php echo $del_project_schedule; ?>' == "") {
                    if (confirm("确认删除 " + schedule_name + " 节点?")) {
                        $.post("./index.php?r=backend/project/deleteproject_schedule", {project_schedule_id: project_schedule_id}, function(datainfo) {
                            var data = eval("(" + datainfo + ")");
                            if (data.data == "success") {
                                alert("删除成功！");
                                window.location.href = "./index.php?r=backend/project/project_schedule&project_id=<?php echo $project_id; ?>";
                            } else
                                alert("删除失败！");
                        });
                    }
                } else if ('<?php echo $del_project_schedule; ?>' == "hidden") {
                    window.location.href = './index.php?r=backend/nonPrivilege/index';
                }
            }
        </script>
        <!-- HTML5 shim, for IE6-8 support of HTML5 elements -->
        <!--[if lt IE 9]>
            <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
            <![endif]-->
        <!-- Fav and touch icons -->
    </head>
    <body> 
        <?php echo $leftContent; ?>
        <!--  PaPER WRaP -->
        <div class="wrap-fluid" style="margin-left:250px">
            <div class="container-fluid paper-wrap bevel tlbr">
                <!-- CONTENT -->
                <!-- BREaDCRUMB -->
                <div id="breadcrumb">
                    <div class="pull-left dis-left">
                        <H3>项目节点信息</H3>
                    </div>
                    <ul class="pull-right dis-left">
                        <li>
                            <span class="entypo-home"></span>
                        </li>
                        <li><i class="fa fa-lg fa-angle-right"></i>
                        </li>
                        <li><a href="./index.php?r=backend/admin/index">首页</a>
                        </li>
                        <li><i class="fa fa-lg fa-angle-right"></i>
                        </li>
                        <li><a href="#">项目</a>
                        </li>
                        <li><i class="fa fa-lg fa-angle-right"></i>
                        </li>
                        <li><a href="./index.php?r=backend/project/project">项目管理</a>
                        </li>
                        <li><i class="fa fa-lg fa-angle-right"></i>
                        </li>
                        <li><a href="#">项目节点信息</a>
                        </li>
                    </ul>
                </div>
                <div class="content-wrap">
                    <div class="row">
                        <div class="col-lg-1">
                            <input type="button" class="btn btn-success btn-set" id="change_project_schedule_step" value="更新顺序">
                        </div>
                    </div>
                </div>
                <div class="content-wrap">
                    <div class="row">
                        <div class="col-lg-12">
                            <table id="project_scheduletable" class="display">
                                <thead>
                                    <tr class="th">
                                        <th style="padding-left: 10px;">序列</th>
                                        <th>所属项目</th>
                                        <th>节点名称</th>
                                        <th style="width:400px;">节点描述</th>
                                        <th>节点状态</th>
<!--                                        <th>开始时间</th>
                                        <th>结束时间</th>-->
                                        <th>截止时间</th>
                                        <th>负责人</th>
                                        <th>节点顺序</th>
                                        <th>操作</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php foreach ($project_schedule_info as $K => $V) {
                                        ?>
                                        <tr>
                                            <td style="padding-left: 13px;"><?php echo $K + 1; ?></td>
                                            <td><?php
                                                $project_model = project::model();
                                                $project_info = $project_model->findByPk($V->_project_id);
                                                if (count($project_info) != 0) {
                                                    echo $project_info->project_name;
                                                } else {
                                                    echo "暂无";
                                                }
                                                ?>
                                            </td>
                                            <td><?php echo $V->schedule_name; ?></td>
                                            <td><?php echo strip_tags($V->schedule_descript); ?></td>
                                            <td><?php
                                                if ($V->status == 0) {
                                                    echo '未开始';
                                                } else if ($V->status == 1) {
                                                    echo '进行中';
                                                } else {
                                                    echo '已完成';
                                                }
                                                ?></td>
    <!--                                            <td><?php // echo $V->start_time;      ?></td>
                                            <td><?php // echo $V->end_time;      ?></td>-->
                                            <td><?php
                                                if ($V->deadline != "0000-00-00") {
                                                    echo $V->deadline;
                                                } else {
                                                    echo "";
                                                };
                                                ?></td>
                                            <td><?php
                                                $master_model = master::model();
                                                $master_info = $master_model->findByPk($V->_master_id);
                                                if (count($master_info) != 0) {
                                                    echo $master_info->master_name;
                                                } else {
                                                    echo "暂无";
                                                }
                                                ?>
                                            </td>
                                            <td>
                                                <input type="text" id="step" name="step" style="color:#9EA7B3;border: none;border-radius:25px" class="step" value="<?php echo $V->step; ?>"/>
                                            </td>
                                            <td>
                                                <a class="edit_btn" href="#" onclick="editproject_schedule(<?php echo $V->project_schedule_id; ?>)"><span class="label label-success">编辑</span></a>
                                                <a href="#" class="delete_btn" onclick="deleteproject_schedule(<?php echo $V->project_schedule_id; ?>, '<?php echo $V->schedule_name; ?>')" ><span class="label label-success">删除</span></a>
                                                <a class="project_schedule_btn" href="./index.php?r=backend/project/project_schedule_picture&project_schedule_id=<?php echo $V->project_schedule_id; ?>&project_id=<?php echo $project_id; ?>"><span class="label label-success">图片管理</span></a>
                                            </td>
                                        </tr>
                                    <?php } ?>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>

                <!-- FOOTER -->

                <div id="footer">
                    <div class="devider-footer-left"></div>
                    <div class="time">
                        <p id="spanDate">
                        <p id="clock">
                    </div>
                    <div class="copyright">Copyright © 2017-2018
                        <span class="entypo-heart"></span><a href="#">优自在装修</a>. All rights reserved.
                    </div>
                </div>
                <!-- / END OF FOOTER -->
            </div>
        </div>
        <!--  END OF PaPER WRaP -->
    </body>

</html>

