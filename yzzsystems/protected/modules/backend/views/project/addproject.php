<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <title><?php echo Yii::app()->session['website_name']; ?></title> 
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta name="description" content="">
        <meta name="author" content="">
        <?php echo $recommend; ?>
        <link rel="stylesheet" href="./css/multiselect/css/bootstrap-3.0.3.min.css" type="text/css">
        <link rel="stylesheet" href="./css/multiselect/css/bootstrap-multiselect.css" type="text/css">
        <link rel="stylesheet" href="./css/multiselect/css/prettify.css" type="text/css">

        <script type="text/javascript" src="./css/multiselect/js/bootstrap-multiselect.js"></script>
        <script type="text/javascript" src="./css/multiselect/js/prettify.js"></script>

        <script type="text/javascript" src="./css/laydate/laydate.js"></script>

        <style type="text/css">
            canvas#canvas4 {
                position: relative;
                top: 20px;
            }
            input[type="search"]{
                padding-top:5px;
                padding-bottom: 5px;
            }

            .content-wrap{
                background-color: #FFF;
                margin: 10px;
                padding-top: 20px;
                padding-bottom: 20px;
                border-radius: 3px;
            }
            .menulist{
                margin-top: 25px;
            }
            #alreadytable_wrapper section{
                border:1px #f5f5f5  solid;
                outline:none;
            }
        </style>
        <script type="text/javascript">
            $(function() {
                $("#logout").click(function() {
                    if (confirm("确定退出？"))
                    {
                        window.location.href = "./index.php?r=backend/admin/Logout";
                    }
                });

                $('#user_id').multiselect({
                    includeSelectAllOption: true,
                    enableFiltering: true,
                    maxHeight: 150
                });


                $("#project-open").css("display", "block");
                $("#_administrator_id").val("<?php
        if (isset($project_info)) {
            echo $project_info->_administrator_id;
        } else {
            echo 0;
        }
        ?>");

                $('#user_id').multiselect('select', [<?php
        if (isset($user_id_str)) {
            echo $user_id_str;
        } else {
            echo "0";
        }
        ?>]);


                $("#save").click(function() {
                    if ($("#project_name").val() == "" || $("#project_name").val() == null) {
                        reback();
                        $("#project_name_info").text("请输入合同号！");
                        return false;
                    } else if ($("#project_address").val() == "" || $("#project_address").val() == null) {
                        reback();
                        $("#project_address_info").text("请输入项目名称！");
                        return false;
                    } else if ($("#area").val() == "" || $("#area").val() == null) {
                        reback();
                        $("#area_info").text("请输入项目面积！");
                        return false;
                    } else if ($("#house_type").val() == "" || $("#house_type").val() == null) {
                        reback();
                        $("#house_type_info").text("请输入项目户型！");
                        return false;
                    } else if ($("#decoration_budget").val() == "" || $("#decoration_budget").val() == null) {
                        reback();
                        $("#decoration_budget_info").text("请输入装修预算！");
                        return false;
                    } else if ($("#_administrator_id").val() == 0) {
                        reback();
                        $("#_administrator_id_info").text("请选择负责人！");
                        return false;
                    } else if ($("#user_id").val() == null) {
                        reback();
                        $("#user_id_info").text("请选择业主！");
                        return false;
                    } else if ($("#contract_time").val() == "" || $("#contract_time").val() == null) {
                        reback();
                        $("#contract_time_info").text("请输入签约时间！");
                        return false;
                    } else {
                        reback();
                        if (confirm("确认保存？")) {
                            add_project_form.submit();
                        }
                    }
                });
            });
            function reback() {
                $("#project_name_info,#project_address_info,#_administrator_id_info,#area_info,#house_type_info,#decoration_budget_info,#contract_time_info").text("*");
                $("#info").text("");
            }
        </script>
        <!-- HTML5 shim, for IE6-8 support of HTML5 elements -->
        <!--[if lt IE 9]>
            <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
            <![endif]-->
        <!-- Fav and touch icons -->
    </head>
    <body> 
        <?php echo $leftContent; ?>
        <!--  PaPER WRaP -->
        <div class="wrap-fluid" style="margin-left:250px">
            <div class="container-fluid paper-wrap bevel tlbr">
                <!-- CONTENT -->
                <!-- BREaDCRUMB -->
                <div id="breadcrumb">
                    <div class="pull-left dis-left">
                        <H3>项目信息</H3>
                    </div>
                    <ul class="pull-right dis-left">
                        <li>
                            <span class="entypo-home"></span>
                        </li>
                        <li><i class="fa fa-lg fa-angle-right"></i>
                        </li>
                        <li><a href="./index.php?r=backend/admin/index">首页</a>
                        </li>
                        <li><i class="fa fa-lg fa-angle-right"></i>
                        </li>
                        <li><a href="#">项目</a>
                        </li>
                        <li><i class="fa fa-lg fa-angle-right"></i>
                        </li>
                        <li><a href="./index.php?r=backend/project/project">项目管理</a>
                        </li>
                        <li><i class="fa fa-lg fa-angle-right"></i>
                        </li>
                        <li><a href="#">项目信息</a>
                        </li>
                    </ul>
                </div>
                <div class="content-wrap">
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="body-nest" id="basic">
                                <div class="form_center">
                                    <form role="form" id="add_project_form" method="post" enctype="multipart/form-data" class="form-horizontal">
                                        <div class="form-group" style="display:none;">
                                            <div class="col-sm-5 control-label"> <label for="project_id">项目ID:</label></div>
                                            <div class="col-sm-3"><input type="text" placeholder="" id="project_id" name ="project_id" class="form-control" value="<?php
                                                if (isset($project_info)) {
                                                    echo $project_info->project_id;
                                                } else {
                                                    echo 0;
                                                }
                                                ?>"></div>
                                            <div class="col-sm-3 "></div>
                                        </div>
                                        <div class="form-group">
                                            <div class="col-sm-5 control-label"> <label for="project_name">合同号:</label></div>
                                            <div class="col-sm-3"><input type="text" id="project_name" name ="project_name" class="form-control" placeholder="请输入合同号" value="<?php
                                                if (isset($project_info)) {
                                                    echo $project_info->project_name;
                                                }
                                                ?>"></div>
                                            <div class="col-sm-3 star" id="project_name_info">*</div>
                                        </div>
                                        <div class="form-group">
                                            <div class="col-sm-5 control-label"> <label for="project_address">项目名称:</label></div>
                                            <div class="col-sm-3"><input type="text" id="project_address" name ="project_address" class="form-control" placeholder="请输入项目名称" value="<?php
                                                if (isset($project_info)) {
                                                    echo $project_info->project_address;
                                                }
                                                ?>"></div>
                                            <div class="col-sm-3 star" id="project_address_info">*</div>
                                        </div>
                                        <div class="form-group">
                                            <div class="col-sm-5 control-label"><label for="project_descript">项目描述:</label>    </div>
                                            <div class="col-sm-3">
                                                <textarea rows="9" style="width:100%;color:#8FBDE6;text-indent:0em" id="project_descript" name="project_descript"><?php
                                                    if (isset($project_info)) {
                                                        echo $project_info->project_descript;
                                                    } else {
                                                        echo "";
                                                    }
                                                    ?></textarea> 
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <div class="col-sm-5 control-label"> <label for="area">项目面积:</label></div>
                                            <div class="col-sm-3"><input type="text" id="area" name ="area" class="form-control" placeholder="请输入项目面积(m²)" value="<?php
                                                if (isset($project_info)) {
                                                    echo $project_info->area;
                                                }
                                                ?>"></div>
                                            <div class="col-sm-3 star" id="area_info">*</div>
                                        </div>
                                        <div class="form-group">
                                            <div class="col-sm-5 control-label"> <label for="house_type">项目户型:</label></div>
                                            <div class="col-sm-3"><input type="text" id="house_type" name ="house_type" class="form-control" placeholder="请输入项目户型" value="<?php
                                                if (isset($project_info)) {
                                                    echo $project_info->house_type;
                                                }
                                                ?>"></div>
                                            <div class="col-sm-3 star" id="house_type_info">*</div>
                                        </div>
                                        <div class="form-group">
                                            <div class="col-sm-5 control-label"> <label for="decoration_budget">装修预算:</label></div>
                                            <div class="col-sm-3"><input type="text" id="decoration_budget" name ="decoration_budget" class="form-control" placeholder="请输入项目装修预算(元)" value="<?php
                                                if (isset($project_info)) {
                                                    echo $project_info->decoration_budget;
                                                }
                                                ?>"></div>
                                            <div class="col-sm-3 star" id="decoration_budget_info">*</div>
                                        </div>
                                        <div class="form-group">
                                            <div class="col-sm-5 control-label"><label for="_administrator_id">负责人:</label></div>
                                            <div class="col-sm-3"> 
                                                <select id="_administrator_id" name="_administrator_id" class="form-control">
                                                    <option value="0">******请选择******</option>
                                                    <?php foreach ($administrator_info as $k => $l) { ?>
                                                        <option value="<?php echo $l->administrator_id; ?>"><?php echo $l->name; ?></option>
                                                    <?php } ?>    
                                                </select>
                                            </div>
                                            <div class="col-sm-3 star" id="_administrator_id_info">*</div>
                                        </div> 

                                        <div class="form-group">
                                            <div class="col-sm-5 control-label"><label for="user_id">业主:</label></div>
                                            <div class="col-sm-3"> 
                                                <select id="user_id" name="user_id[]" class="form-control" multiple="multiple" style="padding:0px;text-align: left;">
                                                    <?php
                                                    foreach ($user_info as $k => $l) {
                                                        ?>
                                                        <option value="<?php echo $l->user_id; ?>"><?php echo $l->name . "(" . $l->cellphone . ")"; ?></option>
                                                    <?php } ?>
                                                </select>   
                                            </div>
                                            <div class="col-sm-3 star" id="user_id_info">*</div>
                                        </div>

                                        <div class="form-group">
                                            <div class="col-sm-5 control-label"> <label for="contract_time">签约时间:</label></div>
                                            <div class="col-sm-3"><input type="text" id="contract_time" name ="contract_time" class="form-control laydate-icon" onclick="laydate()" placeholder="请输入签约时间" value="<?php
                                                if (isset($project_info)) {
                                                    echo $project_info->contract_time;
                                                }
                                                ?>">
                                            </div>
                                            <div class="col-sm-3 star" id="contract_time_info">*</div>
                                        </div>
                                        <div class="form-group" <?php
                                        if (!isset($project_info)) {
                                            echo "style='display:none;'";
                                        }
                                        ?>>
                                            <div class="col-sm-5 control-label">
                                                <label for="complete_add">项目完成图片:</label>
                                            </div>
                                            <div class="col-sm-3"> 
                                                <input type="file" name="complete_add" id="complete_add" /> 
                                            </div>
                                        </div> 

                                        <div class="form-group" style="margin-top: 30px;">
                                            <div class="col-sm-5"></div>
                                            <div class="col-sm-3"> <button class="btn btn-info" type="button" id="save"  style="width: 100%;outline:none;">保存</button></div>
                                            <div class="col-sm-3" style="margin-top:5px;"> <span id="info"></span></div>
                                        </div>
                                    </form>  
                                </div>

                            </div>
                        </div>
                    </div>
                </div>
                <!--  / DEVICE MaNaGER -->
                <!-- FOOTER -->

                <div id="footer">
                    <div class="devider-footer-left"></div>
                    <div class="time">
                        <p id="spanDate">
                        <p id="clock">
                    </div>
                    <div class="copyright">Copyright © 2017-2018
                        <span class="entypo-heart"></span><a href="#">优自在装修</a>. All rights reserved.
                    </div>
                </div>
                <!-- / END OF FOOTER -->
            </div>
        </div>
        <!--  END OF PaPER WRaP -->
    </body>
</html>

