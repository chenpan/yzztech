<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <title><?php echo Yii::app()->session['website_name']; ?></title> 
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta name="description" content="">
        <meta name="author" content="">
        <?php echo $recommend; ?>
        <style type="text/css">
            canvas#canvas4 {
                position: relative;
                top: 20px;
            }
            input[type="search"]{
                padding-top:5px;
                padding-bottom: 5px;
            }

            .content-wrap{
                background-color: #FFF;
                margin: 10px;
                padding-top: 20px;
                padding-bottom: 20px;
                border-radius: 3px;
            }
            .menulist{
                margin-top: 25px;
            }
            #alreadytable_wrapper section{
                border:1px #f5f5f5  solid;
                outline:none;

            }
        </style>
        <script type="text/javascript">
            $(function() {
                $('#projecttable').dataTable({
                    stateSave: true,
                    "pagingType": "input",
                    "language": {
                        "lengthMenu": "每页 _MENU_ 条记录",
                        "zeroRecords": "没有找到记录",
                        "info": "第 _PAGE_ 页 ( 总共 _PAGES_ 页 )",
                        "infoEmpty": "无记录",
                        "infoFiltered": "(从 _MAX_ 条记录过滤)",
                        "search": ""
                    }
                });
                $("#project-open").css("display", "block");

                $("#logout").click(function() {
                    if (confirm("确定退出？"))
                    {
                        window.location.href = "./index.php?r=backend/admin/Logout";
                    }
                });
                $("#addproject").click(function() {
                    window.location.href = "./index.php?r=backend/project/addproject";
                });
                if ('<?php echo $add_project; ?>' == "hidden") {
                    $("#addproject").parent().parent().parent().hide();
                }
            });
            function deleteproject(project_id, project_name) {
                if ('<?php echo $delete_project; ?>' == "") {
                    if (confirm("确认删除 " + project_name + " 项目?")) {
                        $.post("./index.php?r=backend/project/deleteproject", {project_id: project_id}, function(datainfo) {
                            var data = eval("(" + datainfo + ")");
                            if (data.data == "success") {
                                alert("删除成功！");
                                window.location.href = "./index.php?r=backend/project/project";
                            } else
                                alert("删除失败！");
                        });
                    }
                } else if ('<?php echo $delete_project; ?>' == "hidden") {
                    window.location.href = './index.php?r=backend/nonPrivilege/index';
                }
            }

            function editproject(project_id) {
                if ('<?php echo $edit_project; ?>' == "") {
                    window.location.href = './index.php?r=backend/project/addproject&project_id=' + project_id;
                } else if ('<?php echo $edit_project; ?>' == "hidden") {
                    window.location.href = './index.php?r=backend/nonPrivilege/index';
                }
            }
        </script>
        <!-- HTML5 shim, for IE6-8 support of HTML5 elements -->
        <!--[if lt IE 9]>
            <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
            <![endif]-->
        <!-- Fav and touch icons -->
    </head>
    <body> 
        <?php echo $leftContent; ?>
        <!--  PaPER WRaP -->
        <div class="wrap-fluid" style="margin-left:250px">
            <div class="container-fluid paper-wrap bevel tlbr">
                <!-- CONTENT -->
                <!-- BREaDCRUMB -->
                <div id="breadcrumb">
                    <div class="pull-left dis-left">
                        <H3>项目列表</H3>
                    </div>
                    <ul class="pull-right dis-left">
                        <li>
                            <span class="entypo-home"></span>
                        </li>
                        <li><i class="fa fa-lg fa-angle-right"></i>
                        </li>
                        <li><a href="./index.php?r=backend/admin/index">首页</a>
                        </li>
                        <li><i class="fa fa-lg fa-angle-right"></i>
                        </li>
                        <li><a href="#">项目</a>
                        </li>
                        <li><i class="fa fa-lg fa-angle-right"></i>
                        </li>
                        <li><a href="./index.php?r=backend/project/project">项目管理</a>
                        </li>
                    </ul>
                </div>
                <div class="content-wrap">
                    <div class="row">
                        <div class="col-lg-12">
                            <input type="button" class="btn btn-success btn-set" id="addproject" value="新增项目">
                        </div>
                    </div>
                </div>
                <div class="content-wrap">
                    <div class="row">
                        <div class="col-lg-12">
                            <table id="projecttable" class="display">
                                <thead>
                                    <tr class="th">
                                        <th style="padding-left: 10px;">序列</th>
                                        <th>合同号</th>
                                        <th>项目名称</th>
                                        <th>项目描述</th>
                                        <th>项目面积</th>
                                        <th>项目户型</th>
                                        <th>装修预算</th>
                                        <th>签约时间</th>
                                        <th>创建时间</th>
                                        <th>负责人</th>
                                        <th>业主</th>
                                        <th>当前阶段</th>
                                        <th>操作</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php foreach ($project_info as $K => $V) {
                                        ?>
                                        <tr>
                                            <td style="padding-left: 13px;"><?php echo $K + 1; ?></td>
                                            <td><?php echo $V->project_name; ?></td>
                                            <td><?php echo $V->project_address; ?></td>
                                            <td><?php echo $V->project_descript; ?></td>
                                            <td><?php echo $V->area; ?> m² </td>
                                            <td><?php echo $V->house_type; ?></td>
                                            <td><?php echo $V->decoration_budget; ?> 元</td>
                                            <td><?php echo $V->contract_time; ?></td>
                                            <td><?php echo $V->create_time; ?></td>
                                            <td><?php
                                                $administrator_model = administrator::model();
                                                $administrator_info = $administrator_model->findByPk(Yii::app()->session['administrator_id']);
                                                if (count($administrator_info) != 0) {
                                                    echo $administrator_info->name;
                                                } else {
                                                    echo "暂无";
                                                }
                                                ?>
                                            </td>
                                            <td><?php
                                                $project_userid_model = project_userid::model();
                                                $user_model = user::model();
                                                $project_userid_info = $project_userid_model->findAll("project_id = " . $V->project_id);
                                                $str = "";
                                                foreach ($project_userid_info as $k => $l) {
                                                    $user_info = $user_model->findByPk($l->_user_id);
                                                    if (count($user_info) != 0) {
                                                        $str .=$user_info->name . ",";
                                                    }
                                                }
                                                $str = substr($str, 0, -1);
                                                echo $str;
                                                ?>
                                            </td>
                                            <td><?php
                                                $project_schedule_model = project_schedule::model();
                                                $project_schedule_info = $project_schedule_model->find(array("condition" => "_project_id = " . $V->project_id . " AND status = 1 AND isdeleted = 0", "order" => "step DESC"));
                                                if (count($project_schedule_info) != 0) {
                                                    echo $project_schedule_info->schedule_name;
                                                } else {
                                                    $project_schedule_infos = $project_schedule_model->find(array("condition" => "_project_id = " . $V->project_id . " AND status = 0 AND isdeleted = 0", "order" => "step DESC"));
                                                    if (count($project_schedule_infos) != 0) {
                                                        echo "未开始";
                                                    } else {
                                                        echo "已竣工";
                                                    }
                                                }
                                                ?>
                                            </td>
                                            <td>
                                                <a class="edit_btn" href="#" onclick="editproject(<?php echo $V->project_id; ?>)"><span class="label label-success">编辑</span></a>
                                                <a href="#" class="delete_btn" onclick="deleteproject(<?php echo $V->project_id; ?>, '<?php echo $V->project_name; ?>')" ><span class="label label-success">删除</span></a>
                                                <a class="schedule_btn" href="./index.php?r=backend/project/project_schedule&project_id=<?php echo $V->project_id; ?>"><span class="label label-success">项目节点</span></a>
                                            </td>
                                        </tr>
                                    <?php } ?>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>

                <!-- FOOTER -->

                <div id="footer">
                    <div class="devider-footer-left"></div>
                    <div class="time">
                        <p id="spanDate">
                        <p id="clock">
                    </div>
                    <div class="copyright">Copyright © 2017-2018
                        <span class="entypo-heart"></span><a href="#">优自在装修</a>. All rights reserved.
                    </div>
                </div>
                <!-- / END OF FOOTER -->
            </div>
        </div>
        <!--  END OF PaPER WRaP -->
    </body>

</html>

