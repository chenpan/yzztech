<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <title><?php echo Yii::app()->session['website_name']; ?></title> 
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta name="description" content="">
        <meta name="author" content="">
        <?php echo $recommend; ?>
        <script src="./css/dropzone/dropzone.js"></script>
        <link rel="stylesheet" href="./css/dropzone/dropzone.css">
        <style type="text/css">
            canvas#canvas4 {
                position: relative;
                top: 20px;
            }
            input[type="search"]{
                padding-top:5px;
                padding-bottom: 5px;
            }

            .content-wrap{
                background-color: #FFF;
                margin: 10px;
                padding-top: 20px;
                padding-bottom: 20px;
                border-radius: 3px;
            }
            .menulist{
                margin-top: 25px;
            }
            #alreadytable_wrapper section{
                border:1px #f5f5f5  solid;
                outline:none;
            }
        </style>
        <script type="text/javascript">
            $(function() {
                $("#logout").click(function() {
                    if (confirm("确定退出？")) {
                        window.location.href = "./index.php?r=backend/admin/Logout";
                    }
                });
                $("#project-open").css("display", "block");
                Dropzone.autoDiscover = false;
                $("#myDropzone").dropzone({
                    url: "./index.php?r=backend/project/save_project_schedule_picture", //必须填写
                    method: "post", //也可用put
                    paramName: "Filedata", //默认为file
                    maxFiles: <?php echo $file_count; ?>, //一次性上传的文件数量上限
                    maxFilesize: 20, //MB
                    acceptedFiles: ".jpg,.gif,.png,.jpeg", //上传的类型
                    previewsContainer: "#myDropzone", //显示的容器
                    parallelUploads: 3,
                    dictMaxFilesExceeded: "您最多只能上传<?php echo $file_count; ?>个文件！",
                    dictResponseError: '文件上传失败!',
                    dictInvalidFileType: "你不能上传该类型文件,文件类型只能是*.jpg,*.gif,*.png,*.jpeg。",
                    dictFallbackMessage: "浏览器不受支持",
                    dictFileTooBig: "文件过大上传文件最大支持.",
//        previewTemplate: document.querySelector('#myDropzone').innerHTML,//设置显示模板
//        init: function() {
//            this.on("addedfile", function(file) {
//                alert("上传时");
//                //上传文件时触发的事件
//            });
//            this.on("queuecomplete", function(file) {
//                alert("上传完成");
//                //上传完成后触发的方法
//            });
//            this.on("removedfile", function(file) {
//                alert("删除文件");
//                //删除文件时触发的方法
//            });
//        }
                });
            });
        </script>
        <!-- HTML5 shim, for IE6-8 support of HTML5 elements -->
        <!--[if lt IE 9]>
            <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
            <![endif]-->
        <!-- Fav and touch icons -->
    </head>
    <body> 
        <?php echo $leftContent; ?>
        <!--  PaPER WRaP -->
        <div class="wrap-fluid" style="margin-left:250px">
            <div class="container-fluid paper-wrap bevel tlbr">
                <!-- CONTENT -->
                <!-- BREaDCRUMB -->
                <div id="breadcrumb">
                    <div class="pull-left dis-left">
                        <H3>新增项目节点图片</H3>
                    </div>
                    <ul class="pull-right dis-left">
                        <li>
                            <span class="entypo-home"></span>
                        </li>
                        <li><i class="fa fa-lg fa-angle-right"></i>
                        </li>
                        <li><a href="./index.php?r=backend/admin/index">首页</a>
                        </li>
                        <li><i class="fa fa-lg fa-angle-right"></i>
                        </li>
                        <li><a href="#">项目</a>
                        </li>
                        <li><i class="fa fa-lg fa-angle-right"></i>
                        </li>
                        <li><a href="./index.php?r=backend/project/project">项目管理</a>
                        </li>
                        <li><i class="fa fa-lg fa-angle-right"></i>
                        </li>
                        <li><a href="./index.php?r=backend/project/project_schedule&project_id=<?php echo $project_id; ?>">项目节点信息</a>
                        </li>
                        <li><i class="fa fa-lg fa-angle-right"></i>
                        </li>
                        <li><a href="#">新增项目节点图片</a>
                        </li>
                    </ul>
                </div>
                <div class="content-wrap">
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="body-nest" id="basic">
                                <div class="form_center">
                                    <form class="dropzone" name="myDropzone" id="myDropzone"  style="height:200px">
                                        <input type="text" style="display:none" name="project_schedule_id" value="<?php echo $project_schedule_id; ?>">
                                        <div class="am-text-success dz-message">
                                            将文件拖拽到此处<br>或点此打开文件管理器选择文件
                                        </div>
                                    </form>  
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!--  / DEVICE MaNaGER -->
                <!-- FOOTER -->

                <div id="footer">
                    <div class="devider-footer-left"></div>
                    <div class="time">
                        <p id="spanDate">
                        <p id="clock">
                    </div>
                    <div class="copyright">Copyright © 2017-2018
                        <span class="entypo-heart"></span><a href="#">优自在装修</a>. All rights reserved.
                    </div>
                </div>
                <!-- / END OF FOOTER -->
            </div>
        </div>
        <!--  END OF PaPER WRaP -->
    </body>
</html>

