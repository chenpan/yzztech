<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <title><?php echo Yii::app()->session['website_name']; ?></title> 
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta name="description" content="">
        <meta name="author" content="">
        <?php echo $recommend; ?>
        <link href="./css/PictureDisplay/css/index.css" rel="stylesheet">
        <script type="text/javascript" src="./css/PictureDisplay/js/jquery.fancybox.js "></script>
        <style type="text/css">
            canvas#canvas4 {
                position: relative;
                top: 20px;
            }
            input[type="search"]{
                padding-top:5px;
                padding-bottom: 5px;
            }

            .content-wrap{
                background-color: #FFF;
                margin: 10px;
                padding-top: 20px;
                padding-bottom: 20px;
                border-radius: 3px;
            }
            .menulist{
                margin-top: 25px;
            }
            #alreadytable_wrapper section{
                border:1px #f5f5f5  solid;
                outline:none;
            }
        </style>
        <script type="text/javascript">
            $(function() {
                $("#logout").click(function() {
                    if (confirm("确定退出？")) {
                        window.location.href = "./index.php?r=backend/admin/Logout";
                    }
                });

                $("#project-open").css("display", "block");

                $('.fancy').fancybox();
                $('.fancybox-thumbs').fancybox({
                    prevEffect: 'none',
                    nextEffect: 'none',
                    closeBtn: false,
                    arrows: false,
                    nextClick: true,
                    helpers: {
                        thumbs: {
                            width: 50,
                            height: 50
                        }
                    }
                });


                $("#picture_descript").val("<?php
        if (isset($project_schedule_picture_info) && $project_schedule_picture_info->picture_descript != "暂无") {
            echo $project_schedule_picture_info->picture_descript;
        } else {
            echo "";
        }
        ?>");

                $("#save").click(function() {
                    if ($("#picture_name").val() == "" || $("#picture_name").val() == null) {
                        reback();
                        $("#project_name_info").text("请输入图片名称！");
                        return false;
                    } else {
                        reback();
                        if (confirm("确认保存？")) {
                            edit_project_schedule_picture_form.submit();
                        }
                    }
                });
            });
            function reback() {
                $("#picture_name_info").text("*");
                $("#info").text("");
            }
        </script>
        <!-- HTML5 shim, for IE6-8 support of HTML5 elements -->
        <!--[if lt IE 9]>
            <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
            <![endif]-->
        <!-- Fav and touch icons -->
    </head>
    <body> 
        <?php echo $leftContent; ?>
        <!--  PaPER WRaP -->
        <div class="wrap-fluid" style="margin-left:250px">
            <div class="container-fluid paper-wrap bevel tlbr">
                <!-- CONTENT -->
                <!-- BREaDCRUMB -->
                <div id="breadcrumb">
                    <div class="pull-left dis-left">
                        <H3>编辑项目节点图片</H3>
                    </div>
                    <ul class="pull-right dis-left">
                        <li>
                            <span class="entypo-home"></span>
                        </li>
                        <li><i class="fa fa-lg fa-angle-right"></i>
                        </li>
                        <li><a href="./index.php?r=backend/admin/index">首页</a>
                        </li>
                        <li><i class="fa fa-lg fa-angle-right"></i>
                        </li>
                        <li><a href="#">项目</a>
                        </li>
                        <li><i class="fa fa-lg fa-angle-right"></i>
                        </li>
                        <li><a href="./index.php?r=backend/project/project">项目管理</a>
                        </li>
                        <li><i class="fa fa-lg fa-angle-right"></i>
                        </li>
                        <li><a href="./index.php?r=backend/project/project_schedule&project_id=<?php echo $project_id; ?>">项目节点信息</a>
                        </li>
                        <li><i class="fa fa-lg fa-angle-right"></i>
                        </li>
                        <li><a href="#">编辑项目节点图片</a>
                        </li>
                    </ul>
                </div>
                <div class="content-wrap">
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="body-nest" id="basic">
                                <div class="form_center">
                                    <form role="form" id="edit_project_schedule_picture_form" method="post" enctype="multipart/form-data" class="form-horizontal">
                                        <div class="form-group" style="display:none;">
                                            <div class="col-sm-5 control-label"> <label for="picture_id">ID:</label></div>
                                            <div class="col-sm-3"><input type="text" placeholder="" id="picture_id" name ="picture_id" class="form-control" value="<?php
                                                if (isset($project_schedule_picture_info)) {
                                                    echo $project_schedule_picture_info->picture_id;
                                                } else {
                                                    echo 0;
                                                }
                                                ?>"></div>
                                            <div class="col-sm-3 "></div>
                                        </div>
                                        <div class="form-group" style="display:none;">
                                            <div class="col-sm-5 control-label"> <label for="project_id">ID:</label></div>
                                            <div class="col-sm-3"><input type="text" placeholder="" id="project_id" name ="project_id" class="form-control" value="<?php
                                                echo $project_id;
                                                ?>"></div>
                                            <div class="col-sm-3 "></div>
                                        </div>

                                        <div class="form-group">
                                            <div class="col-sm-5 control-label"> <label for="picture_name">图片名称:</label></div>
                                            <div class="col-sm-3"><input type="text" id="picture_name" name ="picture_name" class="form-control" placeholder="请输入图片名称" value="<?php
                                                if (isset($project_schedule_picture_info) && $project_schedule_picture_info->picture_name != "暂无") {
                                                    echo $project_schedule_picture_info->picture_name;
                                                }
                                                ?>"></div>
                                            <div class="col-sm-3 star" id="picture_name_info">*</div>
                                        </div>
                                        <div class="form-group">
                                            <div class="col-sm-5 control-label"><label for="picture_descript">图片描述:</label>    </div>
                                            <div class="col-sm-3"> <textarea rows="9" style="width:100%;color:#8FBDE6;text-indent:1em" id="picture_descript" name="picture_descript"></textarea>    </div>
                                        </div>
                                        <div class="form-group" style="margin-top: 30px;">
                                            <div class="col-sm-5"></div>
                                            <div class="col-sm-3"> <button class="btn btn-info" type="button" id="save"  style="width: 100%;outline:none;">保存</button></div>
                                            <div class="col-sm-3" style="margin-top:5px;"> <span id="info"></span></div>
                                        </div>
                                        <div class="form-group">
                                            <div class="col-sm-5 control-label"><label for="picture">图片:</label></div>
                                            <div class="col-sm-3">
                                                <a class="fancy"  href="<?php echo $project_schedule_picture_info->picture_address; ?>" data-fancybox-group="gallery">
                                                    <img style = "width:370px;height:500px" class="thumbnails" src="<?php echo $project_schedule_picture_info->picture_address; ?>"/>
                                                </a>
                                            </div>
                                        </div>
                                    </form>  
                                </div>

                            </div>
                        </div>
                    </div>
                </div>
                <!--  / DEVICE MaNaGER -->
                <!-- FOOTER -->

                <div id="footer">
                    <div class="devider-footer-left"></div>
                    <div class="time">
                        <p id="spanDate">
                        <p id="clock">
                    </div>
                    <div class="copyright">Copyright © 2017-2018
                        <span class="entypo-heart"></span><a href="#">优自在装修</a>. All rights reserved.
                    </div>
                </div>
                <!-- / END OF FOOTER -->
            </div>
        </div>
        <!--  END OF PaPER WRaP -->
    </body>
</html>

