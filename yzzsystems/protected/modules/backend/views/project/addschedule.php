<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <title><?php echo Yii::app()->session['website_name']; ?></title> 
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta name="description" content="">
        <meta name="author" content="">
        <?php echo $recommend; ?>
        <link rel="stylesheet" href="./css/multiselect/css/bootstrap-3.0.3.min.css" type="text/css">
        <link rel="stylesheet" href="./css/multiselect/css/bootstrap-multiselect.css" type="text/css">
        <link rel="stylesheet" href="./css/multiselect/css/prettify.css" type="text/css">
        <link href="./summernote/summernote.css" rel="stylesheet">
        <script src="./summernote/summernote.js"></script>
        <script type="text/javascript" src="./css/multiselect/js/bootstrap-multiselect.js"></script>
        <script type="text/javascript" src="./css/multiselect/js/prettify.js"></script>
        <style type="text/css">
            canvas#canvas4 {
                position: relative;
                top: 20px;
            }
            input[type="search"]{
                padding-top:5px;
                padding-bottom: 5px;
            }

            .content-wrap{
                background-color: #FFF;
                margin: 10px;
                padding-top: 20px;
                padding-bottom: 20px;
                border-radius: 3px;
            }
            .menulist{
                margin-top: 25px;
            }
            #alreadytable_wrapper section{
                border:1px #f5f5f5  solid;
                outline:none;
            }
        </style>
        <script type="text/javascript">
            $(function() {
                $("#logout").click(function() {
                    if (confirm("确定退出？"))
                    {
                        window.location.href = "./index.php?r=backend/admin/Logout";
                    }
                });

                $('#schedule_descriptx').summernote({
                    height: 400,
                    minHeight: 300,
                    maxHeight: 500,
                    focus: true,
                    toolbar: [
                        // [groupName, [list of button]]
                        ['style', ['bold', 'italic', 'underline', 'clear']],
                        ['font', ['strikethrough', 'superscript', 'subscript']],
                        ['fontsize', ['fontsize']],
                        ['color', ['color']],
//                        ['para', ['ul', 'ol', 'paragraph']],
//                        ['height', ['height']]
                    ]
                });

                $('#schedule_descriptx').summernote('code', '<?php
        if (isset($schedule_info)) {
            echo $schedule_info->schedule_descript;
        } else {
            echo " ";
        }
        ?>');


                $('#user_id').multiselect({
                    includeSelectAllOption: true,
                    enableFiltering: true,
                    maxHeight: 150
                });
                $("#setting_open").css("display", "block");


                $('#user_id').multiselect('select', [<?php
        if (isset($user_id_str)) {
            echo $user_id_str;
        } else {
            echo "0";
        }
        ?>]);


                $("#save").click(function() {
                    if ($("#schedule_name").val() == "" || $("#schedule_name").val() == null) {
                        reback();
                        $("#schedule_name_info").text("请输入节点名称！");
                        return false;
                    } else {
                        reback();
                        if (confirm("确认保存？")) {
                            $("#schedule_descript").val($('#schedule_descriptx').summernote('code'));
                            add_schedule_form.submit();
                        }
                    }
                });
            });
            function reback() {
                $("#schedule_name_info,#schedule_address_info,#_administrator_id_info").text("*");
                $("#info").text("");
            }
        </script>
        <!-- HTML5 shim, for IE6-8 support of HTML5 elements -->
        <!--[if lt IE 9]>
            <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
            <![endif]-->
        <!-- Fav and touch icons -->
    </head>
    <body> 
        <?php echo $leftContent; ?>
        <!--  PaPER WRaP -->
        <div class="wrap-fluid" style="margin-left:250px">
            <div class="container-fluid paper-wrap bevel tlbr">
                <!-- CONTENT -->
                <!-- BREaDCRUMB -->
                <div id="breadcrumb">
                    <div class="pull-left dis-left">
                        <H3>节点信息</H3>
                    </div>
                    <ul class="pull-right dis-left">
                        <li>
                            <span class="entypo-home"></span>
                        </li>
                        <li><i class="fa fa-lg fa-angle-right"></i>
                        </li>
                        <li><a href="./index.php?r=backend/admin/index">首页</a>
                        </li>
                        <li><i class="fa fa-lg fa-angle-right"></i>
                        </li>
                        <li><a href="#">项目</a>
                        </li>
                        <li><i class="fa fa-lg fa-angle-right"></i>
                        </li>
                        <li><a href="./index.php?r=backend/project/schedule">节点管理</a>
                        </li>
                        <li><i class="fa fa-lg fa-angle-right"></i>
                        </li>
                        <li><a href="#">节点信息</a>
                        </li>
                    </ul>
                </div>
                <div class="content-wrap">
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="body-nest" id="basic">
                                <div class="form_center">
                                    <form role="form" id="add_schedule_form" method="post" enctype="multipart/form-data" class="form-horizontal">
                                        <div class="form-group" style="display:none;">
                                            <div class="col-sm-3 control-label"> <label for="schedule_id">节点ID:</label></div>
                                            <div class="col-sm-5"><input type="text" placeholder="" id="schedule_id" name ="schedule_id" class="form-control" value="<?php
                                                if (isset($schedule_info)) {
                                                    echo $schedule_info->schedule_id;
                                                } else {
                                                    echo 0;
                                                }
                                                ?>"></div>
                                            <div class="col-sm-3"></div>
                                        </div>
                                        <div class="form-group">
                                            <div class="col-sm-3 control-label"> <label for="schedule_name">节点名称:</label></div>
                                            <div class="col-sm-5"><input type="text" id="schedule_name" name ="schedule_name" class="form-control" placeholder="请输入节点名称" value="<?php
                                                if (isset($schedule_info)) {
                                                    echo $schedule_info->schedule_name;
                                                }
                                                ?>"></div>
                                            <div class="col-sm-3 star" id="schedule_name_info">*</div>
                                        </div>                                        
                                        <div class="form-group">
                                            <div class="col-sm-3 control-label"><label for="schedule_descriptx">节点描述:</label></div>
                                            <div class="col-sm-5"> 
                                                <div id="schedule_descriptx" name="schedule_descriptx"></div>
                                                <input type="text" id="schedule_descript" name ="schedule_descript" style="display:none" class="form-control">
                                            </div>
                                        </div>  

                                        <div class="form-group">
                                            <div class="col-sm-3 control-label">
                                                <label for="not_begin_pic_add">节点未开始图标:</label>
                                            </div>
                                            <div class="col-sm-5"> 
                                                <input type="file" name="not_begin_pic_add" id="not_begin_pic_add" /> 
                                            </div>
                                        </div> 
                                        <div class="form-group">
                                            <div class="col-sm-3 control-label">
                                                <label for="begin_pic_add">节点开始图标:</label>
                                            </div>
                                            <div class="col-sm-5"> 
                                                <input type="file" name="begin_pic_add" id="begin_pic_add" /> 
                                            </div>
                                        </div> 

                                        <div class="form-group" style="margin-top: 30px;">
                                            <div class="col-sm-3"></div>
                                            <div class="col-sm-5"> <button class="btn btn-info" type="button" id="save"  style="width: 100%;outline:none;">保存</button></div>
                                            <div class="col-sm-3" style="margin-top:5px;"> <span id="info"></span></div>
                                        </div>
                                    </form>  
                                </div>

                            </div>
                        </div>
                    </div>
                </div>
                <!--  / DEVICE MaNaGER -->
                <!-- FOOTER -->

                <div id="footer">
                    <div class="devider-footer-left"></div>
                    <div class="time">
                        <p id="spanDate">
                        <p id="clock">
                    </div>
                    <div class="copyright">Copyright © 2017-2018
                        <span class="entypo-heart"></span><a href="#">优自在装修</a>. All rights reserved.
                    </div>
                </div>
                <!-- / END OF FOOTER -->
            </div>
        </div>
        <!--  END OF PaPER WRaP -->
    </body>
</html>

