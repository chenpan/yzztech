<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <title><?php echo Yii::app()->session['website_name']; ?></title> 
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta name="description" content="">
        <meta name="author" content="">
        <?php echo $recommend; ?>
        <link href="./css/PictureDisplay/css/index.css" rel="stylesheet">
        <script type="text/javascript" src="./css/PictureDisplay/js/jquery.fancybox.js "></script>
        <style type="text/css">
            canvas#canvas4 {
                position: relative;
                top: 20px;
            }
            input[type="search"]{
                padding-top:5px;
                padding-bottom: 5px;
            }

            .content-wrap{
                background-color: #FFF;
                margin: 10px;
                padding-top: 20px;
                padding-bottom: 20px;
                border-radius: 3px;
            }
            .menulist{
                margin-top: 25px;
            }
            #alreadytable_wrapper section{
                border:1px #f5f5f5  solid;
                outline:none;

            }
        </style>
        <script type="text/javascript">
            $(function() {
                var project_schedule_picture_table = $('#project_schedule_picture_table').dataTable({
                    stateSave: true,
                    "pagingType": "input",
                    "language": {
                        "lengthMenu": "每页 _MENU_ 条记录",
                        "zeroRecords": "没有找到记录",
                        "info": "第 _PAGE_ 页 ( 总共 _PAGES_ 页 )",
                        "infoEmpty": "无记录",
                        "infoFiltered": "(从 _MAX_ 条记录过滤)",
                        "search": ""
                    }
                });
                $("#project-open").css("display", "block");

                $("#add_picture").click(function() {
                    window.location.href = "./index.php?r=backend/project/add_project_schedule_picture&project_schedule_id=<?php echo $project_schedule_id; ?>&project_id=<?php echo $project_id; ?>";
                });

                $('.fancy').fancybox();
                $('.fancybox-thumbs').fancybox({
                    prevEffect: 'none',
                    nextEffect: 'none',
                    closeBtn: false,
                    arrows: false,
                    nextClick: true,
                    helpers: {
                        thumbs: {
                            width: 50,
                            height: 50
                        }
                    }
                });

                $("#logout").click(function() {
                    if (confirm("确定退出？"))
                    {
                        window.location.href = "./index.php?r=backend/admin/Logout";
                    }
                });

                if ('<?php echo $add_picture; ?>' == "hidden") {
                    $("#add_picture").hide();
                }

            });

            function edit_project_schedule_picture(picture_id) {
                if ('<?php echo $edit_picture; ?>' == "") {
                    window.location.href = './index.php?r=backend/project/edit_project_schedule_picture&project_id=<?php echo $project_id; ?>&picture_id=' + picture_id;
                } else if ('<?php echo $edit_picture; ?>' == "hidden") {
                    window.location.href = './index.php?r=backend/nonPrivilege/index';
                }
            }

            function delete_project_schedule_picture(picture_id, picture_name) {
                if ('<?php echo $delete_picture; ?>' == "") {
                    if (confirm("确认删除 " + picture_name + " 图片?")) {
                        $.post("./index.php?r=backend/project/del_project_schedule_picture", {picture_id: picture_id}, function(datainfo) {
                            var data = eval("(" + datainfo + ")");
                            if (data.data == "success") {
                                alert("删除成功！");
                                window.location.href = "./index.php?r=backend/project/project_schedule_picture&project_schedule_id=<?php echo $project_schedule_id; ?>&project_id=<?php echo $project_id; ?>";
                            } else
                                alert("删除失败！");
                        });
                    }
                } else if ('<?php echo $delete_picture; ?>' == "hidden") {
                    window.location.href = './index.php?r=backend/nonPrivilege/index';
                }
            }
        </script>
        <!-- HTML5 shim, for IE6-8 support of HTML5 elements -->
        <!--[if lt IE 9]>
            <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
            <![endif]-->
        <!-- Fav and touch icons -->
    </head>
    <body> 
        <?php echo $leftContent; ?>
        <!--  PaPER WRaP -->
        <div class="wrap-fluid" style="margin-left:250px">
            <div class="container-fluid paper-wrap bevel tlbr">
                <!-- CONTENT -->
                <!-- BREaDCRUMB -->
                <div id="breadcrumb">
                    <div class="pull-left dis-left">
                        <H3>项目节点图片信息</H3>
                    </div>
                    <ul class="pull-right dis-left">
                        <li>
                            <span class="entypo-home"></span>
                        </li>
                        <li><i class="fa fa-lg fa-angle-right"></i>
                        </li>
                        <li><a href="./index.php?r=backend/admin/index">首页</a>
                        </li>
                        <li><i class="fa fa-lg fa-angle-right"></i>
                        </li>
                        <li><a href="#">项目</a>
                        </li>
                        <li><i class="fa fa-lg fa-angle-right"></i>
                        </li>
                        <li><a href="./index.php?r=backend/project/project">项目管理</a>
                        </li>
                        <li><i class="fa fa-lg fa-angle-right"></i>
                        </li>
                        <li><a href="./index.php?r=backend/project/project_schedule&project_id=<?php echo $project_id; ?>">项目节点信息</a>
                        </li>
                        </li>
                        <li><i class="fa fa-lg fa-angle-right"></i>
                        </li>
                        <li><a href="#">项目节点图片信息</a>
                        </li>
                    </ul>
                </div>
                <div class="content-wrap">
                    <div class="row">
                        <div class="col-lg-1">
                            <input type="button" class="btn btn-success btn-set" id="add_picture" value="新增节点图片">
                        </div>
                    </div>
                </div>
                <div class="content-wrap">
                    <div class="row">
                        <div class="col-lg-12">
                            <table id="project_schedule_picture_table" class="display">
                                <thead>
                                    <tr class="th">
                                        <th style="padding-left: 10px;">序列</th>
                                        <th>所属节点</th>
                                        <th>图片名称</th>
                                        <th>图片描述</th>
                                        <th>展示图片</th>
                                        <th>添加时间</th>
                                        <th>操作人</th>
                                        <th>操作</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php foreach ($project_schedule_picture_info as $K => $V) {
                                        ?>
                                        <tr>
                                            <td style="padding-left: 13px;"><?php echo $K + 1; ?></td>
                                            <td><?php
                                                $project_schedule_model = project_schedule::model();
                                                $project_schedule_info = $project_schedule_model->findByPk($V->_project_schedule_id);
                                                if (count($project_schedule_info) != 0) {
                                                    echo $project_schedule_info->schedule_name;
                                                } else {
                                                    echo "暂无";
                                                }
                                                ?>
                                            </td>
                                            <td><?php echo $V->picture_name; ?></td>
                                            <td><?php echo $V->picture_descript; ?></td>
                                            <td>
                                                <?php
                                                if ($V->picture_address == NULL)
                                                    echo "无";
                                                else {
                                                    echo '<a class="fancy"  href="' . $V->picture_address . '" data-fancybox-group="gallery">';
                                                    echo '<img style = "width:20px;height:20px" class="thumbnails" src="' . $V->picture_address . '"/>';
                                                    echo '</a>';
                                                }
                                                ?>
                                            </td>
                                            <td><?php echo $V->create_time; ?></td>                                           
                                            <td><?php
                                                $administrator_model = administrator::model();
                                                $administrator_info = $administrator_model->findByPk($V->creator_id);
                                                if (count($administrator_info) != 0) {
                                                    echo $administrator_info->name;
                                                } else {
                                                    echo "暂无";
                                                }
                                                ?>
                                            </td>
                                            <td>
                                                <a href="#" class="edit_btn" onclick="edit_project_schedule_picture(<?php echo $V->picture_id; ?>)" ><span class="label label-success">编辑</span></a>
                                                <a class="delete_btn" href="#" onclick="delete_project_schedule_picture(<?php echo $V->picture_id; ?>, '<?php echo $V->picture_name; ?>')"><span class="label label-success">删除</span></a>
                                            </td>
                                        </tr>
                                    <?php } ?>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>

                <!-- FOOTER -->

                <div id="footer">
                    <div class="devider-footer-left"></div>
                    <div class="time">
                        <p id="spanDate">
                        <p id="clock">
                    </div>
                    <div class="copyright">Copyright © 2017-2018
                        <span class="entypo-heart"></span><a href="#">优自在装修</a>. All rights reserved.
                    </div>
                </div>
                <!-- / END OF FOOTER -->
            </div>
        </div>
        <!--  END OF PaPER WRaP -->
    </body>

</html>

