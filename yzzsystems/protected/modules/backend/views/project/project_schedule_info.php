<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <title><?php echo Yii::app()->session['website_name']; ?></title> 
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta name="description" content="">
        <meta name="author" content="">
        <?php echo $recommend; ?>
        <link href="./summernote/summernote.css" rel="stylesheet">
        <script src="./summernote/summernote.js"></script>
        <script src="./css/laydate/laydate.js" type="text/javascript"></script>
        <style type="text/css">
            canvas#canvas4 {
                position: relative;
                top: 20px;
            }
            input[type="search"]{
                padding-top:5px;
                padding-bottom: 5px;
            }

            .content-wrap{
                background-color: #FFF;
                margin: 10px;
                padding-top: 20px;
                padding-bottom: 20px;
                border-radius: 3px;
            }
            .menulist{
                margin-top: 25px;
            }
            #alreadytable_wrapper section{
                border:1px #f5f5f5  solid;
                outline:none;
            }
        </style>
        <script type="text/javascript">
            $(function() {
                $("#logout").click(function() {
                    if (confirm("确定退出？"))
                    {
                        window.location.href = "./index.php?r=backend/admin/Logout";
                    }
                });

                $('#schedule_descriptx').summernote({
                    height: 400,
                    minHeight: 300,
                    maxHeight: 500,
                    focus: true,
                    toolbar: [
                        // [groupName, [list of button]]
                        ['style', ['bold', 'italic', 'underline', 'clear']],
                        ['font', ['strikethrough', 'superscript', 'subscript']],
                        ['fontsize', ['fontsize']],
                        ['color', ['color']],
//                        ['para', ['ul', 'ol', 'paragraph']],
//                        ['height', ['height']]
                    ]
                });

                $('#schedule_descriptx').summernote('code', '<?php
        if (isset($project_schedule_info)) {
            echo $project_schedule_info->schedule_descript;
        } else {
            echo " ";
        }
        ?>');


                $("#project-open").css("display", "block");
                $("#_master_id").val("<?php
        if (isset($project_schedule_info)) {
            echo $project_schedule_info->_master_id;
        } else {
            echo 0;
        }
        ?>");

                $("#status").val("<?php
        if (isset($project_schedule_info)) {
            echo $project_schedule_info->status;
        } else {
            echo 0;
        }
        ?>");

                $("#save").click(function() {
                    if ($("#schedule_name").val() == "" || $("#schedule_name").val() == null) {
                        reback();
                        $("#schedule_name_info").text("请输入节点名称！");
                        return false;
                    }
//                    else if ($("#completion_degree").val() == "" || $("#completion_degree").val() == null) {
//                        reback();
//                        $("#completion_degree_info").text("请输入完成度！");
//                        return false;
//                    } else if (isNaN($("#completion_degree").val())) {
//                        reback();
//                        $("#completion_degree_info").text("完成度必须为数字！");
//                        return false;
//                    } 
                    else if ($("#_master_id").val() == 0) {
                        reback();
                        $("#_master_id_info").text("请选择负责人！");
                        return false;
                    } else if ($("#status").val() == -1) {
                        reback();
                        $("#status_info").text("请选择状态！");
                        return false;
                    }
//                    else if ($("#start_time").val() == "" || $("#start_time").val() == null) {
//                        reback();
//                        $("#start_time_info").text("请输入开始日期！");
//                        return false;
//                    } else if ($("#end_time").val() != "" && $("#start_time").val() > $("#end_time").val()) {
//                        $("#end_time_info").text("请选择正确的时间！");
//                        return false;
//                    }
                    else if ($("#status").val() == 2 && ($("#deadline").val() == "" || $("#deadline").val() == null)) {
                        reback();
                        $("#deadline_info").text("请设置截止日期！");
                        return false;
                    } else {
                        reback();
                        if (confirm("确认保存？")) {
                            $("#schedule_descript").val($('#schedule_descriptx').summernote('code'));
                            project_schedule_info_form.submit();
                        }
                    }
                });
            });
            function reback() {
                $("#schedule_name_info,#_master_id_info,#status_info,#deadline_info").text("*");//completion_degree_info,#start_time_info,#end_time_info,
                $("#info").text("");
            }
        </script>
        <!-- HTML5 shim, for IE6-8 support of HTML5 elements -->
        <!--[if lt IE 9]>
            <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
            <![endif]-->
        <!-- Fav and touch icons -->
    </head>
    <body> 
        <?php echo $leftContent; ?>
        <!--  PaPER WRaP -->
        <div class="wrap-fluid" style="margin-left:250px">
            <div class="container-fluid paper-wrap bevel tlbr">
                <!-- CONTENT -->
                <!-- BREaDCRUMB -->
                <div id="breadcrumb">
                    <div class="pull-left dis-left">
                        <H3>项目信息</H3>
                    </div>
                    <ul class="pull-right dis-left">
                        <li>
                            <span class="entypo-home"></span>
                        </li>
                        <li><i class="fa fa-lg fa-angle-right"></i>
                        </li>
                        <li><a href="./index.php?r=backend/admin/index">首页</a>
                        </li>
                        <li><i class="fa fa-lg fa-angle-right"></i>
                        </li>
                        <li><a href="#">项目</a>
                        </li>
                        <li><i class="fa fa-lg fa-angle-right"></i>
                        </li>
                        <li><a href="./index.php?r=backend/project/project">项目管理</a>
                        </li>
                        <li><i class="fa fa-lg fa-angle-right"></i>
                        </li>
                        <li><a href="#">项目节点信息</a>
                        </li>
                    </ul>
                </div>
                <div class="content-wrap">
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="body-nest" id="basic">
                                <div class="form_center">
                                    <form role="form" id="project_schedule_info_form" method="post" enctype="multipart/form-data" class="form-horizontal">
                                        <div class="form-group" style="display:none;">
                                            <div class="col-sm-3 control-label"> <label for="project_schedule_id">ID:</label></div>
                                            <div class="col-sm-5"><input type="text" placeholder="" id="project_schedule_id" name ="project_schedule_id" class="form-control" value="<?php
                                                if (isset($project_schedule_info)) {
                                                    echo $project_schedule_info->project_schedule_id;
                                                } else {
                                                    echo 0;
                                                }
                                                ?>"></div>
                                            <div class="col-sm-3 "></div>
                                        </div>
                                        <div class="form-group">
                                            <div class="col-sm-3 control-label"> <label for="schedule_name">节点名称：</label></div>
                                            <div class="col-sm-5"><input type="text" id="schedule_name" name ="schedule_name" class="form-control" placeholder="请输入节点名称" value="<?php
                                                if (isset($project_schedule_info)) {
                                                    echo $project_schedule_info->schedule_name;
                                                }
                                                ?>"></div>
                                            <div class="col-sm-3 star" id="schedule_name_info">*</div>
                                        </div>
                                        <div class="form-group">
                                            <div class="col-sm-3 control-label"><label for="schedule_descriptx">节点描述:</label></div>
                                            <div class="col-sm-5"> 
                                                <div id="schedule_descriptx" name="schedule_descriptx"></div>
                                                <input type="text" id="schedule_descript" name ="schedule_descript" style="display:none" class="form-control">
                                            </div>                                       
                                        </div>
                                        <!--                                        <div class="form-group">
                                                                                    <div class="col-sm-5 control-label"> <label for="completion_degree">完成度：</label></div>
                                                                                    <div class="col-sm-3"><input type="text" id="completion_degree" name ="completion_degree" class="form-control" placeholder="请输入完成度" value="<?php
//                                        if (isset($project_schedule_info)) {
//                                            echo $project_schedule_info->completion_degree;
//                                        }
                                        ?>"></div>
                                                                                    <div class="col-sm-3 star" id="completion_degree_info">*</div>
                                                                                </div>-->
                                        <div class="form-group">
                                            <div class="col-sm-3 control-label"><label for="_master_id">负责人： </label></div>
                                            <div class="col-sm-5"> 
                                                <select id="_master_id" name="_master_id" class="form-control">
                                                    <option value="0">******请选择******</option>
                                                    <?php foreach ($master_info as $k => $l) { ?>
                                                        <option value="<?php echo $l->master_id; ?>"><?php echo $l->master_name; ?></option>
                                                    <?php } ?>    
                                                </select>
                                            </div>
                                            <div class="col-sm-3 star" id="_master_id_info">*</div>
                                        </div>
                                        <div class="form-group">
                                            <div class="col-sm-3 control-label"><label for="status">状态： </label></div>
                                            <div class="col-sm-5"> 
                                                <select id="status" name="status" class="form-control">
                                                    <option value="-1">******请选择******</option>                                                    
                                                    <option value="0">未开始</option>
                                                    <option value="1">进行中</option>
                                                    <option value="2">已完成</option>                                                    
                                                </select>
                                            </div>
                                            <div class="col-sm-3 star" id="status_info">*</div>
                                        </div>
                                        <!--                                        <div class="form-group">
                                                                                    <div class="col-sm-5 control-label"> <label for="start_time">开始日期：</label></div>
                                                                                    <div class="col-sm-3"><input type="text" class="form-control laydate-icon" id="start_time" name="start_time"
                                                                                                                 placeHolder="请设置开始日期" onclick="laydate({istime: true, format: 'YYYY-MM-DD'})"
                                                                                                                 value="<?php
//                                                                         if (isset($project_schedule_info)) {
//                                                                             echo $project_schedule_info->start_time;
//                                                                         }
                                        ?>">
                                                                                    </div>
                                                                                    <div class="col-sm-3 star" id="start_time_info">*</div>
                                                                                </div>
                                                                                <div class="form-group">
                                                                                    <div class="col-sm-5 control-label"> <label for="end_time">结束日期：</label></div>
                                                                                    <div class="col-sm-3"><input type="text" class="form-control laydate-icon" id="end_time" name="end_time"
                                                                                                                 placeHolder="请设置结束日期" onclick="laydate({istime: true, format: 'YYYY-MM-DD'})"
                                                                                                                 value="<?php
//                                                                         if (isset($project_schedule_info)) {
//                                                                             echo $project_schedule_info->end_time;
//                                                                         }
                                        ?>">
                                                                                    </div>
                                                                                    <div class="col-sm-3 star" id="end_time_info">*</div>
                                                                                </div>-->
                                        <div class="form-group">
                                            <div class="col-sm-3 control-label"> <label for="deadline">截止日期：</label></div>
                                            <div class="col-sm-5"><input type="text" class="form-control laydate-icon" id="deadline" name="deadline"
                                                                         placeHolder="请设置截止日期" onclick="laydate({istime: true, format: 'YYYY-MM-DD'})"
                                                                         value="<?php
                                                                         if (isset($project_schedule_info)) {
                                                                             if ($project_schedule_info->deadline != "0000-00-00") {
                                                                                 echo $project_schedule_info->deadline;
                                                                             } else {
                                                                                 echo "";
                                                                             }
                                                                         }
                                                                         ?>">
                                            </div>
                                            <div class="col-sm-3 star" id="deadline_info"></div>
                                        </div>
                                        <div class="form-group" style="margin-top: 30px;">
                                            <div class="col-sm-3"></div>
                                            <div class="col-sm-5"> <button class="btn btn-info" type="button" id="save"  style="width: 100%;outline:none;">保存</button></div>
                                            <div class="col-sm-3" style="margin-top:5px;"> <span id="info"></span></div>
                                        </div>
                                    </form>  
                                </div>

                            </div>
                        </div>
                    </div>
                </div>
                <!--  / DEVICE MaNaGER -->
                <!-- FOOTER -->

                <div id="footer">
                    <div class="devider-footer-left"></div>
                    <div class="time">
                        <p id="spanDate">
                        <p id="clock">
                    </div>
                    <div class="copyright">Copyright © 2017-2018
                        <span class="entypo-heart"></span><a href="#">优自在装修</a>. All rights reserved.
                    </div>
                </div>
                <!-- / END OF FOOTER -->
            </div>
        </div>
        <!--  END OF PaPER WRaP -->
    </body>
</html>

