<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <title><?php echo Yii::app()->session['website_name']; ?></title> 
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta name="description" content="">
        <meta name="author" content="">
        <?php echo $recommend; ?>
        <style type="text/css">
            canvas#canvas4 {
                position: relative;
                top: 20px;
            }
            input[type="search"]{
                padding-top:5px;
                padding-bottom: 5px;
            }

            .content-wrap{
                background-color: #FFF;
                margin: 10px;
                padding-top: 20px;
                padding-bottom: 20px;
                border-radius: 3px;
            }
            .menulist{
                margin-top: 25px;
            }
            #alreadytable_wrapper section{
                border:1px #f5f5f5  solid;
                outline:none;
            }
            #jurisdiction-open{
                display: block;
            }
            #permission-assignment{
                background: none repeat scroll 0 0 rgba(0, 0, 0, 0.3);
            }
            .btn-set{
                background-color:#76B8E6 !important;
                color:white!important;
            }
            .btn-set:hover{
                background-color:#56AFEC!important;
                color:white!important;
            }
        </style>
        <script type="text/javascript">
            $(document).ready(function() {
                var table = $('#alreadytable').dataTable({
                    "pagingType": "input",
                    "Processing": false, //datatable获取数据时候是否显示正在处理提示信息。
                    "order": [[1, "asc"]],
                    "stateSave": false,
                    "language": {
                        "lengthMenu": "每页 _MENU_ 条记录",
                        "zeroRecords": "没有找到记录",
                        "info": "第 _PAGE_ 页 ( 总共 _PAGES_ 页 )",
                        "infoEmpty": "无记录",
                        "infoFiltered": "(从 _MAX_ 条记录过滤)",
                        "search": "",
                        "Processing": "正在加载...",
                    }
                });
                $("#logout").click(function() {
                    if (confirm("确定注销？"))
                    {
                        window.location.href = "./index.php?r=backend/admin/Logout";
                    }
                });
                if ('<?php echo $add_status; ?>' == "hidden") {
                    $("#createUser").parent().parent().parent().hide();
                }
//                $(".save").click(function() {
//                    var adminID = $(this).next(".saveid").val();
//                    console.log(adminID);
//                    window.location.href = "./index.php?r=backend/auth/modify&adminID=" + adminID;
//                });
//                $(".del").click(function() {
//                    if (confirm("确定删除？")) {
//                        var adminId = $(this).next(".del").val();
//                        $.post("./index.php?r=backend/auth/assignDel", {adminId: adminId}, function(data) {
//                            if (data.data == "success")
//                            {
//                                alert("删除成功！");
//                                window.location.href = "./index.php?r=backend/auth/assignRole";
//                            } else if (data.data == "false")
//                            {
//                                alert("删除失败！");
//                                window.location.href = "./index.php?r=backend/auth/assignRole";
//                            }
//                        }, 'json');
//                    }
//                });
            });
            function editrole(id) {
                window.location.href = "./index.php?r=backend/auth/modify&adminID=" + id;
            }
            function delrole(id) {
                if (confirm("确定删除？")) {
                    $.post("./index.php?r=backend/auth/assignDel", {adminId: id}, function(data) {
                        if (data.data == "success")
                        {
                            alert("删除成功！");
                            window.location.href = "./index.php?r=backend/auth/assignRole";
                        } else if (data.data == "false")
                        {
                            alert("删除失败！");
                            window.location.href = "./index.php?r=backend/auth/assignRole";
                        }
                    }, 'json');
                }
            }
//            function auto() {
//                var left = ($("td.action").width() - $("select.form-control").outerWidth()) / 2;
//                $("select.form-control").css('margin-left', left - 10);
//            }
        </script>

        <!-- HTML5 shim, for IE6-8 support of HTML5 elements -->
        <!--[if lt IE 9]>
            <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
            <![endif]-->
        <!-- Fav and touch icons -->
    </head>
    <body> 
        <?php echo $leftContent; ?>
        <!--  PaPER WRaP -->
        <div class="wrap-fluid" style="margin-left:250px">
            <div class="container-fluid paper-wrap bevel tlbr">
                <!-- CONTENT -->
                <!-- BREaDCRUMB -->
                <div id="breadcrumb">
                    <div class="pull-left dis-left">
                        <H3>权限分配</H3>
                    </div>
                    <ul class="pull-right dis-left menulist">
                        <li>
                            <span class="entypo-home"></span>
                        </li>
                        <li><i class="fa fa-lg fa-angle-right"></i>
                        </li>
                        <li><a href="./index.php?r=backend/platform/admin/index">首页</a>
                        </li>
                        <li><i class="fa fa-lg fa-angle-right"></i>
                        </li>
                        <li>权限</li>
                        <li><i class="fa fa-lg fa-angle-right"></i>
                        </li>
                        <li><a href="./index.php?r=backend/platform/auth/role">权限分配</a>
                        </li>
                    </ul>
                </div>
                <div class="content-wrap">
                    <div class="row">
                        <div class="col-lg-12">
                            <DIV id="createUser" >
                                <A href="./index.php?r=backend/auth/addAdmin" id="createUser"><span class="btn btn-success btn-set">添加管理员</span></A>
                            </DIV>    
                        </div>
                    </div>
                </div>
                <div class="content-wrap">
                    <div class="row">
                        <div class="col-lg-12">
                            <table id="alreadytable" class="display">
                                <thead>
                                    <tr class="th">
                                        <th>序列</th>
                                        <th>用户</th>
                                        <th>角色</th>
                                        <th>手机号</th>
                                        <th>QQ</th>
                                        <th>操作</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php foreach ($administrator_info as $K => $V) {
                                        ?>
                                        <TR>
                                            <td><?php echo $K + 1; ?></td>
                                            <td><?php echo $V->name; ?></td>
                                            <td class="action">
                                                <div>
                                                    <?php foreach ($administrator_role_info as $k => $l) {
                                                        ?>
                                                        <?php
                                                        if ($l->administrator_role_id == $V->_administrator_role_id) {
                                                            echo $l->administrator_role_name;
                                                        }
                                                        ?>
                                                    <?php } ?>
                                                </div>
                                            </td>
                                            <td><?php echo $V->cellphone; ?></td>
                                            <td><?php echo $V->qq; ?></td>
                                            <td class="action">
                                                <a style="text-decoration: none;cursor: pointer;" <?php echo $edit_status; ?>  onclick="editrole(<?php echo $V->administrator_id; ?>)">
                                                    <span class="label label-success">编辑</span>
                                                </a>
                                                <a style="text-decoration: none;cursor: pointer;" <?php echo $delete_status; ?> onclick="delrole(<?php echo $V->administrator_id; ?>)">
                                                    <span class="label label-success">删除</span>
                                                </a>
                                            </td>
                                        </TR>
                                    <?php } ?>
                                </tbody>
                            </table>
                        </DIV>
                    </DIV>   
                </div>
                <!--  / DEVICE MaNaGER -->
                <!-- FOOTER -->

                <div id="footer">
                    <div class="devider-footer-left"></div>
                    <div class="time">
                        <p id="spanDate">
                        <p id="clock">
                    </div>
                    <div class="copyright">Copyright © 2017-2018
                        <span class="entypo-heart"></span><a href="#">优自在装修</a>. All rights reserved.
                    </div>
                </div>
                <!-- / END OF FOOTER -->
            </div>
        </div>
        <!--  END OF PaPER WRaP -->
    </body>

</html>

