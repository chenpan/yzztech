<!DOCTYPE html>
<html>
    <head>
        <META content="IE=11.0000" http-equiv="X-UA-Compatible">
        <META charset="UTF-8">    
        <META name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no"> 
        <TITLE><?php echo Yii::app()->session['website_name']; ?></TITLE>   

        <?php echo $recommend; ?>
        <script src="./css/bootstrap/highcharts.js" type="text/javascript"></script>

        <style type="text/css">
            canvas#canvas4 {
                position: relative;
                top: 20px;
            }
            #breadcrumb {
                width: 96.5%;
                margin-left:auto;
                margin-right: auto;
            }
            #index-open{
                background: none repeat scroll 0 0 rgba(0, 0, 0, 0.3);
            }
            table tr:hover{
                background-color: transparent;
            }
            table tr{
                border-bottom: 1px #F1F1F1 solid;
            }
            .content-wrap {
                padding: 0 30px;
                border-radius: 3px;
                margin-bottom:20px; 
            }
            .speed{
                margin-top: 30px;
            }
        </style>
        <!--         HTML5 shim, for IE6-8 support of HTML5 elements 
                [if lt IE 9]>
                    <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
                    <![endif]
                 Fav and touch icons 
                 END OF RIGHT SLIDER CONTENT-->
        <script type="text/javascript">
            $(function() {
                $("#logout").click(function() {
                    if (confirm("确定退出？"))
                    {
                        window.location.href = "./index.php?r=backend/admin/Logout";
                    }
                });
            });
        </script>
    </head>
    <body> 
        <?php echo $leftContent; ?>
        <!--PAPER WRAP--> 
        <div class="wrap-fluid" id="allscream">
            <div class="container-fluid paper-wrap bevel tlbr">
                <!--CONTENT--> 
                <!--BREADCRUMB--> 
                <div id="breadcrumb">
                    <div class="pull-left dis-left">
                        <H3><?php echo Yii::app()->session['website_name']; ?><SMALL>Version 1.0</SMALL></H3>
                    </div>
                    <ul class="pull-right dis-left">
                        <li>
                            <span class="entypo-home"></span>
                        </li>
                        <li><i class="fa fa-lg fa-angle-right"></i>
                        </li>
                        <li><a href="./index.php?r=backend/admin/index">首页</a>
                        </li>
                    </ul>
                </div>
                <!--END OF BREADCRUMB--> 
                <!--DEVICE MANAGER--> 
                <div class="content-wrap" style="margin-bottom:0px;">
                    <div class="row">
                        <div class="col-lg-3">
                            <div class="profit" id="profitClose">
                                <div class="headline">
                                    <h3>
                                        <span>
                                            <i class="maki-commerical-building"></i>&#160;&#160;项目个数(单位:个)</span>
                                    </h3>
                                </div>
                                <div class="value">
                                    <span class="pull-left"><i class="clock-position"></i></span>
                                    <?php echo $total_project; ?>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-3">
                            <div class="revenue" id="revenueClose">
                                <div class="headline">
                                    <h3>
                                        <span>
                                            <i class="fontawesome-group"></i>&#160;&#160;师傅量(单位:人)</span>
                                    </h3>
                                </div>
                                <div class="value">
                                    <span class="pull-left"><i class="gauge-position"></i></span>
                                    <?php echo $total_master; ?>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-3">
                            <div class="order" id="orderClose">
                                <div class="headline ">
                                    <h3>
                                        <span>
                                            <i class="entypo-skype-circled"></i>&#160;&#160;设计师(单位:人)</span>
                                    </h3>
                                </div>
                                <div class="value">
                                    </span><?php echo $total_designer; ?>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-3">
                            <div class="member" id="memberClose">
                                <div class="headline ">
                                    <h3>
                                        <span>
                                            <i class="maki-beer"></i>
                                            &#160;&#160;业主量(单位:人)
                                        </span>
                                    </h3>
                                </div>
                                <div class="value">
                                    </span><?php echo $total_user; ?>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!--/ DEVICE MANAGER--> 
                <div class="content-wrap">
                    <div class="row">
                        <div class="col-lg-6">
                            <div class="chart-wrap">
                                <div id="registercharts" style="width:100%;">
                                    <p>最近新增项目</p>
                                    <table class="table table-striped">
                                        <tr>
                                            <th>序号</th>
                                            <th>合同号</th>
                                            <th>项目名称</th>
                                            <th>新增时间</th>
                                        </tr>
                                        <?php foreach ($project_info as $k => $l) { ?>
                                            <tr>
                                                <td><?php echo $k + 1; ?></td>
                                                <td><?php echo $l->project_name; ?></td>
                                                <td><?php echo $l->project_address; ?></td>
                                                <td><?php echo $l->create_time; ?></td>
                                            </tr>
                                        <?php } ?>
                                    </table>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-6">
                            <div class="chart-wrap">
                                <div id="refundcharts" style="width:100%;">
                                    <p>最近新增师傅</p>
                                    <table class="table table-striped">
                                        <tr>
                                            <th>序号</th>
                                            <th>师傅名称</th>
                                            <th>新增时间</th>
                                        </tr>
                                        <?php foreach ($master_info as $k => $l) { ?>
                                            <tr>
                                                <td><?php echo $k + 1; ?></td>
                                                <td><?php echo $l->master_name; ?></td>
                                                <td><?php echo $l->create_time; ?></td>
                                            </tr>
                                        <?php } ?>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>  
                </div>
                <div class="content-wrap">
                    <div class="row">
                        <div class="col-lg-6">
                            <div class="chart-wrap">
                                <div id="incomecharts" style="width:100%;" >
                                    <p>最近新增设计师</p>
                                    <table class="table table-striped">
                                        <tr>
                                            <th>序号</th>
                                            <th>设计师名称</th>
                                            <th>新增时间</th>
                                        </tr>
                                        <?php foreach ($designer_info as $k => $l) { ?>
                                            <tr>
                                                <td><?php echo $k + 1; ?></td>
                                                <td><?php echo $l->designer_name; ?></td>
                                                <td><?php echo $l->create_time; ?></td>
                                            </tr>
                                        <?php } ?>
                                    </table>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-6">
                            <div class="chart-wrap">
                                <div id="ordercharts" style="width:100%;">
                                    <p>最近新增业主</p>
                                    <table class="table table-striped">
                                        <tr>
                                            <th>序号</th>
                                            <th>业主名称</th>
                                            <th>手机号</th>
                                            <th>注册时间</th>
                                        </tr>
                                        <?php foreach ($user_info as $k => $l) { ?>
                                            <tr>
                                                <td><?php echo $k + 1; ?></td>
                                                <td><?php echo $l->name; ?></td>
                                                <td><?php echo $l->cellphone; ?></td>
                                                <td><?php echo $l->create_time; ?></td>
                                            </tr>
                                        <?php } ?>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>  
                </div>
                <!--                <div class="content-wrap">
                                    <div class="row">
                                        <div class="col-lg-4">
                                            <div class="chart-wrap">
                                                <div id="refundratechartsweb_count" style="width:100%;" ></div>
                                            </div>
                                        </div>
                                        <div class="col-lg-4">
                                            <div class="chart-wrap">
                                                <div id="refundratechartsterminal_count" style="width:100%;" ></div>
                                            </div>
                                        </div>
                                        <div class="col-lg-4">
                                            <div class="chart-wrap">
                                                <div id="refundratechartstotal_count" style="width:100%;"></div>
                                            </div>
                                        </div>
                                    </div>  
                                </div>-->
                <!--/END OF CONTENT--> 

                <!--FOOTER--> 
                <div class="footer-space"></div>
                <div id="footer">
                    <div class="devider-footer-left"></div>
                    <div class="time">
                        <p id="spanDate">
                        <p id="clock">
                    </div>
                    <div class="copyright">Copyright © 2017-2018
                        <span class="entypo-heart"></span><a href="#">优自在装修</a>. All rights reserved.
                    </div>
                </div>
                <!--/ END OF FOOTER--> 
            </div>
        </div>
        <!--END OF PAPER WRAP--> 
    </body>
</html>