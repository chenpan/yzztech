<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <title><?php echo Yii::app()->session['website_name']; ?></title> 
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta name="description" content="">
        <meta name="author" content="">
        <?php echo $recommend; ?>
        <style type="text/css">
            canvas#canvas4 {
                position: relative;
                top: 20px;
            }
            input[type="search"]{
                padding-top:5px;
                padding-bottom: 5px;
            }

            .content-wrap{
                background-color: #FFF;
                margin: 10px;
                padding-top: 20px;
                padding-bottom: 20px;
                border-radius: 3px;
            }
            .menulist{
                margin-top: 25px;
            }
            #alreadytable_wrapper section{
                border:1px #f5f5f5  solid;
                outline:none;

            }
        </style>
        <script type="text/javascript">
            $(function() {
                $('#settingtable').dataTable({
                    stateSave: true,
                    "pagingType": "input",
                    "language": {
                        "lengthMenu": "每页 _MENU_ 条记录",
                        "zeroRecords": "没有找到记录",
                        "info": "第 _PAGE_ 页 ( 总共 _PAGES_ 页 )",
                        "infoEmpty": "无记录",
                        "infoFiltered": "(从 _MAX_ 条记录过滤)",
                        "search": ""
                    }
                });
                $("#setting_open").css("display", "block");

                $("#logout").click(function() {
                    if (confirm("确定退出？"))
                    {
                        window.location.href = "./index.php?r=backend/admin/Logout";
                    }
                });
            });

            function editsetting(base_config_id) {
                if ('<?php echo $edit_setting; ?>' == "") {
                    window.location.href = './index.php?r=backend/setting/editsetting&base_config_id=' + base_config_id;
                } else if ('<?php echo $edit_setting; ?>' == "hidden") {
                    window.location.href = './index.php?r=backend/nonPrivilege/index';
                }
            }
        </script>
        <!-- HTML5 shim, for IE6-8 support of HTML5 elements -->
        <!--[if lt IE 9]>
            <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
            <![endif]-->
        <!-- Fav and touch icons -->
    </head>
    <body> 
        <?php echo $leftContent; ?>
        <!--  PaPER WRaP -->
        <div class="wrap-fluid" style="margin-left:250px">
            <div class="container-fluid paper-wrap bevel tlbr">
                <!-- CONTENT -->
                <!-- BREaDCRUMB -->
                <div id="breadcrumb">
                    <div class="pull-left dis-left">
                        <H3>基础信息</H3>
                    </div>
                    <ul class="pull-right dis-left">
                        <li>
                            <span class="entypo-home"></span>
                        </li>
                        <li><i class="fa fa-lg fa-angle-right"></i>
                        </li>
                        <li><a href="./index.php?r=backend/admin/index">首页</a>
                        </li>
                        <li><i class="fa fa-lg fa-angle-right"></i>
                        </li>
                        <li><a href="#">配置</a>
                        </li>
                        <li><i class="fa fa-lg fa-angle-right"></i>
                        </li>
                        <li><a href="./index.php?r=backend/setting/setting">基础信息</a>
                        </li>
                    </ul>
                </div>
                <div class="content-wrap">
                    <div class="row">
                        <div class="col-lg-12">
                            <table id="settingtable" class="display">
                                <thead>
                                    <tr class="th">
                                        <th style="padding-left: 10px;">序列</th>
                                        <th>网站名称</th>
                                        <th>左菜单名称</th>
                                        <th>操作</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php foreach ($base_config_info as $K => $V) {
                                        ?>
                                        <tr>
                                            <td style="padding-left: 13px;"><?php echo $K + 1; ?></td>
                                            <td><?php echo $V->website_name; ?></td>
                                            <td><?php echo $V->menu_name; ?></td>
                                            <td>
                                                <a class="edit_btn" href="#" onclick="editsetting(<?php echo $V->base_config_id; ?>)"><span class="label label-success">编辑</span></a>
                                            </td>
                                        </tr>
                                    <?php } ?>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>

                <!-- FOOTER -->

                <div id="footer">
                    <div class="devider-footer-left"></div>
                    <div class="time">
                        <p id="spanDate">
                        <p id="clock">
                    </div>
                    <div class="copyright">Copyright © 2017-2018
                        <span class="entypo-heart"></span><a href="#">优自在装修</a>. All rights reserved.
                    </div>
                </div>
                <!-- / END OF FOOTER -->
            </div>
        </div>
        <!--  END OF PaPER WRaP -->
    </body>

</html>

