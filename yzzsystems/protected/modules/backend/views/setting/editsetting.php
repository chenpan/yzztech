<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <title><?php echo Yii::app()->session['website_name']; ?></title> 
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta name="description" content="">
        <meta name="author" content="">
        <?php echo $recommend; ?>
        <style type="text/css">
            canvas#canvas4 {
                position: relative;
                top: 20px;
            }
            input[type="search"]{
                padding-top:5px;
                padding-bottom: 5px;
            }

            .content-wrap{
                background-color: #FFF;
                margin: 10px;
                padding-top: 20px;
                padding-bottom: 20px;
                border-radius: 3px;
            }
            .menulist{
                margin-top: 25px;
            }
            #alreadytable_wrapper section{
                border:1px #f5f5f5  solid;
                outline:none;
            }
        </style>
        <script type="text/javascript">
            $(function() {
                $("#logout").click(function() {
                    if (confirm("确定退出？"))
                    {
                        window.location.href = "./index.php?r=backend/admin/Logout";
                    }
                });

                $("#setting_open").css("display", "block");


                $("#save").click(function() {
                    reback();
                    if (confirm("确认保存？")) {
                        add_base_config_form.submit();
                    }
                });
            });
            function reback() {
                $("#website_name_info,#menu_name_info").text("*");
                $("#info").text("");
            }
        </script>
        <!-- HTML5 shim, for IE6-8 support of HTML5 elements -->
        <!--[if lt IE 9]>
            <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
            <![endif]-->
        <!-- Fav and touch icons -->
    </head>
    <body> 
        <?php echo $leftContent; ?>
        <!--  PaPER WRaP -->
        <div class="wrap-fluid" style="margin-left:250px">
            <div class="container-fluid paper-wrap bevel tlbr">
                <!-- CONTENT -->
                <!-- BREaDCRUMB -->
                <div id="breadcrumb">
                    <div class="pull-left dis-left">
                        <H3>基础信息</H3>
                    </div>
                    <ul class="pull-right dis-left">
                        <li>
                            <span class="entypo-home"></span>
                        </li>
                        <li><i class="fa fa-lg fa-angle-right"></i>
                        </li>
                        <li><a href="./index.php?r=backend/admin/index">首页</a>
                        </li>
                        <li><i class="fa fa-lg fa-angle-right"></i>
                        </li>
                        <li><a href="#">项目</a>
                        </li>
                        <li><i class="fa fa-lg fa-angle-right"></i>
                        </li>
                        <li><a href="./index.php?r=backend/setting/setting">基础信息</a>
                        </li>
                    </ul>
                </div>
                <div class="content-wrap">
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="body-nest" id="basic">
                                <div class="form_center">
                                    <form role="form" id="add_base_config_form" method="post" enctype="multipart/form-data" class="form-horizontal">
                                        <div class="form-group" style="display:none;">
                                            <div class="col-sm-5 control-label"> <label for="base_config_id">信息ID:</label></div>
                                            <div class="col-sm-3"><input type="text" placeholder="" id="base_config_id" name ="base_config_id" class="form-control" value="<?php
                                                if (isset($base_config_info)) {
                                                    echo $base_config_info->base_config_id;
                                                } else {
                                                    echo 0;
                                                }
                                                ?>"></div>
                                            <div class="col-sm-3 "></div>
                                        </div>
                                        <div class="form-group">
                                            <div class="col-sm-5 control-label"> <label for="website_name">网站名称：</label></div>
                                            <div class="col-sm-3"><input type="text" id="website_name" name ="website_name" class="form-control" placeholder="请输入网站名称" value="<?php
                                                if (isset($base_config_info)) {
                                                    echo $base_config_info->website_name;
                                                }
                                                ?>"></div>
                                            <div class="col-sm-3 star" id="website_name_info"></div>
                                        </div>
                                        <div class="form-group">
                                            <div class="col-sm-5 control-label"> <label for="menu_name">左菜单名称：</label></div>
                                            <div class="col-sm-3"><input type="text" id="menu_name" name ="menu_name" class="form-control" placeholder="请输入左菜单名称" value="<?php
                                                if (isset($base_config_info)) {
                                                    echo $base_config_info->menu_name;
                                                }
                                                ?>"></div>
                                            <div class="col-sm-3 star" id="menu_name_info"></div>
                                        </div>
                                        <div class="form-group" style="margin-top: 30px;">
                                            <div class="col-sm-5"></div>
                                            <div class="col-sm-3"> <button class="btn btn-info" type="button" id="save"  style="width: 100%;outline:none;">保存</button></div>
                                            <div class="col-sm-3" style="margin-top:5px;"> <span id="info"></span></div>
                                        </div>
                                    </form>  
                                </div>

                            </div>
                        </div>
                    </div>
                </div>
                <!--  / DEVICE MaNaGER -->
                <!-- FOOTER -->

                <div id="footer">
                    <div class="devider-footer-left"></div>
                    <div class="time">
                        <p id="spanDate">
                        <p id="clock">
                    </div>
                    <div class="copyright">Copyright © 2017-2018
                        <span class="entypo-heart"></span><a href="#">优自在装修</a>. All rights reserved.
                    </div>
                </div>
                <!-- / END OF FOOTER -->
            </div>
        </div>
        <!--  END OF PaPER WRaP -->
    </body>
</html>

