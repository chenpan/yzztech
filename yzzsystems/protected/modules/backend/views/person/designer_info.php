<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <title><?php echo Yii::app()->session['website_name']; ?></title> 
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta name="description" content="">
        <meta name="author" content="">
        <?php echo $recommend; ?>
        <link rel="stylesheet" href="./css/multiselect/css/bootstrap-3.0.3.min.css" type="text/css">
        <link rel="stylesheet" href="./css/multiselect/css/bootstrap-multiselect.css" type="text/css">
        <link rel="stylesheet" href="./css/multiselect/css/prettify.css" type="text/css">

        <link href="./css/PictureDisplay/css/index.css" rel="stylesheet">
        <script type="text/javascript" src="./css/PictureDisplay/js/jquery.fancybox.js "></script>

        <script type="text/javascript" src="./css/multiselect/js/bootstrap-multiselect.js"></script>
        <script type="text/javascript" src="./css/multiselect/js/prettify.js"></script>

        <script type="text/javascript" src="./css/laydate/laydate.js"></script>

        <style type="text/css">
            canvas#canvas4 {
                position: relative;
                top: 20px;
            }
            input[type="search"]{
                padding-top:5px;
                padding-bottom: 5px;
            }

            .content-wrap{
                background-color: #FFF;
                margin: 10px;
                padding-top: 20px;
                padding-bottom: 20px;
                border-radius: 3px;
            }
            .menulist{
                margin-top: 25px;
            }
            #alreadytable_wrapper section{
                border:1px #f5f5f5  solid;
                outline:none;
            }
        </style>
        <script type="text/javascript">
            $(function() {
                $("#logout").click(function() {
                    if (confirm("确定退出？"))
                    {
                        window.location.href = "./index.php?r=backend/admin/Logout";
                    }
                });

                $('#user_id').multiselect({
                    includeSelectAllOption: true,
                    enableFiltering: true,
                    maxHeight: 150
                });

                $('.fancy').fancybox();
                $('.fancybox-thumbs').fancybox({
                    prevEffect: 'none',
                    nextEffect: 'none',
                    closeBtn: false,
                    arrows: false,
                    nextClick: true,
                    helpers: {
                        thumbs: {
                            width: 50,
                            height: 50
                        }
                    }
                });

                $("#person-open").css("display", "block");
                $("#_position_id").val("<?php
        if (isset($designer_info)) {
            echo $designer_info->_position_id;
        } else {
            echo 0;
        }
        ?>");
                $("#level").val("<?php
        if (isset($designer_info)) {
            echo $designer_info->level;
        } else {
            echo 0;
        }
        ?>");

                $("#save").click(function() {
                    if ($("#designer_name").val() == "" || $("#designer_name").val() == null) {
                        reback();
                        $("#designer_name_info").text("请输入设计师名字！");
                        return false;
                    } else if ($("#cellphone").val() == "" || $("#cellphone").val() == null) {
                        reback();
                        $("#cellphone_info").text("请输入设计师电话！");
                        return false;
                    } else if ($("#cellphone").val().length != 11) {
                        reback();
                        $("#cellphone_info").text("电话号码不正确");
                        return false;
                    } else if ($("#_position_id").val() == 0) {
                        reback();
                        $("#_position_id_info").text("请选择职位名称！");
                        return false;
                    } else if ($("#level").val() == null) {
                        reback();
                        $("#level_info").text("请选择职能级别！");
                        return false;
                    } else if ($("#entry_time").val() == "" || $("#entry_time").val() == null) {
                        reback();
                        $("#entry_time_info").text("请输入行时间！");
                        return false;
                    } else {
                        reback();
                        if (confirm("确认保存？")) {
                            add_designer_form.submit();
                        }
                    }
                });
            });
            function reback() {
                $("#designer_name_info,#cellphone_info,#_position_id_info,#level_info,#entry_time_info").text("*");
                $("#info").text("");
            }
        </script>
        <!-- HTML5 shim, for IE6-8 support of HTML5 elements -->
        <!--[if lt IE 9]>
            <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
            <![endif]-->
        <!-- Fav and touch icons -->
    </head>
    <body> 
        <?php echo $leftContent; ?>
        <!--  PaPER WRaP -->
        <div class="wrap-fluid" style="margin-left:250px">
            <div class="container-fluid paper-wrap bevel tlbr">
                <!-- CONTENT -->
                <!-- BREaDCRUMB -->
                <div id="breadcrumb">
                    <div class="pull-left dis-left">
                        <H3>设计师信息</H3>
                    </div>
                    <ul class="pull-right dis-left">
                        <li>
                            <span class="entypo-home"></span>
                        </li>
                        <li><i class="fa fa-lg fa-angle-right"></i>
                        </li>
                        <li><a href="./index.php?r=backend/admin/index">首页</a>
                        </li>
                        <li><i class="fa fa-lg fa-angle-right"></i>
                        </li>
                        <li><a href="#">人员</a>
                        </li>
                        <li><i class="fa fa-lg fa-angle-right"></i>
                        </li>
                        <li><a href="./index.php?r=backend/person/designer">设计师管理</a>
                        </li>
                        <li><i class="fa fa-lg fa-angle-right"></i>
                        </li>
                        <li><a href="#">设计师信息</a>
                        </li>
                    </ul>
                </div>
                <div class="content-wrap">
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="body-nest" id="basic">
                                <div class="form_center">
                                    <form role="form" id="add_designer_form" method="post" enctype="multipart/form-data" class="form-horizontal">
                                        <div class="form-group" style="display:none;">
                                            <div class="col-sm-5 control-label"> <label for="designer_id">设计师ID:</label></div>
                                            <div class="col-sm-3"><input type="text" placeholder="" id="designer_id" name ="designer_id" class="form-control" value="<?php
                                                if (isset($designer_info)) {
                                                    echo $designer_info->designer_id;
                                                } else {
                                                    echo 0;
                                                }
                                                ?>"></div>
                                            <div class="col-sm-3 "></div>
                                        </div>
                                        <div class="form-group">
                                            <div class="col-sm-5 control-label"> <label for="designer_name">设计师名字:</label></div>
                                            <div class="col-sm-3"><input type="text" id="designer_name" name ="designer_name" class="form-control" placeholder="请输入设计师名称" value="<?php
                                                if (isset($designer_info)) {
                                                    echo $designer_info->designer_name;
                                                }
                                                ?>"></div>
                                            <div class="col-sm-3 star" id="designer_name_info">*</div>
                                        </div>
                                        <div class="form-group">
                                            <div class="col-sm-5 control-label"> <label for="cellphone">设计师电话:</label></div>
                                            <div class="col-sm-3"><input type="text" id="cellphone" name ="cellphone" class="form-control" placeholder="请输入设计师电话" value="<?php
                                                if (isset($designer_info)) {
                                                    echo $designer_info->cellphone;
                                                }
                                                ?>">
                                            </div>
                                            <div class="col-sm-3 star" id="cellphone_info">*</div>
                                        </div>
                                        <div class="form-group">
                                            <div class="col-sm-5 control-label"><label for="designer_description">设计师描述:</label>    </div>
                                            <div class="col-sm-3">
                                                <textarea rows="9" style="width:100%;color:#8FBDE6;text-indent:0em" id="designer_description" name="designer_description"><?php
                                                    if (isset($designer_info)) {
                                                        echo $designer_info->designer_description;
                                                    } else {
                                                        echo "";
                                                    }
                                                    ?></textarea> 
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <div class="col-sm-5 control-label"><label for="head_add">设计师头像:</label></div>
                                            <div class="col-sm-3"> 
                                                <input type="file" name="head_add" id="head_add" /> 
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <div class="col-sm-5 control-label"><label for="_position_id">职位名称:</label></div>
                                            <div class="col-sm-3"> 
                                                <select id="_position_id" name="_position_id" class="form-control">
                                                    <option value="0">******请选择******</option>
                                                    <?php foreach ($position_info as $k => $l) { ?>
                                                        <option value="<?php echo $l->position_id; ?>"><?php echo $l->position_name; ?></option>
                                                    <?php } ?>    
                                                </select>
                                            </div>
                                            <div class="col-sm-3 star" id="_position_id_info">*</div>
                                        </div>
                                        <div class="form-group">
                                            <div class="col-sm-5 control-label"><label for="level">职能级别:</label></div>
                                            <div class="col-sm-3"> 
                                                <select id="level" name="level" class="form-control">
                                                    <option value="0">******请选择******</option>
                                                    <option value="A">A</option>
                                                    <option value="B">B</option>
                                                    <option value="C">C</option>
                                                    <option value="D">D</option>
                                                    <option value="E">E</option>
                                                </select>
                                            </div>
                                            <div class="col-sm-3 star" id="level_info">*</div>
                                        </div>
                                        <div class="form-group">
                                            <div class="col-sm-5 control-label"> <label for="entry_time">入行时间:</label></div>
                                            <div class="col-sm-3">
                                                <input type="text" class="form-control laydate-icon" id="entry_time" name="entry_time"
                                                       placeHolder="请设置入行时间" onclick="laydate({istime: true, format: 'YYYY-MM-DD'})"
                                                       value="<?php
                                                       if (isset($designer_info)) {
                                                           echo $designer_info->entry_time;
                                                       }
                                                       ?>">
                                            </div>
                                            <div class="col-sm-3 star" id="entry_time_info">*</div>
                                        </div>                                       

                                        <div class="form-group" style="margin-top: 30px;">
                                            <div class="col-sm-5"></div>
                                            <div class="col-sm-3"> <button class="btn btn-info" type="button" id="save"  style="width: 100%;outline:none;">保存</button></div>
                                            <div class="col-sm-3" style="margin-top:5px;"> <span id="info"></span></div>
                                        </div>
                                        <?php if (isset($designer_info)) { ?>
                                            <div class="form-group">
                                                <div class="col-sm-5 control-label"><label for="picture">现有头像:</label></div>
                                                <div class="col-sm-3">
                                                    <a class="fancy"  href="<?php
                                                    if (isset($designer_info)) {
                                                        echo $designer_info->head_add;
                                                    }
                                                    ?>" data-fancybox-group="gallery">
                                                        <img style = "width:100px;height:100px" class="thumbnails" src="<?php
                                                        if (isset($designer_info)) {
                                                            echo $designer_info->head_add;
                                                        }
                                                        ?>"/>
                                                    </a>
                                                </div>
                                            </div>
                                        <?php } ?>
                                    </form>  
                                </div>

                            </div>
                        </div>
                    </div>
                </div>
                <!--  / DEVICE MaNaGER -->
                <!-- FOOTER -->

                <div id="footer">
                    <div class="devider-footer-left"></div>
                    <div class="time">
                        <p id="spanDate">
                        <p id="clock">
                    </div>
                    <div class="copyright">Copyright © 2017-2018
                        <span class="entypo-heart"></span><a href="#">优自在装修</a>. All rights reserved.
                    </div>
                </div>
                <!-- / END OF FOOTER -->
            </div>
        </div>
        <!--  END OF PaPER WRaP -->
    </body>
</html>

