<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <title><?php echo Yii::app()->session['website_name']; ?></title> 
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta name="description" content="">
        <meta name="author" content="">
        <?php echo $recommend; ?>
        <link href="./css/PictureDisplay/css/index.css" rel="stylesheet">
        <script type="text/javascript" src="./css/PictureDisplay/js/jquery.fancybox.js "></script>
        <style type="text/css">
            canvas#canvas4 {
                position: relative;
                top: 20px;
            }
            input[type="search"]{
                padding-top:5px;
                padding-bottom: 5px;
            }

            .content-wrap{
                background-color: #FFF;
                margin: 10px;
                padding-top: 20px;
                padding-bottom: 20px;
                border-radius: 3px;
            }
            .menulist{
                margin-top: 25px;
            }
            #alreadytable_wrapper section{
                border:1px #f5f5f5  solid;
                outline:none;

            }
        </style>
        <script type="text/javascript">
            $(function() {
                $('#designertable').dataTable({
                    stateSave: true,
                    "pagingType": "input",
                    "language": {
                        "lengthMenu": "每页 _MENU_ 条记录",
                        "zeroRecords": "没有找到记录",
                        "info": "第 _PAGE_ 页 ( 总共 _PAGES_ 页 )",
                        "infoEmpty": "无记录",
                        "infoFiltered": "(从 _MAX_ 条记录过滤)",
                        "search": ""
                    }
                });

                $('.fancy').fancybox();
                $('.fancybox-thumbs').fancybox({
                    prevEffect: 'none',
                    nextEffect: 'none',
                    closeBtn: false,
                    arrows: false,
                    nextClick: true,
                    helpers: {
                        thumbs: {
                            width: 50,
                            height: 50
                        }
                    }
                });

                $("#person-open").css("display", "block");

                $("#logout").click(function() {
                    if (confirm("确定退出？")) {
                        window.location.href = "./index.php?r=backend/admin/Logout";
                    }
                });
                $("#adddesigner").click(function() {
                    window.location.href = "./index.php?r=backend/person/designer_info";
                });
                if ('<?php echo $add_designer; ?>' == "hidden") {
                    $("#adddesigner").parent().parent().parent().hide();
                }
            });
            function deletedesigner(designer_id, designer_name) {
                if ('<?php echo $del_designer; ?>' == "") {
                    if (confirm("确认删除 " + designer_name + " 设计师?")) {
                        $.post("./index.php?r=backend/person/deletedesigner", {designer_id: designer_id}, function(datainfo) {
                            var data = eval("(" + datainfo + ")");
                            if (data.data == "success") {
                                alert("删除成功！");
                                window.location.href = "./index.php?r=backend/person/designer";
                            } else {
                                alert("删除失败！");
                            }
                        });
                    }
                } else if ('<?php echo $del_designer; ?>' == "hidden") {
                    window.location.href = './index.php?r=backend/nonPrivilege/index';
                }
            }

            function editdesigner(designer_id) {
                if ('<?php echo $edit_designer; ?>' == "") {
                    window.location.href = './index.php?r=backend/person/designer_info&designer_id=' + designer_id;
                } else if ('<?php echo $edit_designer; ?>' == "hidden") {
                    window.location.href = './index.php?r=backend/nonPrivilege/index';
                }
            }
        </script>
        <!-- HTML5 shim, for IE6-8 support of HTML5 elements -->
        <!--[if lt IE 9]>
            <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
            <![endif]-->
        <!-- Fav and touch icons -->
    </head>
    <body> 
        <?php echo $leftContent; ?>
        <!--  PaPER WRaP -->
        <div class="wrap-fluid" style="margin-left:250px">
            <div class="container-fluid paper-wrap bevel tlbr">
                <!-- CONTENT -->
                <!-- BREaDCRUMB -->
                <div id="breadcrumb">
                    <div class="pull-left dis-left">
                        <H3>设计师列表</H3>
                    </div>
                    <ul class="pull-right dis-left">
                        <li>
                            <span class="entypo-home"></span>
                        </li>
                        <li><i class="fa fa-lg fa-angle-right"></i>
                        </li>
                        <li><a href="./index.php?r=backend/admin/index">首页</a>
                        </li>
                        <li><i class="fa fa-lg fa-angle-right"></i>
                        </li>
                        <li><a href="#">人员</a>
                        </li>
                        <li><i class="fa fa-lg fa-angle-right"></i>
                        </li>
                        <li><a href="./index.php?r=backend/person/designer">设计师列表</a>
                        </li>
                    </ul>
                </div>
                <div class="content-wrap">
                    <div class="row">
                        <div class="col-lg-12">
                            <input type="button" class="btn btn-success btn-set" id="adddesigner" value="新增设计师">
                        </div>
                    </div>
                </div>
                <div class="content-wrap">
                    <div class="row">
                        <div class="col-lg-12">
                            <table id="designertable" class="display">
                                <thead>
                                    <tr class="th">
                                        <th style="padding-left: 10px;">序列</th>
                                        <th>设计师名字</th>
                                        <th>设计师电话</th>
                                        <th>设计师描述</th>
                                        <th>设计师头像</th>
                                        <th>职位名称</th>
                                        <th>职能级别</th>
                                        <th>入行时间</th>
                                        <th>创建时间</th>
                                        <th>操作</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php foreach ($designer_info as $K => $V) {
                                        ?>
                                        <tr>
                                            <td style="padding-left: 13px;"><?php echo $K + 1; ?></td>
                                            <td><?php echo $V->designer_name; ?></td>
                                            <td><?php echo $V->cellphone; ?></td>
                                            <td><?php echo $V->designer_description; ?></td>
                                            <td><?php
                                                if ($V->head_add == NULL)
                                                    echo "无";
                                                else {
                                                    echo '<a class="fancy"  href="' . $V->head_add . '" data-fancybox-group="gallery">';
                                                    echo '<img style = "width:20px;height:20px" class="thumbnails" src="' . $V->head_add . '"/>';
                                                    echo '</a>';
                                                }
                                                ?>
                                            <td><?php
                                                $position_model = position::model();
                                                $position_info = $position_model->findByPk($V->_position_id);
                                                if (count($position_info) != 0) {
                                                    echo $position_info->position_name;
                                                } else {
                                                    echo "暂无";
                                                }
                                                ?></td>
                                            <td><?php echo $V->level; ?></td>
                                            <td><?php echo $V->entry_time; ?></td>
                                            <td><?php echo $V->create_time; ?></td>                                            
                                            <td>
                                                <a class="edit_btn" href="#" onclick="editdesigner(<?php echo $V->designer_id; ?>)"><span class="label label-success">编辑</span></a>
                                                <a href="#" class="delete_btn" onclick="deletedesigner(<?php echo $V->designer_id; ?>, '<?php echo $V->designer_name; ?>')" ><span class="label label-success">删除</span></a>
                                            </td>
                                        </tr>
                                    <?php } ?>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>

                <!-- FOOTER -->

                <div id="footer">
                    <div class="devider-footer-left"></div>
                    <div class="time">
                        <p id="spanDate">
                        <p id="clock">
                    </div>
                    <div class="copyright">Copyright © 2017-2018
                        <span class="entypo-heart"></span><a href="#">优自在装修</a>. All rights reserved.
                    </div>
                </div>
                <!-- / END OF FOOTER -->
            </div>
        </div>
        <!--  END OF PaPER WRaP -->
    </body>

</html>

