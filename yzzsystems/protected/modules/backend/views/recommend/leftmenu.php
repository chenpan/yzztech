<!-- TOP NAVBAR -->
<nav role="navigation" class="navbar navbar-static-top">
    <div class="container-fluid" style="margin-top: 20px">
        <!-- Brand and toggle get grouped for better mobile display -->
        <div class="navbar-header">
            <button data-target="#bs-example-navbar-collapse-1" data-toggle="collapse" class="navbar-toggle" type="button">
                <span class="entypo-menu"></span>
            </button>
            <div id="logo-mobile" class="visible-xs">
                <h1>WEB管理<span>v1.2</span></h1>
            </div>
        </div>
        <!-- Collect the nav links, forms, and other content for toggling -->
        <div id="bs-example-navbar-collapse-1" class="collapse navbar-collapse">
            <!--            <ul class="nav navbar-nav">
                            <li class="dropdown">
            
                                <a data-toggle="dropdown" class="dropdown-toggle" href="#"><i style="font-size:20px;" class="icon-conversation"></i><div class="noft-red">23</div></a>
            
            
                                <ul style="margin: 11px 0 0 9px;" role="menu" class="dropdown-menu dropdown-wrap">
                                    <li>
                                        <a href="#">
                                            <img alt="" class="img-msg img-circle" src="./head_pic/1.png">大方<b>刚刚</b>
                                        </a>
                                    </li>
                                    <li class="divider"></li>
                                    <li>
                                        <a href="#">
                                            <img alt="" class="img-msg img-circle" src="./head_pic/2.png">张超<b>3分钟前</b>
                                        </a>
                                    </li>
                                    <li class="divider"></li>
                                    <li>
                                        <a href="#">
                                            <img alt="" class="img-msg img-circle" src="./head_pic/3.png">吕泽松<b>2小时前</b>
                                        </a>
                                    </li>
                                    <li class="divider"></li>
                                    <li>
                                        <a href="#">
                                            <img alt="" class="img-msg img-circle" src="./head_pic/4.png">李雯<b>1天前</b>
                                        </a>
                                    </li>
                                    <li class="divider"></li>
                                    <li>
                                        <a href="#">
                                            <img alt="" class="img-msg img-circle" src="./head_pic/5.png">王刚<b>2个月前</b>
                                        </a>
                                    </li>
                                    <li class="divider"></li>
                                    <li>
                                        <div>查看全部信息</div>
                                    </li>
                                </ul>
                            </li>
                            <li>
                                <a data-toggle="dropdown" class="dropdown-toggle" href="#"><i style="font-size:19px;" class="icon-warning tooltitle"></i><div class="noft-green">5</div></a>
                                <ul style="margin: 12px 0 0 0;min-width: 340px;" role="menu" class="dropdown-menu dropdown-wrap">
                                    <li>
                                        <a href="#">
                                            <span style="background:#DF2135" class="noft-icon fontawesome-file"></span>某某项目完成 <b>刚刚</b>
                                        </a>
                                    </li>
                                    <li class="divider"></li>
                                    <li>
                                        <a href="#">
                                            <span style="background:#AB6DB0" class="noft-icon entypo-signal"></span>某某新建一个项目  <b>15分钟前</b>
                                        </a>
                                    </li>
                                    <li class="divider"></li>
                                    <li>
                                        <a href="#">
                                            <span style="background:#FFA200" class="noft-icon fontawesome-print"></span>某某装修项目完成<b>2小时前</b>
                                        </a>
                                    </li>
                                    <li class="divider"></li>
                                    <li>
                                        <a href="#">
                                            <span style="background:#86C440" class="noft-icon fontawesome-remove"></span>某某装修项目完成 <b>1天前</b>
                                        </a>
                                    </li>
                                    <li class="divider"></li>
                                    <li>
                                        <a href="#">
                                            <span style="background:#0DB8DF" class="noft-icon fontawesome-circle"></span>某某装修项目完成  <b>3个月前</b>
                                        </a>
                                    </li>
                                    <li class="divider"></li>
                                    <li>
                                        <div style="min-width: 340px;">查看全部通知</div>
                                    </li>
                                </ul>
                            </li>
                                    <li><a href="#"><i data-toggle="tooltip" data-placement="bottom" title="Help" style="font-size:20px;" class="icon-help tooltitle"></i></a>
                            </li>
            
                        </ul>-->
            <div id="nt-title-container" class="navbar-left running-text visible-lg" style="width: 950px">
                <ul class="date-top">
                    <li class="entypo-calendar" style="margin-right:5px"></li>
                    <li id="Date"></li>
                </ul>
                <ul id="digital-clock" class="digital">
                    <li class="entypo-clock" style="margin-right:5px"></li>
                    <li class="hour"></li>
                    <li>:</li>
                    <li class="min"></li>
                    <li>:</li>
                    <li class="sec"></li>
                    <li class="meridiem"></li>
                </ul>
                <!--TITLE -->
                <div id="paper-top">
                    <div class="devider-vertical visible-lg"></div>
                    <div class="tittle-middle-header">
                        <div class="alert" id="wel-time">
                            <button type="button" class="close" data-dismiss="alert" style="margin-right: 10px;">×</button>
                            &nbsp;
                            <span class="tittle-alert entypo-info-circled"></span>
                            &nbsp;&nbsp;欢迎 <span style="font-weight: bold;"><?php echo $username; ?></span>&nbsp;&nbsp;&nbsp;<span id="login-time">您本次登录时间为 <?php echo urldecode($logintime); ?><span>
                                    </div>
                                    </div>
                                    </div>
                                    <!--/ TITLE -->
                                    </div>

                                    <ul style="margin-right:0;" class="nav navbar-nav navbar-right">
                                        <li>
                                            <a data-toggle="dropdown" class="dropdown-toggle" href="#">
                                                <img alt="" class="admin-pic img-circle" src="./head_pic/1.png">
                                                Hi, <?php echo $username; ?> <b class="caret"></b>
                                            </a>
                                            <ul style="margin-top:14px;" role="menu" class="dropdown-setting dropdown-menu">
                                                <li>
                                                    <a href="./index.php?r=backend/admin/modify">
                                                        <span class="entypo-user"></span>&#160;&#160;个人中心</a>
                                                </li>
                                                <li class="divider"></li>
                                                <li>
                                                    <a href="#">
                                                        <span class="entypo-lifebuoy"></span>&#160;&#160;求助</a>
                                                </li>
                                                <li class="divider"></li>
                                                <li>
                                                    <a href="#" id="logout">
                                                        <span class="entypo-leaf"></span>&#160;&#160;退出</a>
                                                </li>
                                            </ul>
                                        </li>
                                        <!--                                        <li>
                                                                                    <a class="dropdown-toggle" href="./index.php?r=backend/admin/downloadApp">
                                                                                        APP下载
                                                                                    </a>
                                                                                </li>-->
                                        <li>
                                            <a data-toggle="dropdown" class="dropdown-toggle" href="#" title="更换背景">
                                                <span class="icon-gear"></span>&nbsp;&nbsp;&nbsp;</a>
                                            <ul role="menu" class="dropdown-setting dropdown-menu">
                                                <li class="theme-bg">
                                                    <div id="button-bg"></div>
                                                    <div id="button-bg2"></div>
                                                    <div id="button-bg3"></div>
                                                    <div id="button-bg5"></div>
                                                    <div id="button-bg6"></div>
                                                    <div id="button-bg7"></div>
                                                    <div id="button-bg8"></div>
                                                    <div id="button-bg9"></div>
                                                    <div id="button-bg10"></div>
                                                    <div id="button-bg11"></div>
                                                    <div id="button-bg12"></div>
                                                    <div id="button-bg13"></div>
                                                </li>
                                            </ul>
                                        </li>
                                    </ul>
                                    </div>
                                    <!-- /.navbar-collapse -->
                                    </div>
                                    <!-- /.container-fluid -->
                                    </nav>
                                    <!-- /END OF TOP NAVBAR -->
                                    <!-- SIDE MENU -->
                                    <div id="skin-select" style="top:48px;">
                                        <div id="logo">
                                            <!--<h1>优自在装修</h1>-->  
                                            <h1><?php echo $menu_name; ?></h1>  
                                        </div>

                                        <a id="toggle">
                                            <span class="entypo-menu"></span>
                                        </a>
                                        <div class="dark">
                                            <form action="#">
                                                <span>
                                                    <input type="text" name="search" value="" class="search rounded id_search" placeholder="搜索..." autofocus="">
                                                </span>
                                            </form>
                                        </div>

                                        <div class="search-hover">
                                            <form id="demo-2">
                                                <input type="search" placeholder="搜索..." class="id_search">
                                            </form>
                                        </div>
                                        <div class="skin-part" style="">
                                            <div id="tree-wrap">
                                                <div class="side-bar">
                                                    <ul class="topnav menu-left-nest top-white" style="margin:10px">                                                        
                                                        <li id="index-open">
                                                            <a class="tooltip-tip ajax-load" href="./index.php?r=backend/admin/index" title="首页">
                                                                <i class="fontawesome-home"  style="margin-left:3px;"></i>
                                                                <span>首页</span>
                                                            </a>
                                                        </li>
                                                    </ul>
                                                    <ul <?php
                                                    if ($leftmenu['项目'] == 0 && $leftmenu['节点'] == 0) {
                                                        echo 'style="display:none"';
                                                    }
                                                    ?> class="topnav menu-left-nest top-white">
                                                        <li>
                                                            <a class="tooltip-tip ajax-load" href="#" title="项目">
                                                                <i class="entypo-tape" style="margin-left:3px;"></i>
                                                                <span <?php
                                                                if ($leftmenu['项目'] == 0) {
                                                                    echo 'style="display:none"';
                                                                }
                                                                ?>  >项目</span>                                   
                                                            </a>
                                                            <ul id="project-open">                                                                
                                                                <li>
                                                                    <a  id="project-control"<?php
                                                                    if ($leftmenu['项目管理'] == 0) {
                                                                        echo 'style="display:none"';
                                                                    }
                                                                    ?> class="tooltip-tip2 ajax-load" href="./index.php?r=backend/project/project" title="项目管理"><i class="entypo-flow-cascade"></i><span>项目管理</span></a>
                                                                </li>                                                                
                                                            </ul>                                                            
                                                        </li>   
                                                    </ul>
                                                    <ul <?php
                                                    if ($leftmenu['配置'] == 0) {
                                                        echo 'style="display:none"';
                                                    }
                                                    ?> class="topnav menu-left-nest top-white">
                                                        <li>
                                                            <a class="tooltip-tip ajax-load" href="#" title="配置">
                                                                <i class="entypo-video" style="margin-left:3px;"></i>
                                                                <span <?php
                                                                if ($leftmenu['配置'] == 0) {
                                                                    echo 'style="display:none"';
                                                                }
                                                                ?>  style="margin-left:-2px;">配置</span>                                   
                                                            </a>
                                                            <ul id="setting_open">
                                                                <li>
                                                                    <a  id="schedule-control"<?php
                                                                    if ($leftmenu['节点管理'] == 0) {
                                                                        echo 'style="display:none"';
                                                                    }
                                                                    ?> class="tooltip-tip2 ajax-load" href="./index.php?r=backend/project/schedule" title="节点管理"><i class="entypo-flow-cascade"></i><span>节点管理</span></a>
                                                                </li>
                                                                <li>
                                                                    <a  id="setting-control"<?php
                                                                    if ($leftmenu['基础信息'] == 0) {
                                                                        echo 'style="display:none"';
                                                                    }
                                                                    ?> class="tooltip-tip2 ajax-load" href="./index.php?r=backend/setting/setting" title="基础信息">
                                                                        <i class="entypo-flow-cascade"></i><span>基础信息</span>
                                                                    </a>
                                                                </li>
                                                                <li>
                                                                    <a  id="setting-control"<?php
                                                                    if ($leftmenu['职位管理'] == 0) {
                                                                        echo 'style="display:none"';
                                                                    }
                                                                    ?> class="tooltip-tip2 ajax-load" href="./index.php?r=backend/setting/position" title="职位管理">
                                                                        <i class="entypo-flow-cascade"></i><span>职位管理</span>
                                                                    </a>
                                                                </li>
                                                            </ul>                                                            
                                                        </li>   
                                                    </ul>
                                                    <ul <?php
                                                    if ($leftmenu['人员'] == 0) {
                                                        echo 'style="display:none"';
                                                    }
                                                    ?> class="topnav menu-left-nest top-white">
                                                        <li>
                                                            <a class="tooltip-tip ajax-load" href="#" title="人员">
                                                                <i class="maki-school" style="margin-left:3px;"></i>
                                                                <span <?php
                                                                if ($leftmenu['人员'] == 0) {
                                                                    echo 'style="display:none"';
                                                                }
                                                                ?>  style="margin-left:-2px;">人员</span>                                   
                                                            </a>
                                                            <ul id="person-open">
                                                                <li>
                                                                    <a  id="master-control"<?php
                                                                    if ($leftmenu['师傅管理'] == 0) {
                                                                        echo 'style="display:none"';
                                                                    }
                                                                    ?> class="tooltip-tip2 ajax-load" href="./index.php?r=backend/person/master" title="师傅管理"><i class="entypo-flow-cascade"></i><span>师傅管理</span></a>
                                                                </li>
                                                                <li>
                                                                    <a  id="desgner-control"<?php
                                                                    if ($leftmenu['设计师管理'] == 0) {
                                                                        echo 'style="display:none"';
                                                                    }
                                                                    ?> class="tooltip-tip2 ajax-load" href="./index.php?r=backend/person/designer" title="设计师管理"><i class="entypo-flow-cascade"></i><span>设计师管理</span></a>
                                                                </li>
                                                                <!--                                                                <li>
                                                                                                                                    <a  id="user-control"<?php
//                                                                    if ($leftmenu['用户管理'] == 0) {
//                                                                        echo 'style="display:none"';
//                                                                    }
                                                                ?> class="tooltip-tip2 ajax-load" href="./index.php?r=backend/person/user" title="用户管理">
                                                                                                                                        <i class="entypo-flow-cascade"></i><span>用户管理</span>
                                                                                                                                    </a>
                                                                                                                                </li>-->
                                                            </ul>                                                            
                                                        </li>   
                                                    </ul>
                                                    <ul style="margin:10px" class="topnav menu-left-nest top-white" <?php
                                                    if ($leftmenu['权限'] == 0) {
                                                        echo 'style="display:none"';
                                                    }
                                                    ?>>
                                                        <li>
                                                            <a <?php
                                                            if ($leftmenu['权限'] == 0) {
                                                                echo 'style="display:none"';
                                                            }
                                                            ?> class="tooltip-tip ajax-load" href="#" title="权限">
                                                                <i class="fontawesome-eye-close"></i>
                                                                <span style="margin-left:3px;">权限</span>
                                                            </a>
                                                            <ul id="jurisdiction-open">
                                                                <li>
                                                                    <a id="roles-permissions"<?php
                                                                    if ($leftmenu['角色与权限'] == 0) {
                                                                        echo 'style="display:none"';
                                                                    }
                                                                    ?> class="tooltip-tip2 ajax-load" href="./index.php?r=backend/auth/role" title="角色与权限"><i class="entypo-flow-cascade"></i><span>角色与权限</span></a>
                                                                </li>
                                                                <li>
                                                                    <a id="permission-assignment"<?php
                                                                    if ($leftmenu['权限分配'] == 0) {
                                                                        echo 'style="display:none"';
                                                                    }
                                                                    ?> class="tooltip-tip2 ajax-load" href="./index.php?r=backend/auth/assignRole" title="权限分配"><i class="entypo-flow-cascade"></i><span>权限分配</span></a>
                                                                </li>
                                                            </ul>
                                                        </li>
                                                    </ul>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <!-- END OF SIDE MENU -->
