<?php

class baseController extends Controller {

    public $menu = array(
        //0代表不显示 1代表显示
        "项目" => 0, //项目
        "项目管理" => 0, //权限管理
        "节点管理" => 0, //节点管理
        "权限" => 0, //权限管理
        "角色与权限" => 0,
        "权限分配" => 0,
        "配置" => 0,
        "基础信息" => 0,
        "职位管理" => 0,
        "人员" => 0,
        "师傅管理" => 0,
        "用户管理" => 0,
        "设计师管理" => 0,
    );

    public function getLeftContent() {
        //获取左则导航栏
        $common = new commonController();
        $_administrator_role_id = Yii::app()->session['_administrator_role_id'];
        $role_menu_item = role_menu_item::model();
        $menu_item_model = menu_item::model();
        foreach ($this->menu as $k => &$v) {
            $menu_item_info = $menu_item_model->find("menu_item_name='$k'");
            $menu_item_id = $menu_item_info->menu_item_id;
            if (count($role_menu_item->find("_administrator_role_id = '$_administrator_role_id'and _menu_item_id ='$menu_item_id'")) > 0) {
                $v = 1;
            }
        }
        $date = Yii::app()->session['logintime'];
        $url = $common->webUrl . "?r=backend/recommend/leftmenu&username=" . Yii::app()->session['name'] . "&menu=" . json_encode($this->menu) . "&logintime=" . urlencode($date) . "&menu_name=" . Yii::app()->session['menu_name'];
        $structContent = file_get_contents($url);
        return $structContent;
    }

    /*
     * 检查权限
     * 输入权限名称
     */

    protected function checkAccess($menu_item_name, $filterChain) {
        if (isset(Yii::app()->session['_administrator_role_id'])) {
            $_administrator_role_id = Yii::app()->session['_administrator_role_id'];
            $role_menu_item_model = role_menu_item::model();
            $role_menu_item_info = $role_menu_item_model->findAll("_administrator_role_id = " . $_administrator_role_id);
            $menu_item_model = menu_item::model();
            $role_menu_id = $menu_item_model->find("menu_item_name = '$menu_item_name'")->menu_item_id;
            if (count($role_menu_item_info) > 0) {
                $flag = false;
                foreach ($role_menu_item_info as $K => $V) {
                    if ($V->_menu_item_id == $role_menu_id) {
                        $filterChain->run();
                        $flag = true;
                        break;
                    }
                }
                if (!$flag) {
                    $this->redirect('./index.php?r=backend/nonPrivilege/index');
                }
            } else {
                //无权访问
                $this->redirect('./index.php?r=backend/nonPrivilege/index');
            }
        } else {
            //无权访问
            $this->redirect('./index.php?r=backend/nonPrivilege/index');
        }
    }

}
