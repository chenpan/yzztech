<?php

include 'BaseController.php';

class authController extends baseController {

    public function getrecommend() {//获取CSS JS引用
        $common = new commonController();
        $url = $common->webUrl . "?r=backend/recommend/recommend";
        $structContent = file_get_contents($url);
        return $structContent;
    }

    public function filters() {
        return array(
            'Role + Role',
            'deleteRole + deleteRole',
            'assignRole + assignRole',
            'addAuth + addAuth',
            'addRole + addRole',
            'editRole + editRole',
            'addAdmin + addAdmin',
            'editAdmin + modify',
        );
    }

    public function filterRole($filterChain) {
        $this->checkAccess("角色与权限", $filterChain);
    }

    public function filterassignRole($filterChain) {
        $this->checkAccess("权限分配", $filterChain);
    }

    public function filteraddAuth($filterChain) {
        $this->checkAccess("创建权限", $filterChain);
    }

    public function filteraddRole($filterChain) {
        $this->checkAccess("创建角色", $filterChain);
    }

    public function filterdeleteRole($filterChain) {
        $this->checkAccess("删除角色", $filterChain);
    }

    public function filtereditRole($filterChain) {
        $this->checkAccess("编辑角色", $filterChain);
    }

    public function filteraddAdmin($filterChain) {
        $this->checkAccess("添加管理员", $filterChain);
    }

    public function filtereditAdmin($filterChain) {
        $this->checkAccess("编辑管理员", $filterChain);
    }

    public function actionRole() {
        $leftContent = $this->getLeftContent();
        $recommend = $this->getrecommend();
        $administrator_role_model = administrator_role::model();
        $administrator_role_info = $administrator_role_model->findAll();

        //删除功能权限判断
        $name = Yii::app()->session['name'];
        $administrator_model = administrator::model();
        $role_menu_item_model = role_menu_item::model();
        $menu_item_model = menu_item::model();
        $_administrator_role_id = $administrator_model->find("name='$name'")->_administrator_role_id;
        $role_menu_item_info = $role_menu_item_model->findAll("_administrator_role_id='$_administrator_role_id'");
        $delete_status = "hidden";
        $add_status = "hidden";
        $edit_status = "hidden";
        $add_qx_status = "hidden";
        foreach ($role_menu_item_info as $value) {
            $_menu_item_id = $value->_menu_item_id;
            $menu_item_info = $menu_item_model->find("menu_item_id ='$_menu_item_id'");
            $menu_item_name = $menu_item_info->menu_item_name;
            if ($menu_item_name == "创建角色") {
                $add_status = "";
            }
            if ($menu_item_name == "编辑角色") {
                $edit_status = "";
            }
            if ($menu_item_name == "删除角色") {
                $delete_status = "";
            }
            if ($menu_item_name == "创建权限") {
                $add_qx_status = "";
            }
        }

        $this->renderPartial('role', array('administrator_role_info' => $administrator_role_info, 'leftContent' => $leftContent, 'recommend' => $recommend, "add_status" => $add_status, "edit_status" => $edit_status, "delete_status" => $delete_status, "add_qx_status" => $add_qx_status));
    }

    /*
     * 删除角色
     */

    public function actiondeleteRole() {
        if (isset($_POST['id'])) {
            $id = $_POST['id'];
            $administrator_role_model = administrator_role::model();
            $role_menu_item_model = role_menu_item::model();
            if ((count($administrator_role_model->deleteAll("administrator_role_id=" . $id)) && count($role_menu_item_model->deleteAll("_administrator_role_id=" . $id)) ) > 0) {
                $json = '{"data":"success"}';
            } else {
                $json = '{"data":"false"}';
            }
            echo $json;
        }
    }

    /*
     * 分配角色
     */

    public function actionassignRole() {
        //管理员
        $leftContent = $this->getLeftContent();
        $recommend = $this->getrecommend();
        $administrator_model = administrator::model();
        $administrator_info = $administrator_model->findAll("isdeleted = 0");
        //角色名称
        $administrator_role_model = administrator_role::model();
        $administrator_role_info = $administrator_role_model->findAll(); //"administrator_role_name != '超级管理员'"
        //权限判断
        $name = Yii::app()->session['name'];
        $role_menu_item_model = role_menu_item::model();
        $menu_item_model = menu_item::model();
        $_administrator_role_id = $administrator_model->find("name='$name'")->_administrator_role_id;
        $role_menu_item_info = $role_menu_item_model->findAll("_administrator_role_id='$_administrator_role_id'");
        $add_status = "hidden";
        $edit_status = "hidden";
        $delete_status = "hidden";
        foreach ($role_menu_item_info as $value) {
            $_menu_item_id = $value->_menu_item_id;
            $menu_item_info = $menu_item_model->find("menu_item_id ='$_menu_item_id'");
            $menu_item_name = $menu_item_info->menu_item_name;
            if ($menu_item_name == "添加管理员") {
                $add_status = "";
            }
            if ($menu_item_name == "编辑管理员") {
                $edit_status = "";
            }
            if ($menu_item_name == "删除管理员") {
                $delete_status = "";
            }
        }

        $this->renderPartial('assignRole', array('administrator_info' => $administrator_info, 'administrator_role_info' => $administrator_role_info, 'leftContent' => $leftContent, 'recommend' => $recommend, "add_status" => $add_status, "edit_status" => $edit_status, "delete_status" => $delete_status)); //'admin_info' => $admin_info, 'role_info' => $administrator_role_info, 'page_list' => $page_list,
    }

    public function actionassignData() {
//        if (!(isset($_SERVER['HTTP_X_REQUESTED_WITH']) ? $_SERVER['HTTP_X_REQUESTED_WITH'] === 'XMLHttpRequest' : false)) {
//            $this->redirect('./index.php?r=backend/default/index');
//        }
        $administrator_model = administrator::model();
        $role = role::model();
        $admin_info = $administrator_model->findAll();
        $administrator_role_info = $role->findAll();
        $i = 0;
        foreach ($administrator_role_info as $v) {
            $roleName = $v->rolename;
            $data1[] = $roleName;
        }
        foreach ($admin_info as $value) {
            $i++;
            $name = $value->username;
            $data2[] = array(
                "id" => $i,
                "name" => $name,
                "role" => $data1
            );
        }
        $data3 = array(
            "draw" => 0,
            "recordsTotal" => $i,
            "recordsFiltered" => $i,
            'data' => $data2,
        );
        echo $data = json_encode($data3);
    }

    public function actionaddAuth() {
        $leftContent = $this->getLeftContent();
        $recommend = $this->getrecommend();
        $menu_item_model = menu_item::model();
        $menu_item_info = $menu_item_model->findAll("parent_item_id = 0");
        $this->renderPartial('addAuth', array('menu_item_info' => $menu_item_info, 'leftContent' => $leftContent, 'recommend' => $recommend));
    }

    public function actionsaveAuth() {//保存新增权限
        $authitem_model = menu_item::model();
        if (isset($_POST['parentItem1'])) {
            $auth = $_POST['parentItem1']; //新增权限
            $authchild = $_POST['childTtem1'];
            $itemName1 = $authitem_model->find("menu_item_name = '$auth'");
            if (!$itemName1) {
                $item_model = new menu_item();
                $item_model->menu_item_name = $auth;
                $item_model->parent_item_id = 0;
                if ($item_model->save()) {
                    if ($authchild != NULL) {
                        $itemInfo = $authitem_model->find("menu_item_name = '$auth'");
                        $itemId = $itemInfo->menu_item_id;
                        $itemchild_model = new menu_item();
                        $itemchild_model->menu_item_name = $authchild;
                        $itemchild_model->parent_item_id = $itemId;
                        if ($itemchild_model->save()) {
                            $json = '{"data":"success","datachild":"success"}';
                            echo $json;
                        } else {
                            $json = '{"data":"success","datachild":"false"}';
                            echo $json;
                        }
                    } else {
                        $json = '{"data":"success","datachild":"none"}';
                        echo $json;
                    }
                } else {
                    $json = '{"data":"false","datachild":"false"}';
                    echo $json;
                }
            } else {
                $json = '{"data":"namehad"}';
                echo $json;
            }
        }
        if (isset($_POST['childTtem'])) {  //已有权限添加子权限
            $parentauth = $_POST['parentItem'];
            $childauth = $_POST['childTtem'];
            $count = count($authitem_model->find("menu_item_name='$childauth'"));
            if ($count > 0) {
                $json = '{"data":"namehad"}';
                echo $json;
            } else {
                $item_model1 = new menu_item();
                $itemName = $authitem_model->find("menu_item_name = '$parentauth'");
                $parentId = $itemName->menu_item_id;
                $item_model1->menu_item_name = $childauth;
                $item_model1->parent_item_id = $parentId;
                if ($item_model1->save()) {
                    $json = '{"data":"success"}';
                    echo $json;
                } else {
                    $json = '{"data":"false"}';
                    echo $json;
                }
            }
        }
    }

//
//    /*
//     * 为管理员分配角色
//     */
//
    public function actionassign() {
        if (isset($_POST['adminID']) && isset($_POST['roleID'])) {
            $administratorid = $_POST['adminID'];
            $_roleID = $_POST['roleID'];
            $administrator_model = administrator::model();
            $admin_info = $administrator_model->find("administratorid=" . $administratorid);
            $admin_info->_roleid = $_roleID;
            if ($admin_info->save()) {
                $json = '{"data":"success"}';
            } else {
                $json = '{"data":"false"}';
            }
            echo $json;
        }
    }

    /*
     * 删除管理员
     */

    public function actionassignDel() {
        if (isset($_POST['adminId'])) {
            $administratorid = $_POST['adminId'];
            $administrator_model = administrator::model();
            $admin_info = $administrator_model->find("administrator_id=" . $administratorid);
            agent_printor::model()->deleteAll(array('condition' => "administrator_id = $administratorid"));
            administrator_school::model()->deleteAll(array('condition' => "administrator_id = $administratorid"));
            $admin_info->isdeleted = 1;
            if ($admin_info->save()) {
                $json = '{"data":"success"}';
            } else {
                $json = '{"data":"false"}';
            }
            echo $json;
        }
    }

    /*
     * 创建角色
     */

    public function actionaddRole() {
        $leftContent = $this->getLeftContent();
        $recommend = $this->getrecommend();
        $menu_item_model = menu_item::model();
        $menu_item_info = $menu_item_model->findAll("parent_item_id = 0");
        $this->renderPartial('addRole', array('menu_item_info' => $menu_item_info, 'leftContent' => $leftContent, 'recommend' => $recommend));
    }

    /*
     * 
     */

    public function actionsaveRole() { //保存创建的角色
        $administrator_role_model = new administrator_role();
        if (isset($_POST)) {
            $authrole = $_POST['role'];
            $administrator_role_model->administrator_role_name = $authrole;
            $administrator_role_model->save();
            $authrole_model = administrator_role::model();
            $authItem_model = menu_item::model();
            $administrator_role_info = $authrole_model->find("administrator_role_name='$authrole'");
            $flag = TRUE;
            $chidflag = true;
            if (isset($_POST['authparent'])) {  //父权限
                foreach ($_POST['authparent'] as $value) {
                    $authassignment_model = new role_menu_item();
                    $administrator_role_id = $administrator_role_info->administrator_role_id;
                    $authassignment_model->_administrator_role_id = $administrator_role_id;
                    $item_info = $authItem_model->find("menu_item_name='$value'");
                    $menu_item_id = $item_info->menu_item_id;
                    $authassignment_model->_menu_item_id = $menu_item_id;
                    if (!$authassignment_model->save() > 0) {
                        $flag = FALSE;
                    }
                }
            }
            if (isset($_POST['authchild'])) { // 子权限
                foreach ($_POST['authchild'] as $value) {
                    $authassignment_model1 = new role_menu_item();
                    $role_id = $administrator_role_info->administrator_role_id;
                    $authassignment_model1->_administrator_role_id = $role_id;
                    $item_info = $authItem_model->find("menu_item_name='$value'");
                    $authassignment_Cid = $item_info->menu_item_id;
                    $authassignment_model1->_menu_item_id = $authassignment_Cid;
                    if (!$authassignment_model1->save()) {
                        $chidflag = FALSE;
                    }
                }
            }
            if ($flag || $chidflag) {
                echo "<script>window.location.href='./index.php?r=backend/auth/role';</script>";
            } else {
                echo "<script>alert('创建失败！');window.location.href='./index.php?r=backend/auth/role';</script>";
            }
        }
    }

    /*
     * 编辑角色
     */

    public function actioneditRole($roleid) {
        $leftContent = $this->getLeftContent();
        $recommend = $this->getrecommend();
        $administrator_role_model = administrator_role::model();
        $authitem_model = menu_item::model();
        $assignment = role_menu_item::model();
        $authrole_info = $administrator_role_model->find("administrator_role_id='$roleid'");
        $assignment_info = $assignment->findAll("_administrator_role_id='$roleid'");
        $assign_info = $authitem_model->findAll("parent_item_id=0");
        $auth_name = array();
        foreach ($assign_info as $value) {
            $id = $value->menu_item_id;
            $name = $value->menu_item_name;
            foreach ($assignment_info as $auth_info) {
                $auth_id = $auth_info->_menu_item_id;
                $assig_id = $authitem_model->find("menu_item_id='$auth_id'");
                $parent_id = $assig_id->parent_item_id;
                $parent_name = $assig_id->menu_item_name;
                if ($parent_id == 0 && $parent_name == $name) {
                    $auth_name[$id][0] = $assig_id->menu_item_name;
                } else if ($parent_id == $id) {
                    $auth_name[$id][$auth_id] = $assig_id->menu_item_name;
                }
            }
        }
        $this->renderPartial('editRole', array('authrole_info' => $authrole_info, 'auth_name' => $auth_name, 'assign_info' => $assign_info, 'roleid' => $roleid, 'leftContent' => $leftContent, 'recommend' => $recommend));
    }

    public function actioneditupdateRole() {//编辑角色数据保存
        $roleid = $_POST['roleid'];
        $administrator_role_model = administrator_role::model();
        $authroleid = $roleid;
        $authassignment_model = role_menu_item::model();
        $item_model = menu_item::model();
        $count = $authassignment_model->deleteAll("_administrator_role_id = '$authroleid'");
        if (isset($_POST)) {
            $roleId = $_POST["role"];
            $auth_model_info = $administrator_role_model->find("administrator_role_id = '$authroleid'");
            $roleName = $_POST["role"];
            $auth_model_info->administrator_role_id = $_POST["roleid"];
            $auth_model_info->administrator_role_name = $roleName;
            $count = $auth_model_info->update(array('administrator_role_id', 'administrator_role_name'));
            $flag = TRUE;
            if (isset($_POST['authparent'])) { //父权限
                foreach ($_POST['authparent'] as $value) {
                    $authupdate_model = new role_menu_item();
                    $parent_info = $item_model->find("menu_item_name='$value'");
                    $id = $parent_info->menu_item_id;
                    $authupdate_model->_administrator_role_id = $authroleid;
                    $authupdate_model->_menu_item_id = $id;
                    if (!$authupdate_model->save()) {
                        $flag = FALSE;
                    }
                }
            }
            if (isset($_POST['authchild'])) {//子权限
                foreach ($_POST['authchild'] as $value) {
                    $authupdate_model1 = new role_menu_item();
                    $parent_info = $item_model->find("menu_item_name='$value'");
                    $id = $parent_info->menu_item_id;
                    $authupdate_model1->_administrator_role_id = $authroleid;
                    $authupdate_model1->_menu_item_id = $id;
                    if (!$authupdate_model1->save()) {
                        $flag = FALSE;
                    }
                }
            }
            if ($flag) {
                echo "<script>parent.$('.save_lable').text('保存成功！').css('color','red');</script>";
            } else {
                echo "<script>parent.$('.save_lable').text('保存失败！').css('color','red');</script>";
            }
        }
    }

    /*
     * 添加管理员 
     */

    public function actionaddAdmin() {
        $leftContent = $this->getLeftContent();
        $recommend = $this->getrecommend();
        if (isset($_POST["username"])) {
            $username = addslashes($_POST["username"]);
            $password = md5($_POST["password"]);
            $phone = $_POST["phone"];
            $QQ = $_POST["QQ"];
            $roleId = $_POST["role"];
            $administrator_model = new administrator();
            $administrator_info = $administrator_model->find("cellphone = '$phone' and isdeleted = 0");
            if (count($administrator_info) == 0) {
                $administrator_model->name = $username;
                $administrator_model->password = $password;
                $administrator_model->_administrator_role_id = $roleId;
                $administrator_model->cellphone = $phone;
                $administrator_model->qq = $QQ;
                $administrator_model->createtime = date('Y-m-d H:i:s', time());
                if ($administrator_model->save()) {
                    echo "<script>parent.$('#add_success').text('保存成功！');</script>";
                } else {
                    echo "<script>parent.$('#add_success').text('保存失败！');</script>";
                }
            } else {
                echo "<script>parent.$('#username_error').text('用户手机号已存在！');</script>";
            }
        } else {
            $administrator_role_modell = administrator_role::model();
            $administrator_role_info = $administrator_role_modell->findAll(); //"administrator_role_name != '超级管理员'"
            $this->renderPartial('addAdmin', array('administrator_role_info' => $administrator_role_info, 'leftContent' => $leftContent, 'recommend' => $recommend));
        }
    }

    /*     * *******************判断手机号是否存在 start******************** */

    public function actioncheck_cellphone() {
        $cellphone = $_POST["cellphone"];
        $administrator_info = administrator::model()->find("cellphone = '" . $cellphone . "' and isdeleted = 0");
        if (count($administrator_info) == 0) {
            $json = '{"data":"false"}';
            echo $json;
        } else {
            $json = '{"data":"true"}';
            echo $json;
        }
    }

    /*     * *******************判断手机号是否存在 end******************** */

    /*
     * 新增管理员时选择终端
     */

    public function actionsearchmachine() {
        $school_ids = $_POST["school_id"];
        if ($school_ids[0] == "multiselect-all") {
            $machine_info = machine::model()->findAll();
        } else {
            $strt = "";
            foreach ($school_ids as $k => $l) {
                $strt .= $l . ",";
            }
            $strt = substr($strt, 0, -1);

            $machine_group_info = machine_group::model()->findAll(array("condition" => "school_id in ($strt)"));
            $strt = "";
            foreach ($machine_group_info as $k => $l) {
                $strt .= $l->machine_group_id . ",";
            }
            $strt = substr($strt, 0, -1);

            $machine_info = machine::model()->findAll(array("condition" => "machine_group_id in ($strt)"));
        }

        $machine_infox = array(); //返回的数组
        foreach ($machine_info as $k => $v) {
            array_push($machine_infox, array("machine_id" => $v->machine_id, "machine_friendlyname" => $v->machine_friendlyname));
        }
        $machine_infox = json_encode($machine_infox);
        echo $machine_infox;
    }

    /*
     * 编辑管理员
     */

    public function actionmodify($adminID) {
        $leftContent = $this->getLeftContent();
        $recommend = $this->getrecommend();
        $administrator_model = administrator::model();
        if (isset($_POST["administratorid"])) {
            $administratorid = $_POST["administratorid"];
            $username = addslashes($_POST["username"]);
            $password = md5($_POST["password"]);
            $roleId = $_POST["role"];
            $phone = $_POST["phone"];
            $QQ = $_POST["QQ"];
            $administrator_model = administrator::model();

            $administrator_infos = $administrator_model->findAll("cellphone = '$phone' and administrator_id != $administratorid and isdeleted = 0");
            if (count($administrator_infos) == 0) {
                $administrator_info = $administrator_model->findByPk($administratorid);
                if ($username != "") {
                    $administrator_info->name = $username;
                }
                if ($_POST["password"] != "") {
                    $administrator_info->password = $password;
                }
                $administrator_info->_administrator_role_id = $roleId;
                if ($phone != "") {
                    $administrator_info->cellphone = $phone;
                }
                if ($QQ != "") {
                    $administrator_info->qq = $QQ;
                }

                if ($administrator_info->save()) {
                    echo "<script>parent.$('#save_success').text('保存成功！');</script>";
                } else {
                    echo "<script>parent.$('#save_success').text('保存失败！');</script>";
                }
            } else {
                echo "<script>parent.$('#username_error').text('手机号已存在！');</script>";
            }
        } else {
            $administrator_role_modell = administrator_role::model();
            $administrator_role_info = $administrator_role_modell->findAll(); //"administrator_role_name != '超级管理员'"
            $admin_info = $administrator_model->find("administrator_id= '$adminID'");
            $roleId = $admin_info->_administrator_role_id;

            $this->renderPartial('modify', array('admin_info' => $admin_info, 'roleId' => $roleId, 'role_info' => $administrator_role_info, 'leftContent' => $leftContent, 'recommend' => $recommend));
        }
    }

}
