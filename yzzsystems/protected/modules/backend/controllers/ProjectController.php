<?php

include 'BaseController.php';

class ProjectController extends baseController {

    public function getrecommend() {//获取CSS JS引用
        $common = new commonController();
        $url = $common->webUrl . "?r=backend/recommend/recommend";
        $structContent = file_get_contents($url);
        return $structContent;
    }

    /*
      权限管理
     */

    public function filters() {
        return array(
            'project + project',
            'addproject + addproject',
            'deleteproject + deleteproject',
            'schedule + schedule',
            'addschedule + addschedule',
            'deleteschedule + deleteschedule',
            'changestep + changestep',
            'project_schedule + project_schedule',
            'change_project_schedule_step + change_project_schedule_step',
            'project_schedule_info + project_schedule_info',
            'project_schedule_picture + project_schedule_picture',
            'add_project_schedule_picture + add_project_schedule_picture',
            'edit_project_schedule_picture + edit_project_schedule_picture',
            'del_project_schedule_picture + del_project_schedule_picture'
        );
    }

    public function filterproject($filterChain) {
        $this->checkAccess("项目管理", $filterChain);
    }

    public function filteraddproject($filterChain) {
        $this->checkAccess("添加项目", $filterChain);
    }

    public function filterdeleteproject($filterChain) {
        $this->checkAccess("删除项目", $filterChain);
    }

    public function filterschedule($filterChain) {
        $this->checkAccess("节点管理", $filterChain);
    }

    public function filteraddschedule($filterChain) {
        $this->checkAccess("添加节点", $filterChain);
    }

    public function filterdeleteschedule($filterChain) {
        $this->checkAccess("删除节点", $filterChain);
    }

    public function filterchangestep($filterChain) {
        $this->checkAccess("更新顺序", $filterChain);
    }

    public function filterproject_schedule($filterChain) {
        $this->checkAccess("项目节点", $filterChain);
    }

    public function filterchange_project_schedule_step($filterChain) {
        $this->checkAccess("更新项目节点", $filterChain);
    }

    public function filterproject_schedule_info($filterChain) {
        $this->checkAccess("编辑项目节点", $filterChain);
    }

    public function filterproject_schedule_picture($filterChain) {
        $this->checkAccess("项目节点图片", $filterChain);
    }

    public function filteradd_project_schedule_picture($filterChain) {
        $this->checkAccess("新增节点图片", $filterChain);
    }

    public function filteredit_project_schedule_picture($filterChain) {
        $this->checkAccess("编辑节点图片", $filterChain);
    }

    public function filterdel_project_schedule_picture($filterChain) {
        $this->checkAccess("删除节点图片", $filterChain);
    }

    /*     * ************** 项目管理 start ************** */

    public function actionproject() {
        if (isset(Yii::app()->session['name'])) {
            $leftContent = $this->getLeftContent();
            $recommend = $this->getrecommend();

            //权限判断
            $name = Yii::app()->session['name'];
            $administrator_model = administrator::model();
            $role_menu_item_model = role_menu_item::model();
            $menu_item_model = menu_item::model();
            $_administrator_role_id = $administrator_model->find("name='$name'")->_administrator_role_id;
            $role_menu_item_info = $role_menu_item_model->findAll("_administrator_role_id='$_administrator_role_id'");
            $delete_project = "hidden";
            $add_project = "hidden";
            $edit_project = "hidden";
            foreach ($role_menu_item_info as $value) {
                $_menu_item_id = $value->_menu_item_id;
                $menu_item_info = $menu_item_model->find("menu_item_id ='$_menu_item_id'");
                $menu_item_name = $menu_item_info->menu_item_name;
                if ($menu_item_name == "删除项目") {
                    $delete_project = "";
                }
                if ($menu_item_name == "添加项目") {
                    $add_project = "";
                }
                if ($menu_item_name == "编辑项目") {
                    $edit_project = "";
                }
            }
            $project_model = project::model();
            $project_info = $project_model->findAllBySql("select * from project where isdeleted = 0", array('order' => "project_id DESC")); // and _administrator_id = " . Yii::app()->session["administrator_id"]

            $this->renderPartial('project', array('leftContent' => $leftContent, 'recommend' => $recommend, 'project_info' => $project_info, 'add_project' => $add_project, 'edit_project' => $edit_project, 'delete_project' => $delete_project));
        } else {
            $this->redirect('./index.php?r=backend/default/index');
        }
    }

    /*     * ************** 项目管理 end ************** */
    /*     * ************** 删除项目 start************** */

    public function actiondeleteproject() {
        $project_id = $_POST["project_id"];
        $project_model = project::model();

        $project_info = $project_model->findByPk($project_id);
        if (count($project_info) != 0) {
            $project_info->isdeleted = 1;
        }

        # 删除项目和用户的关系

        project_userid::model()->deleteAll("project_id = $project_id");


        if ($project_info->save()) {
            $json = '{"data":"success"}';
            echo $json;
        } else {
            $json = '{"data":"false"}';
            echo $json;
        }
    }

    /*     * ************** 删除项目 end************** */

    /*     * ************** 添加|编辑项目 start ************** */

    public function actionaddproject($project_id = 0) {
        if (isset(Yii::app()->session['name'])) {
            $leftContent = $this->getLeftContent();
            $recommend = $this->getrecommend();

            if (isset($_POST['project_name'])) {
                if ($_POST['project_id'] == 0) {
                    $project_model = new project();
                } else {
                    $project_modelx = project::model();
                    $project_model = $project_modelx->findByPk($_POST['project_id']);

                    project_userid::model()->deleteAll(array('condition' => "project_id =" . $_POST['project_id']));
                }
                $project_name = $_POST['project_name'];
                $project_model->project_name = $project_name;
                $project_model->project_address = $_POST['project_address'];
                $project_model->project_descript = $_POST['project_descript'];
                $project_model->area = $_POST['area'];
                $project_model->house_type = $_POST['house_type'];
                $project_model->decoration_budget = $_POST['decoration_budget'];
                $project_model->_administrator_id = $_POST['_administrator_id'];
                $project_model->contract_time = $_POST['contract_time'];
                if ($project_model->save()) {

                    if ($_FILES["complete_add"]["size"] > 0) {
                        $file_type = substr(strrchr($_FILES['complete_add']['name'], '.'), 1);  // 后缀
                        $project_idx = $project_model->project_id; // ID
                        move_uploaded_file($_FILES["complete_add"]["tmp_name"], iconv("UTF-8", "gb2312", "./project_pic/" . $project_idx . "." . $file_type));
                        $project_info = $project_model->find("project_id = $project_idx");
                        $project_info->complete_add = "./project_pic/" . $project_idx . "." . $file_type;
                        $project_info->save();
                    }


                    $user_id = $_POST['user_id'];
                    if ($user_id[0] == "multiselect-all") {
                        $i = 1;
                    } else {
                        $i = 0;
                    }
                    #保存进用户和项目关联表
                    for ($i; $i < count($user_id); $i++) {
                        $project_userid_model = new project_userid;
                        $project_userid_model->project_id = $project_model->project_id;
                        $project_userid_model->_user_id = $user_id[$i];
                        $project_userid_model->save();
                    }
                    if ($_POST['project_id'] == 0) {
                        #从节点表导出数据到项目进度里面
                        $schedule_model = schedule::model();
                        $schedule_info = $schedule_model->findAll();

                        foreach ($schedule_info as $v => $l) {
                            $project_schedule_model = new project_schedule();
                            $project_schedule_model->_project_id = $project_model->project_id;
                            $project_schedule_model->schedule_name = $l->schedule_name;
                            $project_schedule_model->schedule_descript = $l->schedule_descript;
                            $project_schedule_model->step = $l->step;
                            $project_schedule_model->not_begin_pic_add = $l->not_begin_pic_add;
                            $project_schedule_model->begin_pic_add = $l->begin_pic_add;
                            $project_schedule_model->save();
                        }
                    }
                    $this->redirect('./index.php?r=backend/project/project');
                }
            } else {
                $administrator_model = administrator::model();
                $administrator_info = $administrator_model->findAll(array('select' => array('administrator_id', 'name'), 'condition' => 'isdeleted = 0'));

                $user_model = user::model();
                $user_info = $user_model->findAll(array('select' => array('user_id', 'name', 'cellphone'), 'condition' => 'isdeleted = 0'));
                if ($project_id == 0) {//新增
                    $this->renderPartial('addproject', array('leftContent' => $leftContent, 'recommend' => $recommend, 'administrator_info' => $administrator_info, 'user_info' => $user_info));
                } else {//编辑
                    $project_info = project::model()->findByPk(array($project_id, 'condition' => 'isdeleted = 0'));


                    $user_id_str = "";
                    $project_userid_info = project_userid::model()->findAll("project_id = '$project_id'");
                    foreach ($project_userid_info as $k => $l) {
                        $user_id_str .= "'" . $l->_user_id . "',";
                    }
                    $user_id_str = substr($user_id_str, 0, -1);

                    $this->renderPartial('addproject', array('project_info' => $project_info, 'leftContent' => $leftContent, 'recommend' => $recommend, 'administrator_info' => $administrator_info, 'user_info' => $user_info, 'user_id_str' => $user_id_str));
                }
            }
        } else {
            $this->redirect('./index.php?r=backend/default/index');
        }
    }

    /*     * ************** 添加|编辑项目 end ************** */
    /*     * ************** 节点管理 start ************** */

    public function actionschedule() {
        if (isset(Yii::app()->session['name'])) {
            $leftContent = $this->getLeftContent();
            $recommend = $this->getrecommend();

//            权限判断
            $name = Yii::app()->session['name'];
            $administrator_model = administrator::model();
            $role_menu_item_model = role_menu_item::model();
            $menu_item_model = menu_item::model();
            $_administrator_role_id = $administrator_model->find("name='$name'")->_administrator_role_id;
            $role_menu_item_info = $role_menu_item_model->findAll("_administrator_role_id='$_administrator_role_id'");
            $delete_schedule = "hidden";
            $add_schedule = "hidden";
            $edit_schedule = "hidden";
            $change_project_step = "hidden";
            foreach ($role_menu_item_info as $value) {
                $_menu_item_id = $value->_menu_item_id;
                $menu_item_info = $menu_item_model->find("menu_item_id ='$_menu_item_id'");
                $menu_item_name = $menu_item_info->menu_item_name;
                if ($menu_item_name == "删除节点") {
                    $delete_schedule = "";
                }
                if ($menu_item_name == "添加节点") {
                    $add_schedule = "";
                }
                if ($menu_item_name == "编辑节点") {
                    $edit_schedule = "";
                }
                if ($menu_item_name == "更新顺序") {
                    $change_project_step = "";
                }
            }
            $schedule_model = schedule::model();
            $schedule_info = $schedule_model->findAll(array('order' => "step asc"));

            $this->renderPartial('schedule', array('leftContent' => $leftContent, 'recommend' => $recommend, 'schedule_info' => $schedule_info, 'add_schedule' => $add_schedule, 'edit_schedule' => $edit_schedule, 'delete_schedule' => $delete_schedule, 'change_project_step' => $change_project_step));
        } else {
            $this->redirect('./index.php?r=backend/default/index');
        }
    }

    /*     * ************** 节点管理 end ************** */
    /*     * ************** 添加|编辑节点 start ************** */

    public function actionaddschedule($schedule_id = 0) {
        if (isset(Yii::app()->session['name'])) {
            $leftContent = $this->getLeftContent();
            $recommend = $this->getrecommend();

            if (isset($_POST['schedule_name'])) {
                $schedule_modelx = schedule::model();

                if ($_POST['schedule_id'] == 0) {
                    $schedule_model = new schedule();
                    #新增时，把节点顺序设置为比最大的大1
                    $schedule_info_step = $schedule_modelx->find(array('order' => "step DESC"));
                    if (count($schedule_info_step) != 0) {
                        $schedule_model->step = $schedule_info_step->step + 1;
                    }
                } else {
                    $schedule_model = $schedule_modelx->findByPk($_POST['schedule_id']);
                }

                $schedule_name = $_POST['schedule_name'];
                $schedule_model->schedule_name = $schedule_name;
                $schedule_model->schedule_descript = $_POST['schedule_descript'];

                if ($schedule_model->save()) {


                    if ($_FILES["not_begin_pic_add"]["size"] > 0) {
                        $file_type = substr(strrchr($_FILES['not_begin_pic_add']['name'], '.'), 1);  // 后缀
                        $schedule_idx = $schedule_model->schedule_id; // ID
                        move_uploaded_file($_FILES["not_begin_pic_add"]["tmp_name"], iconv("UTF-8", "gb2312", "./schedule_pic/s" . $schedule_idx . "_gray." . $file_type));
                        $schedule_info = $schedule_model->find("schedule_id = $schedule_idx");
                        $schedule_info->not_begin_pic_add = "./schedule_pic/s" . $schedule_idx . "_gray." . $file_type;
                        $schedule_info->save();
                    }

                    if ($_FILES["begin_pic_add"]["size"] > 0) {
                        $file_type = substr(strrchr($_FILES['begin_pic_add']['name'], '.'), 1);  // 后缀
                        $schedule_idx = $schedule_model->schedule_id; // ID
                        move_uploaded_file($_FILES["begin_pic_add"]["tmp_name"], iconv("UTF-8", "gb2312", "./schedule_pic/s" . $schedule_idx . "_green." . $file_type));
                        $schedule_info = $schedule_model->find("schedule_id = $schedule_idx");
                        $schedule_info->begin_pic_add = "./schedule_pic/s" . $schedule_idx . "_green." . $file_type;
                        $schedule_info->save();
                    }
                    $this->redirect('./index.php?r=backend/project/schedule');
                }
            } else {
                if ($schedule_id == 0) {//新增
                    $this->renderPartial('addschedule', array('leftContent' => $leftContent, 'recommend' => $recommend));
                } else {//编辑
                    $schedule_info = schedule::model()->findByPk($schedule_id);
                    $this->renderPartial('addschedule', array('schedule_info' => $schedule_info, 'leftContent' => $leftContent, 'recommend' => $recommend));
                }
            }
        } else {
            $this->redirect('./index.php?r=backend/default/index');
        }
    }

    /*     * ************** 添加|编辑节点 end ************** */

    /*     * ************** 删除节点 start************** */

    public function actiondeleteschedule() {
        $schedule_id = $_POST["schedule_id"];
        $schedule_model = schedule::model();

        $count = $schedule_model->deleteByPk($schedule_id);

        if ($count > 0) {
            $json = '{"data":"success"}';
            echo $json;
        } else {
            $json = '{"data":"false"}';
            echo $json;
        }
    }

    /*     * ************** 删除节点 end************** */
    /*     * ************** 更新节点顺序 start************** */

    public function actionchangestep() {
        $step = $_POST["step"]; //step=1&step=2&step=3

        $step_rep = str_replace('step=', '', $step); //把step=替换为空格

        $step_array = explode('&', $step_rep); //ID 1,2,3

        $schedule_model = schedule::model();
        $schedule_info = $schedule_model->findAll(array('order' => "step asc"));

        $status = TRUE;

        foreach ($schedule_info as $k => $l) {
            $x = $step_array[$k];
            $l->step = $step_array[$k];
            if (!$l->save()) {
                $status = FALSE;
            }
        }


        if ($status) {
            $json = '{"data":"success"}';
            echo $json;
        } else {
            $json = '{"data":"false"}';
            echo $json;
        }
    }

    /*     * ************** 更新节点顺序 end************** */
    /*     * ************** 项目的节点管理 start ************** */

    public function actionproject_schedule($project_id = 0) {
        if (isset(Yii::app()->session['name'])) {
            $leftContent = $this->getLeftContent();
            $recommend = $this->getrecommend();

//            权限判断
            $name = Yii::app()->session['name'];
            $administrator_model = administrator::model();
            $role_menu_item_model = role_menu_item::model();
            $menu_item_model = menu_item::model();
            $_administrator_role_id = $administrator_model->find("name='$name'")->_administrator_role_id;
            $role_menu_item_info = $role_menu_item_model->findAll("_administrator_role_id='$_administrator_role_id'");
//            $delete_project_schedule = "hidden";
            $edit_project_schedule = "hidden";
            $change_project_step = "hidden";
            foreach ($role_menu_item_info as $value) {
                $_menu_item_id = $value->_menu_item_id;
                $menu_item_info = $menu_item_model->find("menu_item_id ='$_menu_item_id'");
                $menu_item_name = $menu_item_info->menu_item_name;
                if ($menu_item_name == "更新项目节点") {
                    $change_project_step = "";
                }
                if ($menu_item_name == "编辑项目节点") {
                    $edit_project_schedule = "";
                }
                if ($menu_item_name == "删除项目节点") {
                    $del_project_schedule = "";
                }
            }
            $project_schedule_model = project_schedule::model();
            $project_schedule_info = $project_schedule_model->findAll(array('condition' => "isdeleted = 0 and _project_id = " . $project_id, 'order' => "step asc"));

            $this->renderPartial('project_schedule', array('leftContent' => $leftContent, 'recommend' => $recommend,
                'project_schedule_info' => $project_schedule_info, 'edit_project_schedule' => $edit_project_schedule,
                'del_project_schedule' => $del_project_schedule, 'change_project_step' => $change_project_step, 'project_id' => $project_id));
        } else {
            $this->redirect('./index.php?r=backend/default/index');
        }
    }

    /*     * ************** 项目的节点管理 end ************** */

    /*     * ************** 更新项目节点 start************** */

    public function actionchange_project_schedule_step() {
        $step = $_POST["step"]; //step=1&step=2&step=3
        $project_id = $_POST["project_id"];

        $step_rep = str_replace('step=', '', $step); //把step=替换为空格

        $step_array = explode('&', $step_rep); //ID 1,2,3

        $project_schedule_model = project_schedule::model();
        $project_schedule_info = $project_schedule_model->findAll(array('condition' => "isdeleted = 0 and _project_id = $project_id", 'order' => "step asc"));

        $status = TRUE;

        foreach ($project_schedule_info as $k => $l) {
            $x = $step_array[$k];
            $l->step = $step_array[$k];
            if (!$l->save()) {
                $status = FALSE;
            }
        }
        if ($status) {
            $json = '{"data":"success"}';
            echo $json;
        } else {
            $json = '{"data":"false"}';
            echo $json;
        }
    }

    /*     * ************** 更新项目节点 end************** */

    /*     * ************** 编辑项目节点 start ************** */

    public function actionproject_schedule_info($project_schedule_id = 0) {
        if (isset(Yii::app()->session['name'])) {
            $leftContent = $this->getLeftContent();
            $recommend = $this->getrecommend();

            if (isset($_POST['schedule_name'])) {
                if ($_POST['project_schedule_id'] == 0) {
                    $project_schedule_model = new project_schedule();
                } else {
                    $project_schedule_modelx = project_schedule::model();
                    $project_schedule_model = $project_schedule_modelx->findByPk($_POST['project_schedule_id']);
                }
                $schedule_name = $_POST['schedule_name'];
                $project_schedule_model->schedule_name = $schedule_name;
                $project_schedule_model->schedule_descript = $_POST['schedule_descript'];
                $project_schedule_model->_master_id = $_POST['_master_id'];
                $project_schedule_model->status = $_POST['status'];
//                $project_schedule_model->completion_degree = $_POST['completion_degree'];
//                $project_schedule_model->start_time = $_POST['start_time'];
//                if ($_POST['end_time'] == "" || $_POST['end_time'] == null) {
//                    $project_schedule_model->end_time = NULL;
//                } else {
//                    $project_schedule_model->end_time = $_POST['end_time'];
//                }
                $project_schedule_model->deadline = $_POST['deadline'];

                if ($project_schedule_model->save()) {
                    $this->redirect('./index.php?r=backend/project/project_schedule&project_id=' . $project_schedule_model->_project_id);
                }
            } else {
                $master_model = master::model();
                $master_info = $master_model->findAll(array('select' => array('master_id', 'master_name')));

                $project_schedule_info = project_schedule::model()->findByPk(array($project_schedule_id, "condition" => "isdeleted = 0"));
                $this->renderPartial('project_schedule_info', array('project_schedule_info' => $project_schedule_info, 'master_info' => $master_info, 'leftContent' => $leftContent, 'recommend' => $recommend));
            }
        } else {
            $this->redirect('./index.php?r=backend/default/index');
        }
    }

    /*     * ************** 编辑项目节点 end************** */

    /*     * ************** 删除项目节点 start************** */

    public function actiondeleteproject_schedule() {
        $project_schedule_id = $_POST["project_schedule_id"];
        $project_schedule_model = project_schedule::model();

        $project_schedule_info = $project_schedule_model->findByPk($project_schedule_id);
        if (count($project_schedule_info) != 0) {
            $project_schedule_info->isdeleted = 1;
        }

        if ($project_schedule_info->save()) {
            $json = '{"data":"success"}';
            echo $json;
        } else {
            $json = '{"data":"false"}';
            echo $json;
        }
    }

    /*     * ************** 删除项目节点 end************** */

    /*     * ************** 项目节点图片 start ************** */

    public function actionproject_schedule_picture($project_schedule_id = 0, $project_id = 0) {
        if (isset(Yii::app()->session['name'])) {
            $leftContent = $this->getLeftContent();
            $recommend = $this->getrecommend();

            //权限判断
            $name = Yii::app()->session['name'];
            $administrator_model = administrator::model();
            $role_menu_item_model = role_menu_item::model();
            $menu_item_model = menu_item::model();
            $_administrator_role_id = $administrator_model->find("name='$name'")->_administrator_role_id;
            $role_menu_item_info = $role_menu_item_model->findAll("_administrator_role_id='$_administrator_role_id'");
            $delete_picture = "hidden";
            $add_picture = "hidden";
            $edit_picture = "hidden";
            foreach ($role_menu_item_info as $value) {
                $_menu_item_id = $value->_menu_item_id;
                $menu_item_info = $menu_item_model->find("menu_item_id ='$_menu_item_id'");
                $menu_item_name = $menu_item_info->menu_item_name;
                if ($menu_item_name == "删除节点图片") {
                    $delete_picture = "";
                }
                if ($menu_item_name == "新增节点图片") {
                    $add_picture = "";
                }
                if ($menu_item_name == "编辑节点图片") {
                    $edit_picture = "";
                }
            }

            $project_schedule_picture_model = project_schedule_picture::model();
            $project_schedule_picture_info = $project_schedule_picture_model->findAll(array('condition' => "_project_schedule_id = $project_schedule_id AND isdeleted = 0"));

            $this->renderPartial('project_schedule_picture', array('project_schedule_picture_info' => $project_schedule_picture_info,
                'add_picture' => $add_picture, 'edit_picture' => $edit_picture, 'delete_picture' => $delete_picture, 'project_schedule_id' => $project_schedule_id,
                'project_id' => $project_id, 'leftContent' => $leftContent, 'recommend' => $recommend));
        } else {
            $this->redirect('./index.php?r=backend/default/index');
        }
    }

    /*     * ************** 项目节点图片 end************** */
    /*     * ************** 新增项目节点图片 start ************** */

    public function actionadd_project_schedule_picture($project_schedule_id = 0, $project_id = 0) {
        if (isset(Yii::app()->session['name'])) {
            $leftContent = $this->getLeftContent();
            $recommend = $this->getrecommend();

            $project_schedule_picture_model = project_schedule_picture::model();
            $project_schedule_picture_info = $project_schedule_picture_model->findAll(array('condition' => "_project_schedule_id = $project_schedule_id AND isdeleted = 0"));

            $this->renderPartial('add_project_schedule_picture', array('project_schedule_id' => $project_schedule_id,
                'file_count' => 9 - count($project_schedule_picture_info),
                'leftContent' => $leftContent, 'recommend' => $recommend,
                'project_id' => $project_id));
        } else {
            $this->redirect('./index.php?r=backend/default/index');
        }
    }

    #保存项目节点图片

    public function actionsave_project_schedule_picture() {
        $project_schedule_id = $_POST['project_schedule_id'];
        $project_schedule_picture_model = new project_schedule_picture();

        $project_schedule_picture_model->picture_name = "暂无";
        $project_schedule_picture_model->picture_address = "暂无";
        $project_schedule_picture_model->picture_descript = "暂无";
        $project_schedule_picture_model->_project_schedule_id = $project_schedule_id;
        $project_schedule_picture_model->create_time = date('Y-m-d H:i:s', time());
        $project_schedule_picture_model->creator_id = Yii::app()->session['administrator_id'];

        if ($project_schedule_picture_model->save()) {
            $picture_id = $project_schedule_picture_model->attributes['picture_id'];
            $project_schedule_picture_info = $project_schedule_picture_model->find("picture_id = $picture_id");
            $picture_after = substr(strrchr($_FILES['Filedata']['name'], '.'), 1);  //文件的后缀名 png

            $fileName = $picture_id . '.' . $picture_after;

            $uploaddir = './project_schedule_picture/';

            $tempFile = $_FILES['Filedata']['tmp_name'];

            $targetFile = rtrim($uploaddir, '/') . '/' . $fileName;
            move_uploaded_file($tempFile, iconv("UTF-8", "gb2312", $targetFile));

            #生成水印图片
            $info = getimagesize($targetFile);
            $type = image_type_to_extension($info[2], false);
            $fun = "imagecreatefrom{$type}"; // $fun=imagecreatefromjpeg; $fun=imagecreatefrompng
            $image = $fun($targetFile);
            $font = './project_schedule_picture_water/font.ttf';
            $name = "优自在装修";
            $website = "manage.yzz321.com";
            $color = imagecolorallocatealpha($image, 255, 255, 255, 0);
            imagettftext($image, 15, 0, 70, 50, $color, $font, $name);
            imagettftext($image, 10, 0, 65, 65, $color, $font, $website);
            header('content-type:' . $info['mime']);
            $func = "image{$type}"; //imagejpeg imagepng imagegif
            $func($image);
            $targetFile_water = './project_schedule_picture_water/' . $picture_id . '-water.' . $picture_after;
            $func($image, $targetFile_water);
            imagedestroy($image);

            $project_schedule_picture_info->picture_address = $targetFile;
            $project_schedule_picture_info->picture_water_address = $targetFile_water;
            $project_schedule_picture_info->save();
        }
    }

    /*     * ************** 新增项目节点图片 end************** */

    /*     * ************** 删除项目节点图片 start************** */

    public function actiondel_project_schedule_picture() {
        $picture_id = $_POST["picture_id"];
        $project_schedule_picture_model = project_schedule_picture::model();

        $project_schedule_picture_info = $project_schedule_picture_model->findByPk($picture_id);
        if (count($project_schedule_picture_info) != 0) {
            $project_schedule_picture_info->isdeleted = 1;
        }
        if ($project_schedule_picture_info->save()) {
            $json = '{"data":"success"}';
            echo $json;
        } else {
            $json = '{"data":"false"}';
            echo $json;
        }
    }

    /*     * ************** 删除项目节点图片 end************** */

    /*     * ************** 编辑项目节点图片 start************** */

    public function actionedit_project_schedule_picture($project_id = 0, $picture_id = 0) {
        if (isset(Yii::app()->session['name'])) {
            if (isset($_POST['picture_name'])) {

                $picture_id = $_POST['picture_id'];
                $project_id = $_POST['project_id'];
                $project_schedule_picture_model = project_schedule_picture::model();

                $project_schedule_picture_info = $project_schedule_picture_model->findByPk($picture_id);


                $picture_name = $_POST['picture_name'];
                $picture_descript = $_POST['picture_descript'];
                $project_schedule_picture_info->picture_name = $picture_name;
                $project_schedule_picture_info->picture_descript = $picture_descript;

                if ($project_schedule_picture_info->save()) {
                    $this->redirect('./index.php?r=backend/project/project_schedule_picture&project_schedule_id='
                            . $project_schedule_picture_info->_project_schedule_id . '&project_id=' . $project_id);
                }
            } else {
                $leftContent = $this->getLeftContent();
                $recommend = $this->getrecommend();

                $project_schedule_picture_model = project_schedule_picture::model();

                $project_schedule_picture_info = $project_schedule_picture_model->findByPk(array($picture_id, "condition" => "isdeleted = 0"));

                $this->renderPartial('edit_project_schedule_picture', array('project_schedule_picture_info' => $project_schedule_picture_info,
                    'leftContent' => $leftContent, 'recommend' => $recommend, "project_id" => $project_id));
            }
        } else {
            $this->redirect('./index.php?r=backend/default/index');
        }
    }

    /*     * ************** 编辑项目节点图片 end************** */
}
