<?php

include 'BaseController.php';

class PersonController extends baseController {

    public function getrecommend() {//获取CSS JS引用
        $common = new commonController();
        $url = $common->webUrl . "?r=backend/recommend/recommend";
        $structContent = file_get_contents($url);
        return $structContent;
    }

    /*
      配置
     */

    public function filters() {
        return array(
            'master + master',
            'designer + designer',
//            'editmaster + editmaster',
//            'position + position',
        );
    }

    public function filtermaster($filterChain) {
        $this->checkAccess("师傅管理", $filterChain);
    }

    public function filterdesigner($filterChain) {
        $this->checkAccess("设计师管理", $filterChain);
    }

    /*     * ************** 师傅管理 start ************** */

    public function actionmaster() {
        if (isset(Yii::app()->session['name'])) {
            $leftContent = $this->getLeftContent();
            $recommend = $this->getrecommend();

            //权限判断
            $name = Yii::app()->session['name'];
            $administrator_model = administrator::model();
            $role_menu_item_model = role_menu_item::model();
            $menu_item_model = menu_item::model();
            $_administrator_role_id = $administrator_model->find("name='$name'")->_administrator_role_id;
            $role_menu_item_info = $role_menu_item_model->findAll("_administrator_role_id='$_administrator_role_id'");
            $add_master = "hidden";
            $edit_master = "hidden";
            $del_master = "hidden";
            foreach ($role_menu_item_info as $value) {
                $_menu_item_id = $value->_menu_item_id;
                $menu_item_info = $menu_item_model->find("menu_item_id ='$_menu_item_id'");
                $menu_item_name = $menu_item_info->menu_item_name;
                if ($menu_item_name == "新增师傅信息") {
                    $add_master = "";
                }
                if ($menu_item_name == "编辑师傅信息") {
                    $edit_master = "";
                }
                if ($menu_item_name == "删除师傅信息") {
                    $del_master = "";
                }
            }
            $master_model = master::model();
            $master_info = $master_model->findAll("isdeleted = 0");

            $this->renderPartial('master', array('leftContent' => $leftContent, 'recommend' => $recommend,
                'master_info' => $master_info, 'add_master' => $add_master, 'edit_master' => $edit_master, 'del_master' => $del_master));
        } else {
            $this->redirect('./index.php?r=backend/default/index');
        }
    }

    /*     * ************** 师傅管理 end ************** */

    /*     * ************** 删除师傅 start************** */

    public function actiondeletemaster() {
        $master_id = $_POST["master_id"];
        $master_model = master::model();

        $master_info = $master_model->findByPk($master_id);
        if (count($master_info) != 0) {
            $master_info->isdeleted = 1;
        }


        if ($master_info->save()) {
            $json = '{"data":"success"}';
            echo $json;
        } else {
            $json = '{"data":"false"}';
            echo $json;
        }
    }

    /*     * ************** 删除师傅 end************** */

    /*     * ************** 添加|编辑师傅 start ************** */

    public function actionmaster_info($master_id = 0) {
        if (isset(Yii::app()->session['name'])) {
            $leftContent = $this->getLeftContent();
            $recommend = $this->getrecommend();

            if (isset($_POST['master_name'])) {
                if ($_POST['master_id'] == 0) {
                    $master_model = new master();
                } else {
                    $master_modelx = master::model();
                    $master_model = $master_modelx->findByPk($_POST['master_id']);
                }
                $master_name = $_POST['master_name'];
                $master_model->master_name = $master_name;
                $master_model->cellphone = $_POST['cellphone'];
                $master_model->master_description = $_POST['master_description'];
                $master_model->_position_id = $_POST['_position_id'];
                $master_model->level = $_POST['level'];
                $master_model->entry_time = $_POST['entry_time'];
                $master_model->create_time = date('Y-m-d H:i:s', time());
                $master_model->create_id = Yii::app()->session['administrator_id'];
                if ($master_model->save()) {
                    $master_id = $master_model->attributes['master_id'];
                    $master_info = $master_model->find("master_id = $master_id");
                    if ($_FILES["head_add"]["size"] > 0) {
                        $picture_after = substr(strrchr($_FILES['head_add']['name'], '.'), 1);  //文件的后缀名 png
                        $fileName = $master_id . '.' . $picture_after;
                        $uploaddir = './head_pic/';
                        $tempFile = $_FILES['head_add']['tmp_name'];
                        $targetFile = rtrim($uploaddir, '/') . '/' . $fileName;
                        move_uploaded_file($tempFile, iconv("UTF-8", "gb2312", $targetFile));
                        $master_info->head_add = $targetFile;
                        $master_info->save();
                    } else if ($master_info->head_add == "") {
                        $master_info->head_add = "./head_pic/1.png";
                        $master_info->save();
                    }

                    $this->redirect('./index.php?r=backend/person/master');
                }
            } else {
                $position_model = position::model();
                $position_info = $position_model->findAll();
                if ($master_id == 0) {//新增
                    $this->renderPartial('master_info', array('leftContent' => $leftContent, 'recommend' => $recommend, 'position_info' => $position_info));
                } else {//编辑
                    $master_model = master::model();
                    $master_info = $master_model->findByPk($master_id);
                    $this->renderPartial('master_info', array('master_info' => $master_info, 'leftContent' => $leftContent, 'recommend' => $recommend, 'position_info' => $position_info));
                }
            }
        } else {
            $this->redirect('./index.php?r=backend/default/index');
        }
    }

    /*     * ************** 添加|编辑师傅 end ************** */
    
    /*     * ************** 设计师管理 start ************** */

    public function actiondesigner() {
        if (isset(Yii::app()->session['name'])) {
            $leftContent = $this->getLeftContent();
            $recommend = $this->getrecommend();

            //权限判断
            $name = Yii::app()->session['name'];
            $administrator_model = administrator::model();
            $role_menu_item_model = role_menu_item::model();
            $menu_item_model = menu_item::model();
            $_administrator_role_id = $administrator_model->find("name='$name'")->_administrator_role_id;
            $role_menu_item_info = $role_menu_item_model->findAll("_administrator_role_id='$_administrator_role_id'");
            $add_designer = "hidden";
            $edit_designer = "hidden";
            $del_designer = "hidden";
            foreach ($role_menu_item_info as $value) {
                $_menu_item_id = $value->_menu_item_id;
                $menu_item_info = $menu_item_model->find("menu_item_id ='$_menu_item_id'");
                $menu_item_name = $menu_item_info->menu_item_name;
                if ($menu_item_name == "新增设计师信息") {
                    $add_designer = "";
                }
                if ($menu_item_name == "编辑设计师信息") {
                    $edit_designer = "";
                }
                if ($menu_item_name == "删除设计师信息") {
                    $del_designer = "";
                }
            }
            $designer_model = designer::model();
            $designer_info = $designer_model->findAll("isdeleted = 0");

            $this->renderPartial('designer', array('leftContent' => $leftContent, 'recommend' => $recommend,
                'designer_info' => $designer_info, 'add_designer' => $add_designer, 'edit_designer' => $edit_designer, 'del_designer' => $del_designer));
        } else {
            $this->redirect('./index.php?r=backend/default/index');
        }
    }

    /*     * ************** 设计师管理 end ************** */

    /*     * ************** 删除设计师 start************** */

    public function actiondeletedesigner() {
        $designer_id = $_POST["designer_id"];
        $designer_model = designer::model();

        $designer_info = $designer_model->findByPk($designer_id);
        if (count($designer_info) != 0) {
            $designer_info->isdeleted = 1;
        }


        if ($designer_info->save()) {
            $json = '{"data":"success"}';
            echo $json;
        } else {
            $json = '{"data":"false"}';
            echo $json;
        }
    }

    /*     * ************** 删除设计师 end************** */

    /*     * ************** 添加|编辑设计师 start ************** */

    public function actiondesigner_info($designer_id = 0) {
        if (isset(Yii::app()->session['name'])) {
            $leftContent = $this->getLeftContent();
            $recommend = $this->getrecommend();

            if (isset($_POST['designer_name'])) {
                if ($_POST['designer_id'] == 0) {
                    $designer_model = new designer();
                } else {
                    $designer_modelx = designer::model();
                    $designer_model = $designer_modelx->findByPk($_POST['designer_id']);
                }
                $designer_name = $_POST['designer_name'];
                $designer_model->designer_name = $designer_name;
                $designer_model->cellphone = $_POST['cellphone'];
                $designer_model->designer_description = $_POST['designer_description'];
                $designer_model->_position_id = $_POST['_position_id'];
                $designer_model->level = $_POST['level'];
                $designer_model->entry_time = $_POST['entry_time'];
                $designer_model->create_time = date('Y-m-d H:i:s', time());
                $designer_model->create_id = Yii::app()->session['administrator_id'];
                if ($designer_model->save()) {
                    $designer_id = $designer_model->attributes['designer_id'];
                    $designer_info = $designer_model->find("designer_id = $designer_id");
                    if ($_FILES["head_add"]["size"] > 0) {
                        $picture_after = substr(strrchr($_FILES['head_add']['name'], '.'), 1);  //文件的后缀名 png
                        $fileName = $designer_id . '.' . $picture_after;
                        $uploaddir = './designer_head_pic/';
                        $tempFile = $_FILES['head_add']['tmp_name'];
                        $targetFile = rtrim($uploaddir, '/') . '/' . $fileName;
                        move_uploaded_file($tempFile, iconv("UTF-8", "gb2312", $targetFile));
                        $designer_info->head_add = $targetFile;
                        $designer_info->save();
                    } else if ($designer_info->head_add == "") {
                        $designer_info->head_add = "./designer_head_pic/1.png";
                        $designer_info->save();
                    }

                    $this->redirect('./index.php?r=backend/person/designer');
                }
            } else {
                $position_model = position::model();
                $position_info = $position_model->findAll();
                if ($designer_id == 0) {//新增
                    $this->renderPartial('designer_info', array('leftContent' => $leftContent, 'recommend' => $recommend, 'position_info' => $position_info));
                } else {//编辑
                    $designer_model = designer::model();
                    $designer_info = $designer_model->findByPk($designer_id);
                    $this->renderPartial('designer_info', array('designer_info' => $designer_info, 'leftContent' => $leftContent, 'recommend' => $recommend, 'position_info' => $position_info));
                }
            }
        } else {
            $this->redirect('./index.php?r=backend/default/index');
        }
    }

    /*     * ************** 添加|编辑设计师 end ************** */
}
