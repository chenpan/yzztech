<?php

class recommendController extends Controller {

    public function actionleftmenu($username, $menu, $logintime, $menu_name) {//左侧导航
        $leftmenu = json_decode($menu, TRUE);
        $this->renderPartial('leftmenu', array("username" => $username, "leftmenu" => $leftmenu, "logintime" => $logintime, "menu_name" => $menu_name));
    }

    public function actionrecommend() {//CSS JS 引用
        $this->renderPartial('recommend');
    }

}
