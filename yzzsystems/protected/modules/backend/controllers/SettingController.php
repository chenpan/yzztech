<?php

include 'BaseController.php';

class SettingController extends baseController {

    public function getrecommend() {//获取CSS JS引用
        $common = new commonController();
        $url = $common->webUrl . "?r=backend/recommend/recommend";
        $structContent = file_get_contents($url);
        return $structContent;
    }

    /*
      配置
     */

    public function filters() {
        return array(
            'setting + setting',
            'editsetting + editsetting',
            'position + position',
            'deleteposition + deleteposition',
            'position_info + position_info'
        );
    }

    public function filtersetting($filterChain) {
        $this->checkAccess("基础信息", $filterChain);
    }

    public function filtereditsetting($filterChain) {
        $this->checkAccess("编辑基础信息", $filterChain);
    }

    public function filterposition($filterChain) {
        $this->checkAccess("职位管理", $filterChain);
    }

    public function filterdeleteposition($filterChain) {
        $this->checkAccess("删除职位信息", $filterChain);
    }

    public function filterposition_info($filterChain) {
        $this->checkAccess("编辑职位信息", $filterChain);
    }

    /*     * ************** 基础信息 start ************** */

    public function actionsetting() {
        if (isset(Yii::app()->session['name'])) {
            $leftContent = $this->getLeftContent();
            $recommend = $this->getrecommend();

            //权限判断
            $name = Yii::app()->session['name'];
            $administrator_model = administrator::model();
            $role_menu_item_model = role_menu_item::model();
            $menu_item_model = menu_item::model();
            $_administrator_role_id = $administrator_model->find("name='$name'")->_administrator_role_id;
            $role_menu_item_info = $role_menu_item_model->findAll("_administrator_role_id='$_administrator_role_id'");
            $edit_setting = "hidden";
            foreach ($role_menu_item_info as $value) {
                $_menu_item_id = $value->_menu_item_id;
                $menu_item_info = $menu_item_model->find("menu_item_id ='$_menu_item_id'");
                $menu_item_name = $menu_item_info->menu_item_name;
                if ($menu_item_name == "编辑基础信息") {
                    $edit_setting = "";
                }
            }
            $base_config_model = base_config::model();
            $base_config_info = $base_config_model->findAll();

            $this->renderPartial('setting', array('leftContent' => $leftContent, 'recommend' => $recommend,
                'base_config_info' => $base_config_info, 'edit_setting' => $edit_setting));
        } else {
            $this->redirect('./index.php?r=backend/default/index');
        }
    }

    /*     * ************** 基础信息 end ************** */


    /*     * ************** 项目基础信息 start ************** */

    public function actioneditsetting($base_config_id = 0) {
        if (isset(Yii::app()->session['name'])) {
            $leftContent = $this->getLeftContent();
            $recommend = $this->getrecommend();

            if (isset($_POST['website_name'])) {
                if ($_POST['base_config_id'] == 0) {
                    $base_config_model = new base_config();
                } else {
                    $base_config_modelx = base_config::model();
                    $base_config_model = $base_config_modelx->findByPk($_POST['base_config_id']);
                }
                $website_name = $_POST['website_name'];
                $menu_name = $_POST['menu_name'];
                $base_config_model->website_name = $website_name;
                $base_config_model->menu_name = $menu_name;
                if ($base_config_model->save()) {
                    $this->redirect('./index.php?r=backend/setting/setting');
                }
            } else {
                $base_config_info = base_config::model()->findByPk($base_config_id);

                $this->renderPartial('editsetting', array('base_config_info' => $base_config_info, 'leftContent' => $leftContent, 'recommend' => $recommend));
            }
        } else {
            $this->redirect('./index.php?r=backend/default/index');
        }
    }

    /*     * ************** 项目基础信息 end ************** */
    /*     * ************** 职位管理 start ************** */

    public function actionposition() {
        if (isset(Yii::app()->session['name'])) {
            $leftContent = $this->getLeftContent();
            $recommend = $this->getrecommend();

            //权限判断
            $name = Yii::app()->session['name'];
            $administrator_model = administrator::model();
            $role_menu_item_model = role_menu_item::model();
            $menu_item_model = menu_item::model();
            $_administrator_role_id = $administrator_model->find("name='$name'")->_administrator_role_id;
            $role_menu_item_info = $role_menu_item_model->findAll("_administrator_role_id='$_administrator_role_id'");
            $add_position = "hidden";
            $edit_position = "hidden";
            $del_position = "hidden";
            foreach ($role_menu_item_info as $value) {
                $_menu_item_id = $value->_menu_item_id;
                $menu_item_info = $menu_item_model->find("menu_item_id ='$_menu_item_id'");
                $menu_item_name = $menu_item_info->menu_item_name;
                if ($menu_item_name == "新增职位信息") {
                    $add_position = "";
                }
                if ($menu_item_name == "编辑职位信息") {
                    $edit_position = "";
                }
                if ($menu_item_name == "删除职位信息") {
                    $del_position = "";
                }
            }
            $position_model = position::model();
            $position_info = $position_model->findAll();

            $this->renderPartial('position', array('leftContent' => $leftContent, 'recommend' => $recommend,
                'position_info' => $position_info, 'add_position' => $add_position, 'edit_position' => $edit_position,
                'del_position' => $del_position));
        } else {
            $this->redirect('./index.php?r=backend/default/index');
        }
    }

    /*     * ************** 职位管理 end ************** */
    /*     * ************** 删除职位 start************** */

    public function actiondeleteposition() {
        $position_id = $_POST["position_id"];

        $count = position::model()->deleteAll(array('condition' => "position_id =" . $position_id));

        if ($count > 0) {
            $json = '{"data":"success"}';
            echo $json;
        } else {
            $json = '{"data":"false"}';
            echo $json;
        }
    }

    /*     * ************** 删除职位 end************** */

    /*     * ************** 添加|编辑职位 start ************** */

    public function actionposition_info($position_id = 0) {
        if (isset(Yii::app()->session['name'])) {
            $leftContent = $this->getLeftContent();
            $recommend = $this->getrecommend();

            if (isset($_POST['position_name'])) {
                if ($_POST['position_id'] == 0) {
                    $position_model = new position();
                } else {
                    $position_modelx = position::model();
                    $position_model = $position_modelx->findByPk($_POST['position_id']);
                }
                $position_name = $_POST['position_name'];
                $position_model->position_name = $position_name;
                if ($position_model->save()) {
                    $this->redirect('./index.php?r=backend/setting/position');
                }
            } else {
                if ($position_id == 0) {//新增
                    $this->renderPartial('position_info', array('leftContent' => $leftContent, 'recommend' => $recommend));
                } else {//编辑
                    $position_info = position::model()->findByPk($position_id);
                    $this->renderPartial('position_info', array('position_info' => $position_info, 'leftContent' => $leftContent, 'recommend' => $recommend));
                }
            }
        } else {
            $this->redirect('./index.php?r=backend/default/index');
        }
    }

    /*     * ************** 添加|编辑职位 end ************** */
}
