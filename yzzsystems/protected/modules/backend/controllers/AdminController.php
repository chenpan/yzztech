<?php

include 'BaseController.php';

class adminController extends baseController {

    public function getrecommend() {//获取CSS JS引用
        $common = new commonController();
        $url = $common->webUrl . "?r=backend/recommend/recommend";
        $structContent = file_get_contents($url);
        return $structContent;
    }

    public function actionIndex() {
        if (isset(Yii::app()->session['name'])) {
            $leftContent = $this->getLeftContent();
            $recommend = $this->getrecommend();
            $connection = Yii::app()->db;
            // 项目个数
            $total_project_sql = "SELECT COUNT(project_id) AS total_project FROM project WHERE isdeleted = 0";
            foreach ($connection->createCommand($total_project_sql)->query() as $k => $v) {
                $total_project = $v['total_project'];
            }
            //  师傅量
            $total_master_sql = "SELECT COUNT(master_id) AS total_master FROM master WHERE isdeleted = 0";
            foreach ($connection->createCommand($total_master_sql)->query() as $k => $v) {
                $total_master = $v['total_master'];
            }

            //  设计师量
            $total_designer_sql = "SELECT COUNT(designer_id) AS total_designer FROM designer WHERE isdeleted = 0";
            foreach ($connection->createCommand($total_designer_sql)->query() as $k => $v) {
                $total_designer = $v['total_designer'];
            }

            //  业主量
            $total_user_sql = "SELECT COUNT(user_id) AS total_user FROM user WHERE isdeleted = 0";
            foreach ($connection->createCommand($total_user_sql)->query() as $k => $v) {
                $total_user = $v['total_user'];
            }


            //最近10个项目
            $project_info = project::model()->findAll(array("condition" => "isdeleted = 0", "limit" => "4", "order" => "project_id DESC"));

            //最近10个师傅
            $master_info = master::model()->findAll(array("condition" => "isdeleted = 0", "limit" => "4", "order" => "master_id DESC"));

            //最近10个设计师
            $designer_info = designer::model()->findAll(array("condition" => "isdeleted = 0", "limit" => "4", "order" => "designer_id DESC"));

            //最近10个业主
            $user_info = user::model()->findAll(array("condition" => "isdeleted = 0", "limit" => "4", "order" => "user_id DESC"));

            $this->renderPartial('index', array('leftContent' => $leftContent, 'recommend' => $recommend, 'total_project' => $total_project
                , 'total_user' => $total_user, 'total_master' => $total_master, 'total_designer' => $total_designer,
                'project_info' => $project_info, 'master_info' => $master_info, 'designer_info' => $designer_info, 'user_info' => $user_info));
        } else {
            $this->redirect('./index.php?r=backend/default/index');
        }
    }

//*****************退款统计***************************//
    public function actionrefund_statistics() {
        $school_id = $_POST['school_id'];

        $connection = Yii::app()->db;

        $dayRefund = array();
        $totalrefund_dayss = 0; //退款统计
        for ($i = 6; $i >= 0; $i--) {
            $time = date('Y-m-d', strtotime('-' . $i . 'day'));
            $stime = $time . " 00:00:00";
            $etime = $time . " 23:59:59";
//*****************退款统计***************************//
            $totalrefund_days = 0;
            if (Yii::app()->session['is_agent'] == 0) {//不是代理商
                session_write_close();
                if ($i == 0) {
                    $refund_infox = "select a.refund_amount as total_refund_money,a.points,a.old_pay_method from refund a"
                            . " join orders b on a.order_id = b.order_id"
                            . " where a.refundtime between '$stime' and '$etime'"
                            . " and a.status = 1 and b.school_id in ($school_id)";
                    foreach ($connection->createCommand($refund_infox)->query() as $k => $v) {
                        if ($v['old_pay_method'] == 2) {//微信
                            $totalrefund_days += $v['total_refund_money'] / 100;
                        } else if ($v['old_pay_method'] == 3) {//积分
                            $totalrefund_days += $v['points'] / 100;
                        } else {
                            $totalrefund_days += $v['total_refund_money'];
                        }
                    }
                } else {
                    $refund_infox = "select sum(refunds) as total_refund from statistics_days where time between '$stime' and '$etime' and school_id in ($school_id)";

                    foreach ($connection->createCommand($refund_infox)->query() as $k => $v) {
                        $totalrefund_days += $v['total_refund'];
                    }
                }
            } else {
//终端订单退的钱
                $refund_info_pm = "select a.refund_amount as total_refund_money,a.points,a.old_pay_method from refund a"
                        . " join orders b on a.order_id = b.order_id"
                        . " where a.refundtime between '$stime' and '$etime'"
                        . " and a.status = 1 and b.school_id in ($school_id)"
                        . " and b.machine_id in (" . Yii::app()->session['printor_id'] . ")"
                        . " and b.order_type_code = 2";
                session_write_close();
//线上订单退的钱
                $refund_info_sm = "select a.refund_amount as total_refund_money,a.points,a.old_pay_method from refund a"
                        . " join orders b on a.order_id = b.order_id"
                        . " where a.refundtime between '$stime' and '$etime'"
                        . " and a.status = 1 and b.school_id in ($school_id)"
                        . " and b.machine_id in (" . Yii::app()->session['printor_id'] . ")"
                        . " and b.order_type_code != 2";
                session_write_close();
                foreach ($connection->createCommand($refund_info_pm)->query() as $k => $v) {
                    if ($v['old_pay_method'] == 2) {//微信
                        $totalrefund_days += $v['total_refund_money'] / 100;
                    } else if ($v['old_pay_method'] == 3) {//积分
                        $totalrefund_days += $v['points'] / 100;
                    } else {
                        $totalrefund_days += $v['total_refund_money'];
                    }
                }
                foreach ($connection->createCommand($refund_info_sm)->query() as $k => $v) {
                    if ($v['old_pay_method'] == 2) {//微信
                        $totalrefund_days += $v['total_refund_money'] / 100;
                    } else if ($v['old_pay_method'] == 3) {//积分
                        $totalrefund_days += $v['points'] / 100;
                    } else {
                        $totalrefund_days += $v['total_refund_money'];
                    }
                }
            }
            $totalrefund_dayss += sprintf("%.2f", $totalrefund_days);
            array_push($dayRefund, array('totalrefund_days' => sprintf("%.2f", $totalrefund_days), 'day' => $time = date('m-d', strtotime('-' . $i . 'day'))));
        }
        $dayArray_refund = array();
        $dayArray_refund["totalrefund_dayss"] = $totalrefund_dayss;
        $dayArray_refund["dayRefund"] = $dayRefund;
        echo json_encode($dayArray_refund);
    }

//****************注册人数***********************//
    public function actionregister_statistics() {
        $school_id = $_POST['school_id'];
        $connection = Yii::app()->db;
        $dayReg = array();
        $regCount_dayss = 0; //注册人数统计
        for ($i = 6; $i >= 0; $i--) {
            $time = date('Y-m-d', strtotime('-' . $i . 'day'));
            $stime = $time . " 00:00:00";
            $etime = $time . " 23:59:59";
            $regCount_days = 0;
            if ($i == 0) {
                $reg_infos = "select count(user_id) as total_reg from user where createtime between '$stime' and '$etime' and school_id in ($school_id)";
            } else {
                $reg_infos = "select sum(registers) as total_reg from statistics_days where time between '$stime' and '$etime' and school_id in ($school_id)";
            }
            foreach ($connection->createCommand($reg_infos)->query() as $k => $v) {
                $regCount_days = $v['total_reg'];
            }
            $regCount_dayss += $regCount_days;
            array_push($dayReg, array('regCount_days' => $regCount_days, 'day' => $time = date('m-d', strtotime('-' . $i . 'day'))));
        }
        $dayArray_register = array();
        $dayArray_register["regCount_dayss"] = $regCount_dayss;
        $dayArray_register["dayReg"] = $dayReg;
        echo json_encode($dayArray_register);
    }

//****************订单统计***********************//
    public function actionorders_statistics() {
        $school_id = $_POST['school_id'];

        $connection = Yii::app()->db;

        $dayOrder = array();
        $totalCount_dayss = 0; //订单统计
//统计当天的退款订单量和退款金额
        $refundrateCount_terminal = 0; //终端订单退款率
        $refundrateCount_web = 0; //web端订单退款率
        $refundrateCount_total = 0; //订单总退款
        $refundrateCount_terminal_total = 0; //终端订单退款率
        $refundrateCount_web_total = 0; //web端订单退款率
        $refundrateCount_total_total = 0; //订单总退款率
        for ($i = 6; $i >= 0; $i--) {
            $time = date('Y-m-d', strtotime('-' . $i . 'day'));
            $stime = $time . " 00:00:00";
            $etime = $time . " 23:59:59";
            $totalCount_days_terminal = 1; //终端订单量
            $totalCount_days_web = 1; //web端订单量
            $totalCount_refund_terminal = 1; //终端退款订单量
            $totalCount_refund_web = 1; //web端退款订单量
            if (Yii::app()->session['is_agent'] == 0) {//不是代理商
                session_write_close();
                if ($i == 0) {
//web端打印订单量
                    $business_infos = "SELECT COUNT(order_id) AS total_business FROM orders"
                            . " WHERE order_status_code != 1 AND order_type_code in (1,2,6)"
                            . " AND front_channel_code IN (1,2,3,5)"
                            . " AND createtime BETWEEN '$stime' and '$etime'"
                            . " AND school_id in ($school_id)";

//终端打印订单量
                    $prbusiness_info = "SELECT COUNT(order_id) AS total_prbusiness FROM orders"
                            . " WHERE createtime BETWEEN '$stime' and '$etime'"
                            . " AND school_id in ($school_id) AND order_status_code != 1"
                            . " AND order_type_code = 2";
//WEB端订单退款量
                    $refund_infoq = "SELECT COUNT(DISTINCT(a.order_id)) AS total_refund_order FROM refund a"
                            . " JOIN orders b ON a.order_id = b.order_id"
                            . " WHERE a.status = 1  AND b.order_type_code in (1,2,6)"
                            . " AND b.front_channel_code IN (1,2,3,5)"
                            . " AND a.refundtime BETWEEN '$stime' and '$etime'"
                            . " AND b.school_id in ($school_id)";

//终端退款订单量
                    $refund_infox = "SELECT COUNT(DISTINCT(a.order_id)) AS total_refund_order FROM refund a"
                            . " JOIN orders b ON a.order_id = b.order_id"
                            . " WHERE a.status = 1 AND b.order_type_code = 2"
                            . " AND a.refundtime BETWEEN '$stime' and '$etime'"
                            . " AND b.school_id in ($school_id)";
//                    $business_infos = "select count(order_id) as total_business from orders where createtime between '$stime' and '$etime' and school_id in ($school_id) and order_status_code != 1 and order_type_code != 2";
//                    $prbusiness_info = "select count(order_id) as total_prbusiness from orders where createtime between '$stime' and '$etime' and school_id in ($school_id) and order_status_code != 1 and order_type_code = 2";
//                    $refund_infoq = "select count(a.order_id) as total_refund_order from refund a"
//                            . " join orders b on a.order_id = b.order_id"
//                            . " where a.refundtime between '$stime' and '$etime'"
//                            . " and a.status = 1 and b.school_id in ($school_id)"
//                            . " and b.order_type_code != 2";
//                    $refund_infox = "select count(a.order_id) as total_refund_order from refund a"
//                            . " join orders b on a.order_id = b.order_id"
//                            . " where a.refundtime between '$stime' and '$etime'"
//                            . " and a.status = 1 and b.school_id in ($school_id)"
//                            . " and b.order_type_code = 2";
                } else {
// web订单量
                    $business_infos = "select sum(orders_pc_print + orders_app_print + orders_wx_print) as total_business from statistics_days where time between '$stime' and '$etime' and school_id in ($school_id)";
//终端订单量
                    $prbusiness_info = "select sum(orders_terminal) as total_prbusiness from statistics_days where time between '$stime' and '$etime' and school_id in ($school_id)";

//web订单退款量
                    $refund_infoq = "select sum(orders_refund_pc_print + orders_refund_app_print + orders_refund_wx_print) as total_refund_order from statistics_days where time between '$stime' and '$etime' and school_id in ($school_id)";
//终端退款量
                    $refund_infox = "select sum(orders_refund_terminal) as total_refund_order from statistics_days where time between '$stime' and '$etime' and school_id in ($school_id)";
                }
            } else {
//web端打印订单量
                $business_infos = "SELECT COUNT(order_id) AS total_business FROM orders"
                        . " WHERE order_status_code != 1 AND order_type_code in (1,2,6)"
                        . " AND front_channel_code IN (1,2,3,5)"
                        . " AND createtime BETWEEN '$stime' and '$etime'"
                        . " AND school_id in ($school_id)"
                        . " AND machine_id in (" . Yii::app()->session['printor_id'] . ")";
                session_write_close();
//终端打印订单量
                $prbusiness_info = "SELECT COUNT(order_id) AS total_prbusiness FROM orders"
                        . " WHERE createtime BETWEEN '$stime' and '$etime'"
                        . " AND school_id in ($school_id) AND order_status_code != 1"
                        . " AND order_type_code = 2"
                        . " AND machine_id in (" . Yii::app()->session['printor_id'] . ")";
                session_write_close();
//WEB端订单退款量
                $refund_infoq = "SELECT COUNT(DISTINCT(a.order_id)) AS total_refund_order FROM refund a"
                        . " JOIN orders b ON a.order_id = b.order_id"
                        . " WHERE a.status = 1  AND b.order_type_code in (1,2,6)"
                        . " AND b.front_channel_code IN (1,2,3,5)"
                        . " AND a.refundtime BETWEEN '$stime' and '$etime'"
                        . " AND b.school_id in ($school_id)"
                        . " AND b.machine_id in (" . Yii::app()->session['printor_id'] . ")";
                session_write_close();
//终端退款订单量
                $refund_infox = "SELECT COUNT(DISTINCT(a.order_id)) AS total_refund_order FROM refund a"
                        . " JOIN orders b ON a.order_id = b.order_id"
                        . " WHERE a.status = 1 AND b.order_type_code = 2"
                        . " AND a.refundtime BETWEEN '$stime' and '$etime'"
                        . " AND b.school_id in ($school_id)"
                        . " AND b.machine_id in (" . Yii::app()->session['printor_id'] . ")";
                session_write_close();
//                $business_infos = "select count(order_id) as total_business from orders"
//                        . " where createtime between '$stime' and '$etime'"
//                        . " and school_id in ($school_id) and order_status_code != 1"
//                        . " and order_type_code != 2"
//                        . " and machine_id in (" . Yii::app()->session['printor_id'] . ")";
//                $prbusiness_info = "select count(order_id) as total_prbusiness from orders"
//                        . " where createtime between '$stime' and '$etime'"
//                        . " and school_id in ($school_id) and order_status_code != 1"
//                        . " and order_type_code = 2"
//                        . " and machine_id in (" . Yii::app()->session['printor_id'] . ")";
//                //web订单退款量
//                $refund_infoq = "select count(a.order_id) as total_refund_order from refund a"
//                        . " join orders b on a.order_id = b.order_id"
//                        . " where a.refundtime between '$stime' and '$etime'"
//                        . " and a.status = 1 and b.school_id in ($school_id)"
//                        . " and b.order_type_code != 2"
//                        . " and b.machine_id in (" . Yii::app()->session['printor_id'] . ")";
//                //终端退款量
//                $refund_infox = "select count(a.order_id) as total_refund_order from refund a"
//                        . " join orders b on a.order_id = b.order_id"
//                        . " where a.refundtime between '$stime' and '$etime'"
//                        . " and a.status = 1 and b.school_id in ($school_id)"
//                        . " and b.order_type_code = 2"
//                        . " and b.machine_id in (" . Yii::app()->session['printor_id'] . ")";
            }
            foreach ($connection->createCommand($business_infos)->query() as $k => $v) {
                $totalCount_days_web = $v['total_business'];
            }
            foreach ($connection->createCommand($prbusiness_info)->query() as $k => $v) {
                $totalCount_days_terminal = $v['total_prbusiness'];
            }
            foreach ($connection->createCommand($refund_infoq)->query() as $k => $v) {
                $totalCount_refund_web = $v['total_refund_order'];
            }
            foreach ($connection->createCommand($refund_infox)->query() as $k => $v) {
                $totalCount_refund_terminal = $v['total_refund_order'];
            }
            $totalCount_dayss += $totalCount_days_web + $totalCount_days_terminal;
            if ($i == 1) {
//终端订单退款率
                $refundrateCount_terminal = $totalCount_refund_terminal;
                $refundrateCount_terminal_total = $totalCount_days_terminal;
//WEB端订单退款率
                $refundrateCount_web = $totalCount_refund_web;
                $refundrateCount_web_total = $totalCount_days_web;
//总的退款率
                $refundrateCount_total = $totalCount_refund_terminal + $totalCount_refund_web;
                $refundrateCount_total_total = $totalCount_days_web + $totalCount_days_terminal;
            }
            array_push($dayOrder, array('totalCount_days_web' => $totalCount_days_web, 'totalCount_days_terminal' => $totalCount_days_terminal, 'day' => $time = date('m-d', strtotime('-' . $i . 'day')), 'totalCount_refund_terminal' => $totalCount_refund_terminal, 'totalCount_refund_web' => $totalCount_refund_web));
        }
        $dayArray_orders = array();
        $dayArray_orders["dayOrder"] = $dayOrder;
        $dayArray_orders["totalCount_dayss"] = $totalCount_dayss;
        $dayArray_orders["refundrateCount_terminal"] = $refundrateCount_terminal;
        $dayArray_orders["refundrateCount_terminal_total"] = $refundrateCount_terminal_total;
        $dayArray_orders["refundrateCount_web"] = $refundrateCount_web;
        $dayArray_orders["refundrateCount_web_total"] = $refundrateCount_web_total;
        $dayArray_orders["refundrateCount_total"] = $refundrateCount_total;
        $dayArray_orders["refundrateCount_total_total"] = $refundrateCount_total_total;
        echo json_encode($dayArray_orders);
//        $json = '{"dayOrder":' . $dayOrder . ',"totalCount_dayss":' . $totalCount_dayss . ',"refundrateCount_terminal":' . $refundrateCount_terminal
//                . ',"refundrateCount_terminal_total":' . $refundrateCount_terminal_total . ',"refundrateCount_web":' . $refundrateCount_web
//                . ',"refundrateCount_web_total":' . $refundrateCount_web_total . ',"refundrateCount_total":' . $refundrateCount_total
//                . ',"refundrateCount_total_total":' . $refundrateCount_total_total . '}';
//        echo $json;
    }

//注销
    public function actionLogout() {
        unset(Yii::app()->session['adminuser']);
        if (!isset(Yii::app()->session['adminuser'])) {
            $this->redirect('./index.php?r=backend/default/index');
        }
    }

    public function actionModify() {
        $leftContent = $this->getLeftContent();
        $recommend = $this->getrecommend();
        $name = Yii::app()->session['name']; //用户名
        if (isset($name)) {
            $administrator_model = administrator::model();
            if (isset($_POST['password'])) {

                $adminInfo = $administrator_model->find("name = '$name'");
                $adminInfo->password = md5($_POST['password']);

                if ($adminInfo->save()) {
                    $json = ' {
                    "data":"success"
                }';
                    echo $json;
                } else {
                    $json = ' {
                    "data":"false"
                }';
                    echo $json;
                }
            } else {
                $this->renderPartial('modifyInfo', array('administrator_model' => $administrator_model, 'leftContent' => $leftContent, 'recommend' => $recommend));
            }
        }
    }

    public function actiontestiframe() {
        for ($i = 6; $i >= 0; $i--) {
            $stime = date('Y-m-d', strtotime('-' . $i . 'day'));
            echo $stime . "*";
        }
//        echo date('Y-m-d H:i:s',strtotime('-18 day'));
//        $recommend = $this->getrecommend();
//        $this->renderPartial('testiframe', array('recommend' => $recommend));
    }

}
