<?php

class defaultController extends Controller {
    /*     * *************登陆页面 start***************** */

    public function actionindex() {
        $this->renderPartial('index');
    }

    public function actiontestindex() {
        $this->renderPartial('testindex');
    }

    public function actionlogin() {
        $cellphone = addslashes($_POST["cellphone"]);
        $password = md5($_POST["password"]);

        $user_model = user::model();
        $user_info = $user_model->find(array('condition' => "cellphone='$cellphone' AND password='$password' and isdeleted = 0"));
        if (count($user_info) != 0) {
            $user_info->openid = Yii::app()->session['openid'];
            if ($user_info->save()) {
                $json = '{"data":"true"}';
            } else {
                $json = '{"data":"false","message":"登录失败"}';
            }


//            $_administrator_role_id = $administrator_info->_administrator_role_id;
//            $administrator_id = $administrator_info->administrator_id;
//            Yii::app()->session['administrator_id'] = $administrator_id;
//            Yii::app()->session['name'] = $administrator_info->name;
//            Yii::app()->session['_administrator_role_id'] = $_administrator_role_id;
//            Yii::app()->session['logintime'] = date('Y-m-d H:i:s');
//            unset(Yii::app()->session['phonecheckma']);
        } else if (count($user_info) == 0) {
            $json = '{"data":"false","message":"密码错误"}';
        }
        echo $json;
    }

    # 注册页

    public function actionregister() {
        $this->renderPartial('register');
    }

    #注册发送验证码

    public function actionsendverificationcode() {

        $cellphone = addslashes($_POST["cellphone"]);

        $user_info = user::model()->findAll("cellphone = '$cellphone'");
        if (count($user_info) == 0) {
            if (isset(Yii::app()->session['phonecheckma'])) {
                unset(Yii::app()->session['phonecheckma']);
            }
            $phonecheckma = mt_rand(100000, 999999);
            Yii::app()->session['phonecheckma'] = $phonecheckma;
            $user = 'yzzzx'; //短信接口用户名 $user
            $pwd = 'yzz112233'; //短信接口密码 $pwd
            $mobiles = $cellphone; //说明：取用户输入的手号
            $contents = "亲爱的用户，您此次注册的验证码为" . $phonecheckma . "，验证码很重要，打死都不要告诉任何人哦，谢谢！"; //说明：取用户输入的短信内容
            $chid = 0; //通道ID
            $sendMessage = "http://www.106008.com/WebAPI/SmsAPI.asmx/SendSmsExt?user={$user}&pwd={$pwd}&mobiles={$mobiles}&contents={$contents}&chid={$chid}&sendtime=";
            file_get_contents($sendMessage);
            $json = '{"data":"true"}';
        } else {
            $json = '{"data":"false","message":"手机号已存在"}';
        }
        echo $json;
    }

    #找回密码发送验证码

    public function actionsendverificationcode_findpwd() {

        $cellphone = addslashes($_POST["cellphone"]);

        $user_info = user::model()->findAll("cellphone = '$cellphone'");
        if (count($user_info) != 0) {
            if (isset(Yii::app()->session['phonecheckma'])) {
                unset(Yii::app()->session['phonecheckma']);
            }
            $phonecheckma = mt_rand(100000, 999999);
            Yii::app()->session['phonecheckma'] = $phonecheckma;
            $user = 'yzzzx'; //短信接口用户名 $user
            $pwd = 'yzz112233'; //短信接口密码 $pwd
            $mobiles = $cellphone; //说明：取用户输入的手号
            $contents = "亲爱的用户，您此次注册的验证码为" . $phonecheckma . "，验证码很重要，打死都不要告诉任何人哦，谢谢！"; //说明：取用户输入的短信内容
            $chid = 0; //通道ID
            $sendMessage = "http://www.106008.com/WebAPI/SmsAPI.asmx/SendSmsExt?user={$user}&pwd={$pwd}&mobiles={$mobiles}&contents={$contents}&chid={$chid}&sendtime=";
            file_get_contents($sendMessage);
            $json = '{"data":"true"}';
        } else {
            $json = '{"data":"false","message":"您还未注册哦"}';
        }
        echo $json;
    }

    #注册功能

    public function actionuser_register() {
        $nickname = addslashes($_POST["nickname"]);
        $cellphone = addslashes($_POST["cellphone"]);
        $verfication_code = $_POST["verfication_code"];
        $password = md5($_POST["password"]);

        $user_model = new user();

        $user_info = user::model()->findAll("cellphone = '$cellphone'");

        if (count($user_info) == 0) {
            if ($verfication_code == Yii::app()->session['phonecheckma']) {
                $user_model->cellphone = $cellphone;
                $user_model->password = $password;
                $user_model->openid = Yii::app()->session['openid'];

                if ($user_model->save()) {
                    $user_infox = user::model()->find("cellphone = '$cellphone'");
                    $user_id = $user_infox->user_id;
                    if ($nickname != "" && isset($nickname)) {
                        $user_infox->name = $nickname;
                    } else {
                        $user_infox->name = "yzz_" . $user_id;
                    }
                    $user_infox->save();
                    unset(Yii::app()->session['phonecheckma']);
                    $json = '{"data":"true"}';
                } else {
                    $json = '{"data":"false","message":"保存失败"}';
                }
            } else {
                $json = '{"data":"false","message":"验证码错误"}';
            }
        } else {
            $json = '{"data":"false","message":"手机号已存在"}';
        }
        echo $json;
    }

    # 忘记密码

    public function actionforgetpwd() {
        $this->renderPartial('forgetpwd');
    }

    #验证验证码

    public function actioncheck_verfication() {

        $cellphone = addslashes($_POST["cellphone"]);
        $verfication_code = $_POST["verfication_code"];

        $user_info = user::model()->findAll("cellphone = '$cellphone'");

        if (count($user_info) != 0) {
            if ($verfication_code == Yii::app()->session['phonecheckma']) {
                unset(Yii::app()->session['phonecheckma']);
                Yii::app()->session['cellphone'] = $cellphone;
                $json = '{"data":"true"}';
            } else {
                $json = '{"data":"false","message":"验证码错误"}';
            }
        } else {
            $json = '{"data":"false","message":"手机号不存在"}';
        }
        echo $json;
    }

    #改变密码界面

    public function actionchangepwd() {
        $this->renderPartial('changepwd');
    }

    #改变密码

    public function actionchange_pwd() {
        $cellphone = Yii::app()->session['cellphone'];
        $password = md5($_POST["password"]);

        $user_model = user::model();

        $user_info = $user_model->find("cellphone = '$cellphone'");
        if (count($user_info) != 0) {
            $user_info->password = $password;
            $user_info->openid = Yii::app()->session['openid'];
            if ($user_info->save()) {
                unset(Yii::app()->session['cellphone']);
                $json = '{"data":"true"}';
            } else {
                $json = '{"data":"false","message":"保存失败"}';
            }
        } else {
            $json = '{"data":"false","message":"手机号不存在"}';
        }
        echo $json;
    }

}
