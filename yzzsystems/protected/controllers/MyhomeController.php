<?php

class MyhomeController extends Controller {

    public function actionindex($openid = "xx") {
        if (!isset(Yii::app()->session['openid'])) {
            Yii::app()->session['openid'] = $openid;
        }

        //通过openid找user_id
        $user_model = user::model();

        $user_info = $user_model->find("openid = '" . $openid . "' AND isdeleted = 0");
        if (count($user_info) != 0) {
            $user_id = $user_info->user_id;
            $user_name = $user_info->name;
            //通过user_id找项目信息。
            $project_userid_model = project_userid::model();
            $project_userid_info = $project_userid_model->find("_user_id = " . $user_id);
            if (count($project_userid_info) != 0) {
                #查找项目信息
                $project_id = $project_userid_info->project_id;

                $project_model = project::model();
                $project_info = $project_model->findByPk(array($project_id, 'condition' => 'isdeleted = 0'));
                if (count($project_info) != 0) {
                    #查找项目节点信息
                    $project_schedule_model = project_schedule::model();
                    $project_schedule_info = $project_schedule_model->findAll(array('condition' => '_project_id = ' . $project_id . ' AND isdeleted = 0', "order" => "step asc"));
                    if (count($project_schedule_info) != 0) {
                        $this->renderPartial('index', array('project_schedule_info' => $project_schedule_info, 'project_info' => $project_info, 'user_name' => $user_name));
                    } else {
                        $this->redirect('./index.php?r=Myhome/error&error_info="此项目的节点信息不存在"');
                    }
                } else {
                    $this->redirect('./index.php?r=Myhome/error&error_info="此项目不存在"');
                }
            } else {
                $this->redirect('./index.php?r=Myhome/error&error_info="请先联系管理员给您添加项目"');
            }
        } else {
            $this->redirect('./index.php?r=default/index');
        }
    }

    //错误页
    public function actionerror($error_info = "未知错误") {
        $this->renderPartial('error', array("error_info" => $error_info));
    }
    
    public function actionIndexFirst() {
        $this->renderPartial('indexFirst');
    }

}

?>
