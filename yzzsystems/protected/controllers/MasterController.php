<?php

class MasterController extends Controller {

    public function actionperson_center($master_id = 0) {
        $master_infos = master::model()->findByPk($master_id);

        if (count($master_infos) != 0) {
            //计算星级，为总的星级除以评论的个数。
            $master_comment_info = master_comment::model()->findAll("_master_id =" . $master_infos->master_id);
            $star = 0;
            if (count($master_comment_info) != 0) {
                $star_level = 0;
                foreach ($master_comment_info as $k => $l) {
                    $star_level += $l->star_level;
                }
                $star = round($star_level / count($master_comment_info), 1);
            }
            // 查询职位
            $position_info = position::model()->findByPk($master_infos->_position_id);
            if (count($position_info) != 0) {
                $position_name = $position_info->position_name;
            } else {
                $position_name = "";
            }

            //计算行业时间 以年为单位  用当前时间减去入行时间
            $now_time = time();
            $entry_time = strtotime($master_infos->entry_time);
            $diff = $now_time - $entry_time;
            $diff_year = ceil($diff / 3600 / 24 / 365) - 1;

            //计算装修完成了几笔
            $fitment_info = project_schedule::model()->findAll("_master_id=" . $master_id . " AND status = 2");
            $fitment_count = count($fitment_info);

            //评价
            $master_comment = master_comment::model()->findAll(array("condition" => "description != '' AND _master_id = " . $master_id, 'order' => "master_comment_id DESC", "limit" => "5"));

            $this->renderPartial('person_center', array("master_infos" => $master_infos, "star" => $star, "position_name" => $position_name, "diff_year" => $diff_year,
                "fitment_count" => $fitment_count, "master_comment" => $master_comment));
        } else {
            $this->redirect('./index.php?r = Myhome/error&error_info = "此师傅不存在"');
        }
    }

    //保存评论
    public function actioncomment() {
        $master_comment_model = new master_comment();

        $user_model = user::model();
        $user_info = $user_model->find("openid = '" . Yii::app()->session['openid'] . "' AND isdeleted = 0");
        if (count($user_info) != 0) {
            $master_comment_model->_master_id = $_POST['master_id'];
            $master_comment_model->_user_id = 1;
            $master_comment_model->description = $_POST['comment'];
            $master_comment_model->star_level = $_POST['star_level'];
            $master_comment_model->is_anonymous = $_POST['is_anonymous'];
            if ($master_comment_model->save()) {
                $json = '{"data":"success"}';
            } else {
                $json = '{"data":"false","message":"保存失败"}';
            }
            echo $json;
        } else {
            $this->redirect('./index.php?r=Myhome/error&error_info = "请重新登录"');
        }
    }

}
