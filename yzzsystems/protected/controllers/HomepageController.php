<?php

class HomepageController extends Controller {
    # 首页

    public function actionhomepage() {
        $this->renderPartial('homepage');
    }

    //保存预约
    public function actionappointment() {
        $appointment_model = new appointment();
        $appointment_model->name = $_POST['name'];
        $appointment_model->cellphone = $_POST['tel'];
        $appointment_model->area = $_POST['mianji'];
        if ($appointment_model->save()) {
            echo "<script>parent.alert('预约成功！');parent.window.location.href='./index.php?r=homepage/homepage';</script>";
        }
    }

    // 师傅
    public function actionmaster() {

        $master_info = master::model()->findAll("master_id not in (19,20,21) AND isdeleted = 0");

        $this->renderPartial('master', array("master_info" => $master_info));
    }

    // 设计师
    public function actiondesigner() {

        $designer_info = designer::model()->findAll();

        $this->renderPartial('designer', array("designer_info" => $designer_info));
    }

    // 精品案例
    public function actioncase() {
        $project_info = project::model()->findAllBySql("SELECT * FROM project WHERE project_id NOT IN(SELECT a.project_id FROM project a JOIN project_schedule b ON a.project_id= b._project_id WHERE b.`status`!=2) AND isdeleted = 0");
        $this->renderPartial('case', array("project_info" => $project_info));
    }

    // 联系我们
    public function actioncontact() {
        $this->renderPartial('contact');
    }

    // 装修资讯
    public function actioninformation() {
        $this->renderPartial('information');
    }

    public function actionshuju() {
        $str = '{
    "status": 200,
    "info": {
        "leftList": "<li class=\"list-li\"><a class=\"list-a\" href=\"/zx/anli/183773/?source=2\" tjjj=\"wap_zx;wap_zxtk;wap_zxtk_d\"><img data-original=\"http://mtgi1.jia.com/120/155/20155672.m.jpg\" class=\"lazy\" src=\"\"/><p class=\"title\">简约公寓</p></a></li><li class=\"list-li\"><a class=\"list-a\" href=\"/zx/anli/183771/?source=2\" tjjj=\"wap_zx;wap_zxtk;wap_zxtk_d\"><img data-original=\"http://mtgi1.jia.com/120/155/20155654.m.jpg\" class=\"lazy\" src=\"\"/><p class=\"title\">慵懒个性的麻雀窝</p></a></li><li class=\"list-li\"><a class=\"list-a\" href=\"/zx/anli/183769/?source=2\" tjjj=\"wap_zx;wap_zxtk;wap_zxtk_d\"><img data-original=\"http://mtgi1.jia.com/120/155/20155632.m.jpg\" class=\"lazy\" src=\"\"/><p class=\"title\">现代简约</p></a></li><li class=\"list-li\"><a class=\"list-a\" href=\"/zx/anli/183766/?source=2\" tjjj=\"wap_zx;wap_zxtk;wap_zxtk_d\"><img data-original=\"http://mtgi2.jia.com/120/155/20155614.m.jpg\" class=\"lazy\" src=\"\"/><p class=\"title\">美式乡村风</p></a></li><li class=\"list-li\"><a class=\"list-a\" href=\"/zx/anli/183761/?source=2\" tjjj=\"wap_zx;wap_zxtk;wap_zxtk_d\"><img data-original=\"http://mtgi1.jia.com/120/155/20155550.m.jpg\" class=\"lazy\" src=\"\"/><p class=\"title\">清凉薄荷绿的北欧风</p></a></li>",
        "rightList": "<li class=\"list-li\"><a class=\"list-a\" href=\"/zx/anli/183772/?source=2\" tjjj=\"wap_zx;wap_zxtk;wap_zxtk_d\"><img data-original=\"http://mtgi3.jia.com/120/155/20155662.m.jpg\" class=\"lazy\" src=\"\"/><p class=\"title\">简约复古风</p></a></li><li class=\"list-li\"><a class=\"list-a\" href=\"/zx/anli/183770/?source=2\" tjjj=\"wap_zx;wap_zxtk;wap_zxtk_d\"><img data-original=\"http://mtgi3.jia.com/120/155/20155640.m.jpg\" class=\"lazy\" src=\"\"/><p class=\"title\">高奢中式风</p></a></li><li class=\"list-li\"><a class=\"list-a\" href=\"/zx/anli/183767/?source=2\" tjjj=\"wap_zx;wap_zxtk;wap_zxtk_d\"><img data-original=\"http://mtgi1.jia.com/120/155/20155621.m.jpg\" class=\"lazy\" src=\"\"/><p class=\"title\">混搭风两居室</p></a></li><li class=\"list-li\"><a class=\"list-a\" href=\"/zx/anli/183764/?source=2\" tjjj=\"wap_zx;wap_zxtk;wap_zxtk_d\"><img data-original=\"http://mtgi1.jia.com/120/155/20155563.m.jpg\" class=\"lazy\" src=\"\"/><p class=\"title\">绝对自我的空间！</p></a></li><li class=\"list-li\"><a class=\"list-a\" href=\"/zx/anli/183760/?source=2\" tjjj=\"wap_zx;wap_zxtk;wap_zxtk_d\"><img data-original=\"http://mtgi2.jia.com/120/155/20155537.m.jpg\" class=\"lazy\" src=\"\"/><p class=\"title\">典雅法式风情</p></a></li><input type=\"hidden\" id=\"page\" page=\"1\" page-count=\"3921\"/>"
    }
    }';
        echo $str;
    }

}
