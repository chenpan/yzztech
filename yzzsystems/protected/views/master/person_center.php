<!doctype html>
<html lang="zh">
    <head>
        <title><?php echo $master_infos->master_name; ?>的个人中心</title>
        <meta charset="UTF-8" />
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="stylesheet" href="./frontend/Font-Awesome/css/font-awesome.min.css">
        <style type="text/css">
            /*CSS源代码*/
            /* Common style */
            body,h1,h2,h3,h4,h5,h6,p,form{ margin:0;}
            ul{ margin:0; padding:0; list-style:none;}
            a img,input,button,textarea{ border:none;}
            a,input,button,textarea,select{ outline:none;}
            table{ border-collapse:collapse; border-spacing:0; width:100%;}
            h1{ text-indent:-9999px;}

            /* Link Style */
            a{ text-decoration:none;}
            a:link{ color:#333;}
            a:visited{ color:#333;}
            a:hover{ color:#be0000;}
            a:active{ color:#be0000;}

            /* Font Style */
            body{ font:0.1rem '微软雅黑',tahoma,Srial,helvetica,sans-serif; color:#4f4f4f;background-color: #F5F5F5;}
            h1,h2,h3,h4,h5,h6{ font-size:100%;}
            .normal{ font-weight:bold;}
            body .color6{ color:#666;}
            .t_cen{ text-align:center;}
            .color,body a.color{ color:#FF9800;}

            /* Body Style */
            .con{ padding:0 0.1rem; margin:0 auto;} //width:10.6rem;
            .con_2{ width:10.8rem; margin:0 auto;}
            .hide{ overflow:hidden;}
            .fl{ float:left;}
            /*.fr{ float:right;}*/

            .list{ width:100%; font-size:0.32rem;}
            .list li{ width:100%; border-bottom:1px solid #BBBBBB; padding:0.3rem 0; background:#fff;}
            .list .list_box{ display:block; color:#333;}
            .list_img{ float:left; width:1.5rem; height:1.5rem; padding:0.09rem; margin-right:0.3rem;margin-left:0.5rem;overflow:hidden; }
            .list_img img{ width:100%; height:100%;border-radius: 32px;-webkit-border-radius: 32px;-moz-border-radius: 32px;}
            .list_img_zan{ padding:0.09rem;overflow:hidden; }
            .list_img_zan img{float:left;width:1rem; height:1rem; border-radius: 32px;-webkit-border-radius: 32px;-moz-border-radius: 32px;}
            .list_img_flower img{margin-left: 5px;float:left;width:0.6rem; height:0.6rem; border-radius: 32px;-webkit-border-radius: 32px;-moz-border-radius: 32px;}
            .list_box span{ display:block; float:left; }
            .list_box h3{ font-size:0.42rem; height:0.6rem; line-height:0.6rem; overflow:hidden;margin-top:10px;}
            .list_box p{ margin:0.2rem 0 0.45rem; line-height:0.45rem; overflow:hidden;}
            .list_box font{ font-size:0.36rem;}
            .list_box em{ font-style:normal;display:inline-block;}
            .phone{ float:left; width:1rem; height:1rem; }

            .button {
                width: 100%;
                height: 40px;
                border-radius: 0.2rem;
                margin: 0.5rem 0 0.5rem;
                color: rgba(255, 255, 255, 1);
                background: #008BD6;
                font-size: 14px;
                cursor: pointer;
                overflow: hidden;
                font-weight: bold;
                transition: width 0.3s 0.15s, font-size 0.1s 0.15s;
            }

            .comment{
                width:auto; border:1px solid #BBBBBB; float:left; margin:2px;text-align:center;padding: 7px; border-radius: 0.1rem;
            }
        </style>
    </head>
    <body>
        <ul class="list">
            <li>
                <div class="con hide list_box">
                    <div class="list_img"><img src="<?php echo $master_infos->head_add; ?>"></div>
                    <span class="hide" style="width:3.5rem;">
                        <h3 class="normal"><strong><?php echo $master_infos->master_name; ?></strong></h3>                        
                        <strong class="normal color">
                            <?php for ($i = 0; $i < floor($star); $i++) { ?>
                                <i class="icon-star icon-1x"></i>
                                <?php
                            }
                            for ($i = 0; $i < 5 - floor($star); $i++) {
                                ?>                           
                                <i class="icon-star-empty icon-1x"></i>
                            <?php }
                            ?>
                        </strong>
                        <em class="fr color6"><?php echo $star; ?></em>
                    </span>
                    <span class="hide" style="width:3rem;margin-top: 10px;">
                        <h3 class="normal">
                            <?php echo $position_name; ?>
                        </h3>
                    </span>
                    <div style="margin-top:10px"><img class="phone" src="./frontend/person_center_pic/phone.png"></div>
                </div>
            </li>   
            <li>
                <div href="" class="con hide list_box">
                    <span class="hide" style="width:33%;text-align: center;border-right:1px solid #dedede;">
                        <h3 class="normal"><strong>行业时间</strong></h3>
                        <em class="fr color6"><?php echo $diff_year; ?>年</em>
                    </span>
                    <span class="hide" style="width:33%;text-align: center;border-right:1px solid #dedede;">
                        <h3 class="normal"><strong>完成装修</strong></h3>
                        <em class="fr color6"><?php echo $fitment_count; ?>笔</em>
                    </span>
                    <span class="hide" style="width:33%;text-align: center;">
                        <h3 class="normal"><strong>装修技能</strong></h3>
                        <em class="fr color6"><?php echo $master_infos->level; ?></em>
                    </span>
                </div>
            </li>  
        </ul>

        <ul class="list" style="margin-top:15px;border-top:1px solid #BBBBBB;">
            <li style="text-align:center;">
                <div class="con hide list_box" style="padding-left: 10px;padding-right: 10px;">
                    <div class="list_img_zan">
                        <span style="width:40%;">&nbsp</span>
                        <span class="hide">
                            <h3 class="normal"><strong>简介</strong></h3>                            
                        </span> 
                    </div>
                    <div style="float:left;text-align: left"><?php
                        if ($master_infos->master_description != "") {
                            echo $master_infos->master_description;
                        } else {
                            echo "暂无";
                        }
                        ?></div>
                </div>
            </li>           
        </ul>

        <ul class="list" style="margin-top:15px;border-top:1px solid #BBBBBB;">
            <li style="text-align:center;">
                <div class="con hide list_box" style="padding-left: 10px;padding-right: 10px;">
                    <div class="list_img_zan">
                        <span style="width:30%;">&nbsp</span>
                        <img src="./frontend/person_center_pic/zan.png">
                        <span class="hide">
                            <h3 class="normal"><strong>大家对他的评价</strong></h3>                            
                        </span> 
                    </div>
                    <?php foreach ($master_comment as $k => $l) { ?>
                        <div class="comment"><?php echo $l->description; ?></div>
                    <?php } ?>
                </div>
            </li>           
        </ul>

        <ul class="list" style="margin-top:15px;border-top:1px solid #BBBBBB;">
            <li>
                <div class="con hide list_box">
                    <div class="list_img_flower">
                        <img src="./frontend/person_center_pic/flower.png">
                        <span class="hide">
                            <h3 class="normal" style="margin-top:2px;"><strong>说说你的看法</strong></h3>
                        </span>
                    </div>                    
                </div>
            </li>
            <li>
                <div class="con hide list_box">
                    <span class="hide" style="padding-left:10px;padding-right: 10px;width: 95%;">
                        <em class="fr color6">评分</em>
                        <strong class="normal color star">
                            <i class="icon-star-empty icon-2x" id="1"></i>
                            <i class="icon-star-empty icon-2x" id="2"></i>
                            <i class="icon-star-empty icon-2x" id="3"></i>
                            <i class="icon-star-empty icon-2x" id="4"></i>
                            <i class="icon-star-empty icon-2x" id="5"></i>
                        </strong>
                        <br />
                        <textarea style="border:1px solid #BBBBBB;width: 93%;height: 100px;background-color: #F9F9F9;margin-top:10px;border-radius: 6px;-webkit-border-radius: 6px;-moz-border-radius: 6px;overflow:hidden; resize:none;padding: 10px;" placeholder="你觉得咱们师傅怎么样，说说看"></textarea>
                        <em style="float: right;margin-right: 15px;font-size:13px;line-height: 1.5">匿名评价</em>
                        <img id="is_open" style="float: right;width: 17px;" class="gray" src="./frontend/person_center_pic/gg_gray.png">
                        <button type="button" id="save" class="button">提 &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;交</button>
                    </span>

                </div>
            </li>
        </ul>

        <script src="./frontend/login_js/jquery-1.9.1.min.js" type="text/javascript"></script>    
        <script>
            /*Javascript代码片段*/
            $(document).ready(function() {
                var deviceWidth = $(window).outerWidth();
                if (deviceWidth > 1080) {
                    $("html").css("font-size", "100px");
                } else {
                    $("html").css("font-size", deviceWidth / 1080 * 100 + 'px');
                }

                $("#is_open").click(function() {
                    var className = $(this)[0].className;
                    if (className == "gray") {
                        $(this).attr('src', "./frontend/person_center_pic/gg_blue.png");
                        $(this).attr('class', "blue");
                    } else {
                        $(this).attr('src', "./frontend/person_center_pic/gg_gray.png");
                        $(this).attr('class', "gray");
                    }

                });

                $(".star i").click(function() {
                    var is = $(".star i");
                    for (var w = 0; w < 5; w++) {
                        var iq = is[w];
                        iq.setAttribute('class', 'icon-star-empty icon-2x')
                    }
                    var id = Number($(this).attr('id'));
                    for (var i = 0; i < id; i++) {
                        var ix = is[i];
                        ix.setAttribute('class', 'icon-star icon-2x')
                    }
                });

                $("#save").click(function() {
                    //获取评分
                    var star_level = $(".star").find(".icon-star").length;

                    //获取评论
                    var comment = $("textarea").val();

                    //获取是否是匿名
                    var is_anonymous = $(".blue").length;

                    if (star_level == 0) {
                        alert("请选择评分");
                    }

                    $.post("./index.php?r=Master/comment", {master_id:<?php echo $master_infos->master_id; ?>, star_level: star_level, comment: comment, is_anonymous: is_anonymous}, function(datainfo) {
                        var data = eval("(" + datainfo + ")");
                        if (data.data == "false") {
                            alert(data.message);
                        } else {
                            alert("保存成功");
                        }
                    })
                });
            });
        </script>
    </body>
</html>
