<!doctype html>
<html lang="zh">
    <head>
        <meta charset="UTF-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1"> 
        <meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=yes">
        <title>用户注册</title>
        <link rel="stylesheet" type="text/css" href="./bootstrap/css/bootstrap.min.css"/>
        <link rel="stylesheet" href="./frontend/Font-Awesome/css/font-awesome.min.css">
        <link rel="stylesheet" type="text/css" href="./frontend/login_css/styles.css">

        <script src='./bootstrap/js/bootstrap.js'></script>
    </head>
    <body>
        <div class="wrap-fluid">
            <div class="container-fluid paper-wrap bevel tlbr">
                <!-- CONTENT -->
                <div class="content-wrap">
                    <div class="row">
                        <div class="col-lg-12">
                            <form class="form-horizontal">
                                <div class="form-group" style="text-align: center;">
                                    <div class="col-sm-offset-4 col-sm-3">
                                        <img src="./frontend/logo/logo.png" style="width: 140px;">
                                    </div>
                                </div>
                                <div class="form-group paddd">                                    
                                    <div class="col-sm-offset-4 col-sm-3 bottom_low">
                                        <i class="icon-user"></i>
                                        <input type="text" class="register_cellphone_input name" id="nickname" placeholder="请输入姓名"/>
                                    </div>   
                                </div>
                                <div class="form-group paddd">                                    
                                    <div class="col-sm-offset-4 col-sm-3 bottom_low">
                                        <i class="icon-phone"></i>
                                        <input type="text" class="register_cellphone_input name" id="cellphone" placeholder="请输入手机号码"/>
                                        <a id="sendverificationcode">| 获取验证码</a>
                                    </div>   
                                </div>
                                <div class="form-group paddd">                                    
                                    <div class="col-sm-offset-4 col-sm-3 bottom_low">
                                        <i class="icon-pencil"></i>
                                        <input type="text" class="login__input name" id="verfication_code" placeholder="请输入验证码"/>
                                    </div>   
                                </div>
                                <div class="form-group paddd">                                    
                                    <div class="col-sm-offset-4 col-sm-3 bottom_low">
                                        <i class="icon-lock"></i>
                                        <input type="password" class="login__input pass" id="password" placeholder="请输入密码"/>
                                    </div>   
                                </div>
                                <div class="form-group" style="text-align: center;">
                                    <div class="col-sm-offset-4 col-sm-3">
                                        <button type="button" class="login__submit">注 册</button>
                                    </div>
                                </div>
                                <div class="form-group" style="text-align: center;">
                                    <div class="col-sm-offset-4 col-sm-3">
                                        <p class="login__signup">已经有了账号? &nbsp;<a href="./index.php?r=Default/index" target="_blank">登录</a></p>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
                <!--  / DEVICE MANAGER -->
            </div>
        </div>

        <script src='./frontend/login_js/stopExecutionOnTimeout.js?t=1'></script>
        <script src="./frontend/login_js/jquery-1.9.1.min.js" type="text/javascript"></script>    
        <script>
            $(document).ready(function() {
                var send_status = true;
                //发送验证码
                $("#sendverificationcode").click(function() {
                    if ($("#cellphone").val() == "" || $("#cellphone").val() == null) {
                        alert("手机号码不能为空");
                        return false;
                    } else if ($("#cellphone").val().length != 11) {
                        alert("手机号码不正确");
                        return false;
                    }
                    if (send_status) {
                        $.post("./index.php?r=default/sendverificationcode", {cellphone: $("#cellphone").val()}, function(datainfo) {
                            var data = eval("(" + datainfo + ")");
                            if (data.data == "false") {
                                alert(data.message);
                            } else {
                                time();
                            }
                        });
                    }
                });

                var wait = 60;
                function time() {
                    if (wait == 0) {
                        send_status = true;
                        $("#sendverificationcode").css("cursor", "pointer");
                        $("#sendverificationcode").text("| 发送验证码");
                        wait = 60;
                    } else {
                        send_status = false;
                        $("#sendverificationcode").text("| " + wait + "秒");
                        $("#sendverificationcode").attr("cursor", "default");
                        wait--;
                        setTimeout(function() {
                            time();
                        }, 1000)
                    }
                }



                var animating = false, submitPhase1 = 1100, submitPhase2 = 400;
                function ripple(elem, e) {
                    $('.ripple').remove();
                    var elTop = elem.offset().top, elLeft = elem.offset().left, x = e.pageX - elLeft, y = e.pageY - elTop;
                    var $ripple = $('<div class=\'ripple\'></div>');
                    $ripple.css({
                        top: y,
                        left: x
                    });
                    elem.append($ripple);
                }
                $(document).on('click', '.login__submit', function(e) {
                    if ($("#cellphone").val() == "" || $("#cellphone").val() == null) {
                        alert("手机号码不能为空");
                        return false;
                    } else if ($("#cellphone").val().length != 11) {
                        alert("手机号码不正确");
                        return false;
                    } else if ($("#verfication_code").val() == "" || $("#verfication_code").val() == null) {
                        alert("验证码不能为空");
                        return false;
                    } else if ($("#password").val() == "" || $("#password").val() == null) {
                        alert("密码不能为空");
                        return false;
                    }
                    var nickname = $("#nickname").val();
                    var cellphone = $("#cellphone").val();
                    var verfication_code = $("#verfication_code").val();
                    var password = $("#password").val();

                    var that = this;
                    $.post("./index.php?r=default/user_register", {nickname: nickname, cellphone: cellphone, password: password, verfication_code: verfication_code}, function(datainfo) {
                        var data = eval("(" + datainfo + ")");
                        if (data.data == "false") {
                            alert(data.message);
                        } else {
                            ripple($(that), e);
                            $(that).addClass('processing');
                            setTimeout(function() {
                                $(that).addClass('success');
                                setTimeout(function() {
                                    window.location.href = "./index.php?r=Myhome/index&openid=<?php echo Yii::app()->session['openid']; ?>";
                                }, submitPhase2 - 70);
                            }, submitPhase1);
                        }
                    })
                });
            });
        </script>
    </body>
</html>