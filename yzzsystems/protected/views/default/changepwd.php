<!doctype html>
<html lang="zh">
    <head>
        <meta charset="UTF-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1"> 
        <meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=yes">
        <title>修改密码</title>
        <link rel="stylesheet" type="text/css" href="./bootstrap/css/bootstrap.min.css"/>
        <link rel="stylesheet" href="./frontend/Font-Awesome/css/font-awesome.min.css">
        <link rel="stylesheet" type="text/css" href="./frontend/login_css/styles.css">

        <script src='./bootstrap/js/bootstrap.js'></script>
    </head>
    <body>
        <div class="wrap-fluid">
            <div class="container-fluid paper-wrap bevel tlbr">
                <!-- CONTENT -->
                <div class="content-wrap">
                    <div class="row">
                        <div class="col-lg-12">
                            <form class="form-horizontal"  style="text-align: center;">
                                <div class="form-group">
                                    <div class="col-sm-offset-4 col-sm-3">
                                        <img src="./frontend/logo/logo.png" style="width: 140px;">
                                    </div>
                                </div>
                                <div class="form-group paddd">                                    
                                    <div class="col-sm-offset-4 col-sm-3 bottom_low">
                                        <i class="icon-lock"></i>
                                        <input type="password" class="login__input pass" id="password" placeholder="请输入密码"/>
                                    </div>   
                                </div>
                                <div class="form-group paddd">                                    
                                    <div class="col-sm-offset-4 col-sm-3 bottom_low">
                                        <i class="icon-lock"></i>
                                        <input type="password" class="login__input pass" id="check_password" placeholder="请确认密码"/>
                                    </div>   
                                </div>
                                <div class="form-group">
                                    <div class="col-sm-offset-4 col-sm-3">
                                        <button type="button" class="login__submit">提 交</button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
                <!--  / DEVICE MANAGER -->
            </div>
        </div>

        <script src='./frontend/login_js/stopExecutionOnTimeout.js?t=1'></script>
        <script src="./frontend/login_js/jquery-1.9.1.min.js" type="text/javascript"></script>    
        <script>
            $(document).ready(function() {
                var animating = false, submitPhase1 = 1100, submitPhase2 = 400;
                function ripple(elem, e) {
                    $('.ripple').remove();
                    var elTop = elem.offset().top, elLeft = elem.offset().left, x = e.pageX - elLeft, y = e.pageY - elTop;
                    var $ripple = $('<div class=\'ripple\'></div>');
                    $ripple.css({
                        top: y,
                        left: x
                    });
                    elem.append($ripple);
                }
                $(document).on('click', '.login__submit', function(e) {
                    if ($("#password").val() == "" || $("#password").val() == null) {
                        alert("密码不能为空");
                        return false;
                    } else if ($("#check_password").val() == "" || $("#check_password").val() == null) {
                        alert("确认密码不能为空");
                        return false;
                    } else if ($("#password").val() != $("#check_password").val()) {
                        alert("两次密码不一致");
                        return false;
                    }
                    var password = $("#password").val();
                    var that = this;
                    $.post("./index.php?r=default/change_pwd", {password: password}, function(datainfo) {
                        var data = eval("(" + datainfo + ")");
                        if (data.data == "false") {
                            alert(data.message);
                        } else {
                            ripple($(that), e);
                            $(that).addClass('processing');
                            setTimeout(function() {
                                $(that).addClass('success');
                                setTimeout(function() {
                                    window.location.href = "./index.php?r=Myhome/index";
                                }, submitPhase2 - 70);
                            }, submitPhase1);
                        }
                    });
                });
            });
        </script>
    </body>
</html>