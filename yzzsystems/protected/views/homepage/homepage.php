<!DOCTYPE html>
<html lang="en"><head>
        <meta http-equiv="content-type" content="text/html; charset=UTF-8">
        <meta charset="UTF-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
        <meta http-equiv="X-UA-Compatible" content="IE=9; IE=8; IE=7; IE=EDGE,chrome=1">
        <meta name="viewport" content="width=device-width,initial-scale=1.0, minimum-scale=1.0, maximum-scale=1.0, user-scalable=no">
        <meta name="apple-mobile-web-app-capable" content="yes">
        <meta name="apple-mobile-web-app-status-bar-style" content="black">
        <meta name="format-detection" content="telephone=no">
        <title>优自在装修公司</title>
        <!--<meta name="keywords" content="天怡美装饰，重庆最好的装修公司，重庆家装公司，重庆整装公司，重庆装饰公司，重庆装修公司前十，重庆口碑最好的装修公司，">-->
        <!--<meta name="description" content="天怡美-整装行业的缔造者，以“高品质整装，环保整装，公益整装”引领西南装修市场。装修不后悔，就找【天怡美装饰工程有限公司】">-->
        <link href="./frontend/homepage/frozen.css" rel="stylesheet">
        <link href="./frontend/homepage/swiper.css" rel="stylesheet">
        <link href="./frontend/homepage/style.css" rel="stylesheet">
        <script charset="utf-8" src="./frontend/homepage/lxb.js"></script>
        <script src="./frontend/homepage/hm.js"></script>
        <script type="text/javascript" src="./frontend/homepage/jquery-1.js"></script>
        <script type="text/javascript" src="./frontend/homepage/swiper.js"></script>
<!--        <script>
            var _hmt = _hmt || [];
            (function() {
                var hm = document.createElement("script");
                hm.src = "//hm.baidu.com/hm.js?03d28acc6b36662d1cff0f7f3f842781";
                var s = document.getElementsByTagName("script")[0];
                s.parentNode.insertBefore(hm, s);
            })();
        </script>-->

    </head>
    <body>
        <div class="top clear">
            <div class="fl top-left clear">
                <div class="logo">
                    <img src="./frontend/logo/logo_pic.png" style="height: 40px;width: 100px;">
                </div>
                <!-- <ul class="top-city">
                    <li class="active"><a href="#">全国</a></li>
                    <ul class="top-city-open dn">
                        <li><a href="#">成都站</a></li>
                        <li><a href="#">重庆站</a></li>
                    </ul>
                </ul> -->
            </div>
            <div class="fr top-right">
                <a href="tel:138-0832-5212">138-0832-5212</a>
                <i class="top-tel"></i>
            </div>
        </div>


        <div id="banner" class="swiper-container-horizontal">
            <div class="swiper-wrapper" style="transition-duration: 0ms; transform: translate3d(-3806px, 0px, 0px);">
                <div class="swiper-slide" data-swiper-slide-index="0" style="width: 1903px;">
                    <a href="" target="_blank"><img src="./frontend/homepage/picture_1.jpg"></a>
                </div>
                <div class="swiper-slide swiper-slide-prev" data-swiper-slide-index="0" style="width: 1903px;">
                    <a href="" target="_blank"><img src="./frontend/homepage/picture_2.jpg"></a>
                </div>
                <div class="swiper-slide swiper-slide-active" data-swiper-slide-index="0" style="width: 1903px;">
                    <a href="" target="_blank"><img src="./frontend/homepage/picture_3.jpg"></a>
                </div>
            </div>
            <div class="pagination"><span class="swiper-pagination-bullet swiper-pagination-bullet-active"></span></div>
        </div>

        <div class="index-ov">
            <div class="index-w clear">
                <ul class="fl nav-l">
                    <li>
                        <a href="./index.php?r=homepage/information" class="warp1">
                            <i class="index-about"></i>
                            <!--<p>整居套餐</p>-->
                            <p>装修资讯</p>
                        </a>
                    </li>
                    <li>
                        <a href="./index.php?r=homepage/designer" class="warp2">
                            <i class="index-job"></i>
                            <p>设计师选择</p>
                        </a>
                    </li>
                    <!--   <li>
                          <a href="http://m.tymzs.com.cn/qiyejieshao/" class="warp2">
                              <i class="index-job"></i>
          
                              <p>关于我们</p>
                          </a>
                      </li> -->
                    <li>
                        <a href="./index.php?r=homepage/master" class="warp1">
                            <i class="index-contact"></i>
                            <p>装修师傅一览</p>
                        </a>
                    </li>
                </ul>
                <ul class="nav-r fr">
                    <li>
                        <a href="./index.php?r=homepage/case" class="warp2">
                            <i class="index-flow"></i>

                            <p class="nav-r-p">精品案例</p>
                        </a>
                    </li>
                    <li>
                        <a href="./index.php?r=Myhome/IndexFirst" class="warp1">
                            <i class="index-factor"></i>

                            <p>进度查询</p>
                        </a>
                    </li>
                    <li>
                        <!--./index.php?r=homepage/contact-->
                        <a href="tel:138-0832-5212" class="warp1">
                            <i class="index-app"></i>

                            <p>联系我们</p>
                        </a>
                    </li>
                </ul>
            </div>
        </div>
        <div class="wappic">
        </div>



        <!-- tjx -->
        <!--<div class="waprbg">
            <h2>限时活动</h2><br/>
        <div id="banner">
          <div class="swiper-wrapper">
           
            <div class="swiper-slide"><a href="/huodong" target="_blank"><img src="/themes/tym/mobile/images/b.png" /></a></div>
             
          </div>
        <div class="pagination"></div>
        </div>
        </div>-->
        <!-- tjx -->
<!--        <div class="waprbg">
            <h2>免费量房设计</h2>
            <iframe style="display:none" name="test"></iframe>
            <form action="./index.php?r=homepage/appointment" target="test" method="post">
                <div class="box"><input name="name" placeholder="请输入姓名?" type="text"></div>
                <div class="box"><input id="formAtel" name="tel" placeholder="请输入您的手机号码?" type="text"></div>
                <div class="box"> <input name="mianji" placeholder="请输入房屋面积?" type="text"></div>
                                <div class="add-downs">
                                    <select class="needsclick" name="yusuan">
                                        <option value="" selected="selected">装修预算</option>
                                        <option value="3-5万">3-5万</option>
                                        <option value="5-8万">5-8万</option>
                                        <option value="8-12万">8-12万</option>
                                        <option value="12-18万">12-18万</option>
                                        <option value="18-25万">18-25万</option>
                                        <option value="25-30万">25-30万</option>
                                        <option value="30万以上">30万以上</option>
                                    </select>
                                </div>

                <input name="time" id="time" value="2017-04-17 20:28:46" type="hidden">
                <input name="form" value="zaixianyuyue" type="hidden">
                <input name="laiyuan" value="2" type="hidden">
                <input name="test_value" value="" type="hidden">
                <input name="" class="submit" value="预约免费量房设计" onclick="return checkform(this.form)" type="submit">
            </form>
        </div>-->

        <div class="foot">
            <p>
                ©Copyright&nbsp;2017&nbsp;优自在装修公司<br>
                <!--蜀ICP备14031223号-1&nbsp;&nbsp;Powered&nbsp;by:品度科技-->
            </p>
        </div>

        <script type="text/javascript">

            var mySwiper2 = new Swiper('#banner', {
                autoplay: 5000,
                visibilityFullFit: true,
                loop: true,
                pagination: '.pagination',
            });

        </script>

        <script type="text/javascript">
            function checkform(form) {
                var tel = $('#formAtel').val();
                if (tel == '') {
                    alert("请输入手机号码!");
                    $('#formAtel').focus();
                    return false;
                }
                var reg = /^0?1[3|4|7|5|8][0-9]\d{8}$/;
                if (reg.test(tel)) {
                } else {
                    alert("请填写正确的手机号码!");
                    $('#formAtel').focus();
                    return false;
                }
                return true;
                alert("保存成功");
            }
        </script>



        <script type="text/javascript" src="./frontend/homepage/js.js"></script>

    </body></html>