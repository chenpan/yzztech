<!DOCTYPE html>
<html>
    <head>
        <title>装修案例效果图,设计案例图片_重庆优自在装修手机版</title>
        <meta charset="utf-8" />
        <meta name="keywords" content="装修案例,案例图片" />
        <meta name="description" content= "重庆优自在装修设计案例效果图欣赏,为您提供2015年国内外最新流行的装修设计案例效果图大全。" />
        <meta name="viewport" content="width=device-width,initial-scale= 1.0, minimum-scale=1.0, maximum-scale=1.0"/>
        <!--<link rel="shortcut icon" href="http://m.jia.com/Public/img//wap/favicon.ico" />-->
        <link rel="stylesheet" type="text/css" href="./frontend/information/head.css?rand=20160926" />
        <link rel="stylesheet" type="text/css" href="./frontend/information/webImages.css?rand=20160926" />
        <script type="text/javascript" src="./frontend/information/zepto.min.js?rand=20160926"></script>
        <script type="text/javascript" src="./frontend/information/loading.js?rand=20160926"></script>
        <script type="text/javascript" src="./frontend/information/newWapJia.js?rand=20160926"></script>
        <script type="text/javascript" src="./frontend/information/newTuku.js?rand=20160926"></script>
        <script type="text/javascript" src="./frontend/information/public.js?rand=20160926"></script>
        <script type="text/javascript" src="./frontend/information/lazyload.min.js?rand=20160926"></script>
        <script type="text/javascript" src="./frontend/information/wapCommon.js"></script>
    </script>
</head>
<body id='bbb'>
    <div id="1qa2ws"></div>
    <script src="./frontend/information/jsencrypt.js" type="text/javascript"></script>
    <script src="./frontend/information/tjj.js" type="text/javascript"></script>
    <script type="text/javascript">document.documentElement.style.fontSize = Math.min(window.innerWidth * 100 / 320, 150) + "px";
    </script>
    <header class="newHeader fix_header">
        <a href="javascript:history.back(-1);" class="backtohis pl">返回</a>
        <h2 class="header_title"><span>装修图库</span></h2>
        <a href="javascript:void(0);" class="share-icon pr"></a>
        <!--  <a href="javascript:void(0);" class="header_search pr"></a> -->
    </header>
    <div class="popup-new" id="load_div"></div>
    <div class="loading-common" id="load_icon"></div>
    <div class="popmask"></div>
    <section class="tuku-images-list clearfix" id='mm'>
        <nav class="tuku-images-nav ">
            <ul class="nav-ul" id="ul">
                <li class="nav-li">
                    <a href="javascript:;" class="nav-a">
                        <span class="nav-span">户型</span>
                        <em class="nav-em"></em>
                    </a>
                </li>
                <li class="nav-li">
                    <a href="javascript:;" class="nav-a">
                        <span class="nav-span">面积</span>
                        <em class="nav-em"></em>
                    </a>
                </li>
                <li class="nav-li">
                    <a href="javascript:;" class="nav-a">
                        <span class="nav-span">预算</span>
                        <em class="nav-em"></em>
                    </a>
                </li>
                <li class="nav-li">
                    <a href="javascript:;" class="nav-a">
                        <span class="nav-span">风格</span>
                        <em class="nav-em"></em>
                    </a>
                </li>
            </ul>
            <div class="nav-show-menu" id='unit'>
                <ul class="menu-ul clearfix">
                    <li class="menu-li cur">
                        <a href="http://m.jia.com/zx/anli/0-0-0-0/?&amp;t=" class="menu-a" data-id="0" tjjj="zx.select.unit_全部">全部</a>
                    </li>
                    <li class="menu-li">
                        <a href="/zx/anli/0-14-0-0/?&t=" class="menu-a" data-id='14' tjjj='zx.select.unit_公寓'>公寓</a>
                    </li>
                    <li class="menu-li">
                        <a href="/zx/anli/0-15-0-0/?&t=" class="menu-a" data-id='15' tjjj='zx.select.unit_二居室'>二居室</a>
                    </li>
                    <li class="menu-li">
                        <a href="/zx/anli/0-16-0-0/?&t=" class="menu-a" data-id='16' tjjj='zx.select.unit_别墅'>别墅</a>
                    </li>
                    <li class="menu-li">
                        <a href="/zx/anli/0-17-0-0/?&t=" class="menu-a" data-id='17' tjjj='zx.select.unit_小户型'>小户型</a>
                    </li>
                    <li class="menu-li">
                        <a href="http://m.jia.com/zx/anli/0-18-0-0/?&amp;t=" class="menu-a" data-id="18" tjjj="zx.select.unit_三居室">三居室</a>
                    </li>
                    <li class="menu-li">
                        <a href="http://m.jia.com/zx/anli/0-19-0-0/?&amp;t=" class="menu-a" data-id="19" tjjj="zx.select.unit_复式">复式</a>
                    </li>
                    <li class="menu-li">
                        <a href="http://m.jia.com/zx/anli/0-20-0-0/?&amp;t=" class="menu-a" data-id="20" tjjj="zx.select.unit_一居室">一居室</a>
                    </li>
                    <li class="menu-li">
                        <a href="http://m.jia.com/zx/anli/0-21-0-0/?&amp;t=" class="menu-a" data-id="21" tjjj="zx.select.unit_四居室">四居室</a>
                    </li>
                    <li class="menu-li">
                        <a href="http://m.jia.com/zx/anli/0-24-0-0/?&amp;t=" class="menu-a" data-id="24" tjjj="zx.select.unit_大户型">大户型</a>
                    </li>
                    <li class="menu-li">
                        <a href="http://m.jia.com/zx/anli/0-34-0-0/?&amp;t=" class="menu-a" data-id="34" tjjj="zx.select.unit_四合院">四合院</a>
                    </li>
                    <li class="menu-li">
                        <a href="http://m.jia.com/zx/anli/0-38-0-0/?&amp;t=" class="menu-a" data-id="38" tjjj="zx.select.unit_错层">错层</a>
                    </li>
                    <li class="menu-li">
                        <a href="http://m.jia.com/zx/anli/0-22-0-0/?&amp;t=" class="menu-a" data-id="22" tjjj="zx.select.unit_跃层">跃层</a>
                    </li>                        <li class="menu-li-more"><a href="javascript:;" class="menu-a-more">查看全部</a></li>
                </ul>
            </div>
            <div class="nav-show-menu" id="area">
                <ul class="menu-ul clearfix">
                    <li class="menu-li cur">
                        <a href="http://m.jia.com/zx/anli/0-0-0-0/?&amp;t=" class="menu-a" data-id="0" tjjj="zx.select.area_全部">全部</a>
                    </li>
                    <li class="menu-li">
                        <a href="http://m.jia.com/zx/anli/43-0-0-0/?&amp;t=" class="menu-a" data-id="43" tjjj="zx.select.area_40㎡">40㎡</a>
                    </li>
                    <li class="menu-li">
                        <a href="http://m.jia.com/zx/anli/44-0-0-0/?&amp;t=" class="menu-a" data-id="44" tjjj="zx.select.area_50㎡">50㎡</a>
                    </li>
                    <li class="menu-li">
                        <a href="http://m.jia.com/zx/anli/45-0-0-0/?&amp;t=" class="menu-a" data-id="45" tjjj="zx.select.area_60㎡">60㎡</a>
                    </li>
                    <li class="menu-li">
                        <a href="http://m.jia.com/zx/anli/46-0-0-0/?&amp;t=" class="menu-a" data-id="46" tjjj="zx.select.area_70㎡">70㎡</a>
                    </li><li class="menu-li">
                        <a href="http://m.jia.com/zx/anli/47-0-0-0/?&amp;t=" class="menu-a" data-id="47" tjjj="zx.select.area_80㎡">80㎡</a>
                    </li>
                    <li class="menu-li">
                        <a href="http://m.jia.com/zx/anli/48-0-0-0/?&amp;t=" class="menu-a" data-id="48" tjjj="zx.select.area_90㎡">90㎡</a>
                    </li><li class="menu-li">
                        <a href="http://m.jia.com/zx/anli/49-0-0-0/?&amp;t=" class="menu-a" data-id="49" tjjj="zx.select.area_100㎡">100㎡</a>
                    </li>
                    <li class="menu-li">
                        <a href="http://m.jia.com/zx/anli/50-0-0-0/?&amp;t=" class="menu-a" data-id="50" tjjj="zx.select.area_110㎡">110㎡</a>
                    </li>
                    <li class="menu-li">
                        <a href="http://m.jia.com/zx/anli/51-0-0-0/?&amp;t=" class="menu-a" data-id="51" tjjj="zx.select.area_120㎡">120㎡</a>
                    </li>
                    <li class="menu-li">
                        <a href="http://m.jia.com/zx/anli/52-0-0-0/?&amp;t=" class="menu-a" data-id="52" tjjj="zx.select.area_130㎡">130㎡</a>
                    </li>
                    <li class="menu-li">
                        <a href="http://m.jia.com/zx/anli/53-0-0-0/?&amp;t=" class="menu-a" data-id="53" tjjj="zx.select.area_140㎡以上">140㎡以上</a>
                    </li>                        <li class="menu-li-more"><a href="javascript:;" class="menu-a-more">查看全部</a></li>
                </ul>
            </div>
            <div class="nav-show-menu" id="budget">
                <ul class="menu-ul clearfix">
                    <li class="menu-li cur"><a href="http://m.jia.com/zx/anli/0-0-0-0/?&amp;t=" class="menu-a" data-id="0" tjjj="zx.select.budget_全部">全部</a></li>
                    <li class="menu-li">
                        <a href="http://m.jia.com/zx/anli/0-0-0-54/?&amp;t=" class="menu-a" data-id="54" tjjj="zx.select.budget_3万以下">3万以下</a>
                    </li>
                    <li class="menu-li">
                        <a href="http://m.jia.com/zx/anli/0-0-0-55/?&amp;t=" class="menu-a" data-id="55" tjjj="zx.select.budget_3-5万">3-5万</a>
                    </li>
                    <li class="menu-li">
                        <a href="http://m.jia.com/zx/anli/0-0-0-56/?&amp;t=" class="menu-a" data-id="56" tjjj="zx.select.budget_5-10万">5-10万</a>
                    </li>
                    <li class="menu-li">
                        <a href="http://m.jia.com/zx/anli/0-0-0-57/?&amp;t=" class="menu-a" data-id="57" tjjj="zx.select.budget_10-15万">10-15万</a>
                    </li>
                    <li class="menu-li">
                        <a href="http://m.jia.com/zx/anli/0-0-0-58/?&amp;t=" class="menu-a" data-id="58" tjjj="zx.select.budget_15-20万">15-20万</a>
                    </li>
                    <li class="menu-li">
                        <a href="http://m.jia.com/zx/anli/0-0-0-59/?&amp;t=" class="menu-a" data-id="59" tjjj="zx.select.budget_20万以上">20万以上</a>
                    </li>                        <li class="menu-li-more"><a href="javascript:;" class="menu-a-more">查看全部</a></li>
                </ul>
            </div>
            <div class="nav-show-menu" id="style">
                <ul class="menu-ul clearfix">
                    <li class="menu-li cur">
                        <a href="http://m.jia.com/zx/anli/0-0-0-0/?&amp;t=" class="menu-a" data-id="0" tjjj="zx.select.style_全部">全部</a>
                    </li>
                    <li class="menu-li">
                        <a href="http://m.jia.com/zx/anli/0-0-64-0/?&amp;t=" class="menu-a" data-id="64" tjjj="zx.select.style_简约">简约</a>
                    </li>
                    <li class="menu-li">
                        <a href="http://m.jia.com/zx/anli/0-0-65-0/?&amp;t=" class="menu-a" data-id="65" tjjj="zx.select.style_混搭">混搭</a>
                    </li>
                    <li class="menu-li">
                        <a href="http://m.jia.com/zx/anli/0-0-66-0/?&amp;t=" class="menu-a" data-id="66" tjjj="zx.select.style_欧式">欧式</a>
                    </li>
                    <li class="menu-li">
                        <a href="http://m.jia.com/zx/anli/0-0-67-0/?&amp;t=" class="menu-a" data-id="67" tjjj="zx.select.style_田园">田园</a>
                    </li>
                    <li class="menu-li">
                        <a href="http://m.jia.com/zx/anli/0-0-68-0/?&amp;t=" class="menu-a" data-id="68" tjjj="zx.select.style_古典">古典</a>
                    </li>
                    <li class="menu-li">
                        <a href="http://m.jia.com/zx/anli/0-0-71-0/?&amp;t=" class="menu-a" data-id="71" tjjj="zx.select.style_中式">中式</a>
                    </li>
                    <li class="menu-li">
                        <a href="http://m.jia.com/zx/anli/0-0-72-0/?&amp;t=" class="menu-a" data-id="72" tjjj="zx.select.style_地中海">地中海</a>
                    </li>
                    <li class="menu-li">
                        <a href="http://m.jia.com/zx/anli/0-0-73-0/?&amp;t=" class="menu-a" data-id="73" tjjj="zx.select.style_宜家">宜家</a>
                    </li>
                    <li class="menu-li">
                        <a href="http://m.jia.com/zx/anli/0-0-74-0/?&amp;t=" class="menu-a" data-id="74" tjjj="zx.select.style_美式">美式</a>
                    </li>
                    <li class="menu-li">
                        <a href="http://m.jia.com/zx/anli/0-0-75-0/?&amp;t=" class="menu-a" data-id="75" tjjj="zx.select.style_东南亚">东南亚</a>
                    </li>
                    <li class="menu-li">
                        <a href="http://m.jia.com/zx/anli/0-0-77-0/?&amp;t=" class="menu-a" data-id="77" tjjj="zx.select.style_日式">日式</a>
                    </li>
                    <li class="menu-li">
                        <a href="http://m.jia.com/zx/anli/0-0-79-0/?&amp;t=" class="menu-a" data-id="79" tjjj="zx.select.style_loft">loft</a>
                    </li>
                    <li class="menu-li-more"><a href="javascript:;" class="menu-a-more">查看全部</a></li>
                </ul>
            </div>
        </nav>
        <ul class="list-ul" id="list_left"></ul>
        <ul class="list-ul" id="list_right"></ul>
        <div id="empty"></div>
    </section>
    <div class="tuku-loading"><img src="./frontend/information/loading.gif"></div>
    <script type="text/javascript" src="./frontend/information/add_popup.js?rand="></script>
    <script>
        $(function() {
            function mob() {
                var ppp = /^1[3|4|5|7|8]\d{9}$/;
                $.get("/JiaZhuangxiuTouFang/getUserMobile", function(data) {
                    if (data.setId && ppp.test(data.mobile)) {
                        $(".telcheck_20160531").val(data.mobile);//attr("disabled", "true");
                    } else {
                        if (data.mobile != false) {
                            $(".telcheck_20160531").val(data.mobile);
                        }
                    }

                }, 'json');
            }
//mob();
        })

    </script><script type="text/javascript">
        $(function() {
            if (CookiesInfo.get("FROMCHANNEL") != 'nuomi') {
                add_popup();
            }

            assign_areaflag = 'chongqing'
            var pop_assign_click = 1;
            $("#pop_assign_").live("click", function() {
                var target = '';
                var pop_mobile = $("#pop_mobile").val();
                if (pop_mobile.trim() == '') {
                    alert('请输入您的手机号码');
                    return false;
                }
                if (!/^1[3|4|5|7|8]\d{9}$/.test(pop_mobile)) {
                    alert('对不起，您输入的手机号码有误');
                    return false;
                }
                var str = {username: "装修弹窗报名用户", mobile: pop_mobile, shop_id: 0, city: assign_areaflag, source: "wap-pop-assign"};
                var fun = (function(data) {
                    if (data.status == 200) {
                        if ('brs' == target) {
                            alert('报名成功!');
                            return true;
                        } else {
                            location.href = '/zx/wanshanxinxi?type=app';
                        }
                    } else {
                        alert(data.info);
                    }
                    pop_assign_click = 0
                })
                if (pop_assign_click == 1) {
                    pop_assign_click = 0;
                    load("/new_zhuangxiu/AjaxSaveNewShopApplyNoCode", str, fun, 'json')
                }

            })
        })

    </script><div class="popup-new" id="load_div"></div><div class="loading-common" id="load_icon"></div><script>
        var _hmt = _hmt || [];
        (function() {
            var hm = document.createElement("script");
            if (window.location.href.indexOf("m.jia.com/JiaZhuangxiuTmp") > -1) {
                hm.src = "./frontend/information/hm.js?c1f9f5201c657650b5c74da803886fe1";
            } else {
                hm.src = "./frontend/information/hm.js?0ef6d34be86061f1e88b15621d1fef05";
            }
            var s = document.getElementsByTagName("script")[0];
            s.parentNode.insertBefore(hm, s);
        })();

    </script><!-- 页尾 -->
    <footer class="new-footer newFooter">
        <a href="javascript:;" class="defaultKefuBtn" style="display:none"></a>
        <!-- 小能客服 -->
        <!--<section class="xiaoneng-swiper"></section>-->
        <!-- 版本 -->
        <section class="versions-type">
            <!--                <ul class="clearfix">
                                <li class="on">
                                    <a href="javascript:;">
                                        <div class="table">
                                            <div class="table-cell">
                                                <span class="touch-type">触屏版</span>
                                                <p>触屏版</p>
                                            </div>
                                        </div>
                                    </a>
                                </li>
                                <li>
                                    <a tjjj="m_index_toapp" href="//m.jia.com/qjzx">
                                        <div class="table">
                                            <div class="table-cell">
                                                <span class="app-type">App</span>
                                                <p>App</p>
                                            </div>
                                        </div>
                                        <i class="down-load">下载</i>
                                    </a>
                                </li>
                                <li>
                                    <a tjjj="m_index_toweixin" href="//m.jia.com/page/weixin.html">
                                        <div class="table">
                                            <div class="table-cell">
                                                <span class="wecat-type">微信</span>
                                                <p>微信</p>
                                            </div>
                                        </div>
                                    </a>
                                </li>
                            </ul>-->
            <p class="cr_sm">copyright © 2005-2017优自在 版权所有</p>
            <!--<p class="cr_sm_sec">沪ICP备13002314号 沪B2-20090108</p>-->
        </section>
        <p class="hot-line">优自在服务热线：<span>138-0832-5212</span></p>
    </footer>
    <!-- 滑动到顶部 -->
    <a class="go-back" href="javascript:;">back</a>
    <script type="text/javascript" src="./frontend/information/kefuCommon.js"></script>
    <script type="text/javascript">
        if (window.location.host == "h5.m.jia.com" || navigator.userAgent.indexOf("QIJIA") > -1 || window.location.host == "zixun.m.jia.com") {
            $(".newFooter").hide();
        }
        if (window.location.host == "m.jia.com") {
            var checkLoginVersions = new Date().getTime();
            wapLoadScript("./frontend/information/checkLogin.js?v=" + checkLoginVersions);
        }


        var _bottom_city = CookiesInfo.get("LOGIN_AREAFLAG") || "";
        if (typeof indexFooterShow != "undefined") { //首页
            _bottom_city = $('.page').attr('data-city') || "other";
            if (_bottom_city == "other") {
                _bottom_city = window.location.pathname.split('/')[1] || "other";
            }
        }


        var footerNavStr = '<div class="J_fSpace" style="height:.45rem"></div><nav class="footer-nav">';
        footerNavStr += '<ul><li class="on"><a tjjj="m_index_home" href="//m.jia.com/"><span class="index">首页</span><p>首页</p>';
        footerNavStr += '</a></li><li><a tjjj="m_index_zx" href="//m.jia.com/zx/' + _bottom_city + '/">';
        footerNavStr += '<span class="find-zx">找装修</span><p>找装修</p></a></li><li>';
        footerNavStr += '<a tjjj="m_index_mall" href="//m.jia.com/retail/' + _bottom_city + '/"><span class="find-cl">买材料</span>';
        footerNavStr += '<p>买材料</p></a></li><li><a tjjj="m_index_zixun" href="/zx/page/xzx.html">';
        footerNavStr += '<span class="study-zx">学装修</span><p>学装修</p></a></li><li><a tjjj="m_index_i" href="//m.jia.com/i/">';
        footerNavStr += '<span class="mine">我的</span><p>我的</p></a></li></ul></nav>';


        //底部导航显示
        if (window.location.host == "m.jia.com") {
            if (typeof indexFooterShow != "undefined") { //首页
                $("body").append(footerNavStr);
            }

            if (/^\/i\//.test(window.location.pathname)) { //个人中心首页
                $("body").append(footerNavStr);
                $(".footer-nav li").eq(4).addClass("on").siblings().removeClass("on");
            }
        }


        try {
            if (typeof (urlHrefObject) == "object") {
                if (urlHrefObject.noheader) {
                    $(".newFooter,newHeader").remove();
                }
            }
        } catch (e) {
        }
        ;
    </script>
    <script>
        var _hmt = _hmt || [];
        (function() {
            var hm = document.createElement("script");
            hm.src = "./frontend/information/hm.js?0ef6d34be86061f1e88b15621d1fef05";
            var s = document.getElementsByTagName("script")[0];
            s.parentNode.insertBefore(hm, s);
        })();
    </script>
    <script type="text/javascript">
        var is_load = 0;
        flushPage();
        $(window).on("scroll", function() {
            if (is_load == 0) {
                return;
            }
            var scrollTop = $(this).scrollTop();
            var winHeight = $(this).height();
            var bodyHeight = $('body').height();
            if (scrollTop + winHeight >= bodyHeight) {
                var page_num = $("#page").attr("page");
                var page_count = $("#page").attr("page-count");
                //                console.log(page_num + '--' + page_count);
                if (parseInt(page_num) < parseInt(page_count)) {
                    is_load = 0;
                    flushPage();
                }
            }
        });

        function flushPage() {
            var tjjj_flag = "2";
            var unit = $("#unit ul li.cur a").attr("data-id");
            var area = $("#area ul li.cur a").attr("data-id");
            var budget = $("#budget ul li.cur a").attr("data-id");
            var style = $("#style ul li.cur a").attr("data-id");
            var page = $("#page").attr("page") == undefined ? "1" : (parseInt($("#page").attr("page")) + 1);
            var areaflag = "chongqing";
            var target = "";
            var fun = function(msg) {
                alert(msg);
                if (msg.status == 200) {
                    $("#page").remove();
                    $("#list_left").append(msg.info.leftList);
                    $("#list_right").append(msg.info.rightList);
                    //                    lazyload();
                    $(window).trigger("scroll");
                    is_load = 1
                } else {
                    alert(msg.info);
                }
                $("li img.lazy").each(function(i, e) {
                    var _temp = $(e).attr("data-original");
                    $(e).attr("src", _temp);
                    $(e).removeAttr("data-original");
                    $(e).removeClass("lazy");
                });
                $(".tuku-loading").hide();
            }
            $(".tuku-loading").show();
            $.get("./index.php?r=homepage/shuju", {tjjj_flag: tjjj_flag, unit: unit, area: area, budget: budget, style: style, page: page, areaflag: areaflag, t: target}, fun, 'json');
        }

        function loadFun() {
            if ($(".tuku-images-list").length > 0) {
                headerHeight = $(".tuku-images-nav").offset().top;
                $(window).on("scroll", function() {
                    var scrollTop = $(this).scrollTop();
                    if (scrollTop >= headerHeight) {
                        $(".tuku-images-nav").css({'position': 'fixed', 'top': 0});
                    } else {
                        $(".tuku-images-nav").css({'position': 'absolute', 'top': 0});
                    }
                });
            }
        }
    </script>
    <script>
        window.onload = function() {
            //        document.getElementById("bbb").style.display="block";
            if (isFun("loadFun")) {
                loadFun();
            }
            if (isFun("closeFooterBanner")) {
                closeFooterBanner();
            }
        }
        function isFun(funcName) {
            try {
                if (typeof (eval(funcName)) == "function") {
                    return true;
                }
            } catch (e) {
            }
            return false;
        }
    </script>
</body>
</html>