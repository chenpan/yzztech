<!DOCTYPE HTML>
<html>
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1"> 
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <title><?php echo $user_name; ?> 家的装修日记</title>
        <link rel="stylesheet" href="./platform/css/bootstrap.css">
        <link rel="stylesheet" href="./frontend/myhome_css/main.css" />
        <link rel="stylesheet" href="./frontend/myhome_css/style.css" />

        <link rel="stylesheet" href="./frontend/Font-Awesome/css/font-awesome.min.css">

        <link rel="stylesheet" href="./frontend/myhome_index_css/item.css" />

        <link rel="stylesheet" type="text/css" href="./frontend/photoswipe/css/normalize.css" />
        <link rel="stylesheet" href="./frontend/photoswipe/dist/photoswipe.css"> 
        <link rel="stylesheet" href="./frontend/photoswipe/dist/default-skin/default-skin.css"> 
        <link rel="stylesheet" href="./frontend/photoswipe/css/site.css"> 
        <!-- Core JS file -->
        <script src="./frontend/photoswipe/dist/photoswipe.min.js"></script> 
        <script src="./frontend/login_js/jquery-1.9.1.min.js" type="text/javascript"></script>    
        <!-- UI JS file -->
        <script src="./frontend/photoswipe/dist/photoswipe-ui-default.min.js"></script> 

        <script type="text/javascript" src="./frontend/myhome_js/iscroll.js"></script>
        <!--[if IE]>
                <script src="http://libs.useso.com/js/html5shiv/3.7/html5shiv.min.js"></script>
        <![endif]-->

        <script type="text/javascript" src="./platform/js/bootstrap.js"></script>

        <style type="text/css">
            #wrapper {
                position: relative;
                z-index: 1;
                height: 120px;
                width: 92%;
                /*background: #ccc;*/
                overflow: hidden;
                -ms-touch-action: none;
            }
            #scroller {                
                position: absolute;
                z-index: 1;
                -webkit-tap-highlight-color: rgba(0,0,0,0);
                width: <?php echo count($project_schedule_info) * 55; ?>px;
                height: 120px;
                -webkit-transform: translateZ(0);
                -moz-transform: translateZ(0);
                -ms-transform: translateZ(0);
                -o-transform: translateZ(0);
                transform: translateZ(0);
                -webkit-touch-callout: none;
                -webkit-user-select: none;
                -moz-user-select: none;
                -ms-user-select: none;
                user-select: none;
                -webkit-text-size-adjust: none;
                -moz-text-size-adjust: none;
                -ms-text-size-adjust: none;
                -o-text-size-adjust: none;
                text-size-adjust: none;
            }
            /*
                        #scroller ul {
                            list-style: none;
                            width: 100%;
                            padding: 0;
                            margin: 0;
                        }*/

            #scroller li {
                /*                width: 120px;
                                height: 160px;
                                float: left;
                                line-height: 160px;
                                border-right: 1px solid #ccc;
                                border-bottom: 1px solid #ccc;
                                background-color: #fafafa;
                                font-size: 14px;
                                overflow: hidden;*/
                text-align: center;
            }
        </style>
    </head>
    <!--onload="loaded()"-->
    <body>
        <div class="view-box">
            <div class="b-info" style="background:none;">
                <a href="#">
                    <i></i>
                    <p>
                        <i class="icon-map-marker icon-1x"></i>
                        <span class="b-name"><?php echo $project_info->project_address; ?></span>
                    </p>                    
                </a>
                <!--                <a style="float:right;">
                                    icon-star
                                    <i class="icon-star-empty icon-1x"></i> 
                                </a>-->
            </div>
            <div class="b-info">
                <table>
                    <tbody>
                        <tr>
                            <td>
                                面积：<span><?php echo $project_info->area; ?></span>m²                            
                            </td>
                            <td>
                                户型：<span><?php echo $project_info->house_type; ?></span>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                装修预算：<span>￥<?php echo $project_info->decoration_budget; ?></span>
                            </td>
                            <td>
                                签约时间：<span><?php echo $project_info->contract_time; ?></span>
                            </td>
                        </tr>
                    </tbody>
                </table>
            </div>
            <div class="b-info" id="wrapper">
                <div id="scroller">
                    <ul class="ul_list">
                        <?php
                        foreach ($project_schedule_info as $k => $l) {
                            ?>

                            <li class="li_list">
                                <a href="#<?php echo $l->project_schedule_id; ?>">
                                    <img src="<?php
                                    if ($l->status != 0) {
                                        echo $l->begin_pic_add;
                                    } else {
                                        echo $l->not_begin_pic_add;
                                    }
                                    ?>">
                                    <br>
                                    <span><?php echo $l->schedule_name; ?></span>
                                </a>
                            </li>
                            <?php if ($k + 1 != count($project_schedule_info)) { ?>
                                <li class="li_div">
                                    --
                                </li>

                                <?php
                            }
                        }
                        ?>
                    </ul>    
                </div>
            </div>
            <div class="construction-wrapper">
                <?php foreach ($project_schedule_info as $k => $l) { ?>
                    <article id="<?php echo $l->project_schedule_id; ?>">
                        <h2>
                            <span class="t-tag"><?php echo $l->schedule_name; ?></span>
                            <a data-toggle="modal" data-target="#myModal" onclick="outwindow('<?php echo base64_encode($l->schedule_descript); ?>', '<?php echo $l->schedule_name; ?>')" style="color:#FF9800;font-size: 12px;">施工须知</a>
                            <a class="p-time" <?php
//                            if ($k != count($project_schedule_info) - 1) {
                            if ($l->_master_id != 21) {
                                echo "href='./index.php?r=master/person_center&master_id=" . $l->_master_id . "'";
                            } else {
                                echo "";
                            }
                            ?>>
                                   <?php
                                   $master_infos = master::model()->findByPk($l->_master_id);
                                   if (count($master_infos) != 0) {
                                       ?>
                                    <img src="<?php echo $master_infos->head_add; ?>" style="width:25px;height: 25px;border-radius: 13px;-webkit-border-radius: 13px;-moz-border-radius: 13px;float: left">
                                    <div style="float:left;line-height:2;width:60px;font-weight:100;">
                                        <?php echo $master_infos->master_name; ?>
                                    </div>
                                    <?php
                                } else {
                                    echo "";
                                }
                                ?>
                            </a>
                        </h2>
                        <div class="cd-timeline-content demo-gallery">
                            <?php if ($l->status != 0) { ?>

                                <ul class="clearfix small_pic">
                                    <li>
                                        <?php
                                        $project_schedule_picture_info = project_schedule_picture::model()->findAll(array("condition" => "isdeleted = 0 AND _project_schedule_id = " . $l->project_schedule_id));
                                        if (count($project_schedule_picture_info) != 0) {
                                            foreach ($project_schedule_picture_info as $m => $n) {
                                                ?>

                                                <a href="<?php echo $n->picture_water_address; ?>" data-size="1600x1600" data-med="<?php echo $n->picture_water_address; ?>" data-med-size="1024x1024" data-author="<?php echo $n->picture_descript; ?>" class="demo-gallery__img--main">
                                                    <img src="<?php echo $n->picture_water_address; ?>" alt="" />
                                                    <figure><?php echo $n->picture_name; ?></figure>
                                                </a>

                                                <?php
                                            }
                                        }
                                        ?>
                                    </li>
                                </ul>
                                <div>
        <!--                                    <span style="float:left;font-size: 12px;">
                                        <i class="icon-comment-alt icon-1x"></i> <span>18</span>
                                        icon-heart
                                        <i class="icon-heart-empty icon-1x"></i> <span>31</span>
                                    </span>-->
                                    <span class="p-time" style="font-size: 12px;"><?php echo $l->start_time; ?></span>
                                </div>
                                <?php
                            } else {
                                echo "还没有开始这个阶段哦";
                            }
                            ?> 
                            <!--icon-heart-empty-->
                        </div>
                    </article>
                <?php } ?>

                <!-- Root element of PhotoSwipe. Must have class pswp. -->
                <div class="pswp" tabindex="-1" role="dialog" aria-hidden="true">

                    <!-- Background of PhotoSwipe. 
                         It's a separate element, as animating opacity is faster than rgba(). -->
                    <div class="pswp__bg"></div>

                    <!-- Slides wrapper with overflow:hidden. -->
                    <div class="pswp__scroll-wrap">

                        <!-- Container that holds slides. PhotoSwipe keeps only 3 slides in DOM to save memory. -->
                        <!-- don't modify these 3 pswp__item elements, data is added later on. -->
                        <div class="pswp__container">
                            <div class="pswp__item"></div>
                            <div class="pswp__item"></div>
                            <div class="pswp__item"></div>
                        </div>

                        <!-- Default (PhotoSwipeUI_Default) interface on top of sliding area. Can be changed. -->
                        <div class="pswp__ui pswp__ui--hidden">

                            <div class="pswp__top-bar">

                                <!--  Controls are self-explanatory. Order can be changed. -->

                                <div class="pswp__counter"></div>

                                <button class="pswp__button pswp__button--close" title="Close (Esc)"></button>

                                <!--                                    <button class="pswp__button pswp__button--share" title="Share"></button>
                                
                                                                    <button class="pswp__button pswp__button--fs" title="Toggle fullscreen"></button>-->

                                <!--<button class="pswp__button pswp__button--zoom" title="Zoom in/out"></button>-->

                                <!-- Preloader demo http://codepen.io/dimsemenov/pen/yyBWoR -->
                                <!-- element will get class pswp__preloader--active when preloader is running -->
                                <div class="pswp__preloader">
                                    <div class="pswp__preloader__icn">
                                        <div class="pswp__preloader__cut">
                                            <div class="pswp__preloader__donut"></div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="pswp__share-modal pswp__share-modal--hidden pswp__single-tap">
                                <div class="pswp__share-tooltip"></div> 
                            </div>

                            <button class="pswp__button pswp__button--arrow--left" title="Previous (arrow left)">
                            </button>

                            <button class="pswp__button pswp__button--arrow--right" title="Next (arrow right)">
                            </button>

                            <div class="pswp__caption">
                                <div class="pswp__caption__center"></div>
                            </div>

                            <div class="pswp__descript">
                                <div class="pswp__caption__descript"></div>
                            </div>

                        </div>

                    </div>

                </div>
            </div>
        </div>


        <!-- 弹出窗Modal -->
        <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header" style="text-align:center">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title" id="myModalLabel"></h4>
                    </div>
                    <div class="modal-body" id="schedule_descript">
                        ...
                    </div>
                    <div class="modal-footer" style="text-align:center">
                        <button type="button" class="btn btn-info" data-dismiss="modal">关闭</button>
                        <!--<button type="button" class="btn btn-primary">Save changes</button>-->
                    </div>
                </div>
            </div>
        </div>


        <script type="text/javascript">
            function outwindow(schedule_descript, title) {
                var b = new Base64();

                schedule_descript = b.decode(schedule_descript);
                $("#schedule_descript").html(schedule_descript);
                $("#myModalLabel").text(title);
                $("#myModal").modal("show");
            }
            function Base64() {
                // private property
                _keyStr = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/=";

                // public method for encoding
                this.encode = function(input) {
                    var output = "";
                    var chr1, chr2, chr3, enc1, enc2, enc3, enc4;
                    var i = 0;
                    input = _utf8_encode(input);
                    while (i < input.length) {
                        chr1 = input.charCodeAt(i++);
                        chr2 = input.charCodeAt(i++);
                        chr3 = input.charCodeAt(i++);
                        enc1 = chr1 >> 2;
                        enc2 = ((chr1 & 3) << 4) | (chr2 >> 4);
                        enc3 = ((chr2 & 15) << 2) | (chr3 >> 6);
                        enc4 = chr3 & 63;
                        if (isNaN(chr2)) {
                            enc3 = enc4 = 64;
                        } else if (isNaN(chr3)) {
                            enc4 = 64;
                        }
                        output = output + _keyStr.charAt(enc1) + _keyStr.charAt(enc2) + _keyStr.charAt(enc3) + _keyStr.charAt(enc4);
                    }
                    return output;
                }

                // public method for decoding
                this.decode = function(input) {
                    var output = "";
                    var chr1, chr2, chr3;
                    var enc1, enc2, enc3, enc4;
                    var i = 0;
                    input = input.replace(/[^A-Za-z0-9\+\/\=]/g, "");
                    while (i < input.length) {
                        enc1 = _keyStr.indexOf(input.charAt(i++));
                        enc2 = _keyStr.indexOf(input.charAt(i++));
                        enc3 = _keyStr.indexOf(input.charAt(i++));
                        enc4 = _keyStr.indexOf(input.charAt(i++));
                        chr1 = (enc1 << 2) | (enc2 >> 4);
                        chr2 = ((enc2 & 15) << 4) | (enc3 >> 2);
                        chr3 = ((enc3 & 3) << 6) | enc4;
                        output = output + String.fromCharCode(chr1);
                        if (enc3 != 64) {
                            output = output + String.fromCharCode(chr2);
                        }
                        if (enc4 != 64) {
                            output = output + String.fromCharCode(chr3);
                        }
                    }
                    output = _utf8_decode(output);
                    return output;
                }

                // private method for UTF-8 encoding
                _utf8_encode = function(string) {
                    string = string.replace(/\r\n/g, "\n");
                    var utftext = "";
                    for (var n = 0; n < string.length; n++) {
                        var c = string.charCodeAt(n);
                        if (c < 128) {
                            utftext += String.fromCharCode(c);
                        } else if ((c > 127) && (c < 2048)) {
                            utftext += String.fromCharCode((c >> 6) | 192);
                            utftext += String.fromCharCode((c & 63) | 128);
                        } else {
                            utftext += String.fromCharCode((c >> 12) | 224);
                            utftext += String.fromCharCode(((c >> 6) & 63) | 128);
                            utftext += String.fromCharCode((c & 63) | 128);
                        }

                    }
                    return utftext;
                }

                // private method for UTF-8 decoding
                _utf8_decode = function(utftext) {
                    var string = "";
                    var i = 0;
                    var c = c1 = c2 = 0;
                    while (i < utftext.length) {
                        c = utftext.charCodeAt(i);
                        if (c < 128) {
                            string += String.fromCharCode(c);
                            i++;
                        } else if ((c > 191) && (c < 224)) {
                            c2 = utftext.charCodeAt(i + 1);
                            string += String.fromCharCode(((c & 31) << 6) | (c2 & 63));
                            i += 2;
                        } else {
                            c2 = utftext.charCodeAt(i + 1);
                            c3 = utftext.charCodeAt(i + 2);
                            string += String.fromCharCode(((c & 15) << 12) | ((c2 & 63) << 6) | (c3 & 63));
                            i += 3;
                        }
                    }
                    return string;
                }
            }
            (function() {
                var initPhotoSwipeFromDOM = function(gallerySelector) {
                    var parseThumbnailElements = function(el) {
                        var thumbElements = el.childNodes,
                                numNodes = thumbElements.length,
                                items = [],
                                el,
                                childElements,
                                thumbnailEl,
                                size,
                                item;

                        for (var i = 0; i < numNodes; i++) {
                            el = thumbElements[i];

                            // include only element nodes 
                            if (el.nodeType !== 1) {
                                continue;
                            }

                            childElements = el.children;

                            size = el.getAttribute('data-size').split('x');

                            // create slide object
                            item = {src: el.getAttribute('href'),
                                w: parseInt(size[0], 10),
                                h: parseInt(size[1], 10),
                                author: el.getAttribute('data-author')
                            };

                            item.el = el; // save link to element for getThumbBoundsFn

                            if (childElements.length > 0) {
                                item.msrc = childElements[0].getAttribute('src'); // thumbnail url
                                if (childElements.length > 1) {
                                    item.title = childElements[1].innerHTML; // caption (contents of figure)
                                }
                            }


                            var mediumSrc = el.getAttribute('data-med');
                            if (mediumSrc) {
                                size = el.getAttribute('data-med-size').split('x');
                                // "medium-sized" image
                                item.m = {src: mediumSrc,
                                    w: parseInt(size[0], 10),
                                    h: parseInt(size[1], 10)
                                };
                            }
                            // original image
                            item.o = {src: item.src,
                                w: item.w,
                                h: item.h
                            };

                            items.push(item);
                        }

                        return items;
                    };

                    // find nearest parent element
                    var closest = function closest(el, fn) {
                        return el && (fn(el) ? el : closest(el.parentNode, fn));
                    };

                    var onThumbnailsClick = function(e) {
                        e = e || window.event;
                        e.preventDefault ? e.preventDefault() : e.returnValue = false;

                        var eTarget = e.target || e.srcElement;

                        var clickedListItem = closest(eTarget, function(el) {
                            return el.tagName === 'A';
                        });

                        if (!clickedListItem) {
                            return;
                        }

                        var clickedGallery = clickedListItem.parentNode;

                        var childNodes = clickedListItem.parentNode.childNodes,
                                numChildNodes = childNodes.length,
                                nodeIndex = 0,
                                index;

                        for (var i = 0; i < numChildNodes; i++) {
                            if (childNodes[i].nodeType !== 1) {
                                continue;
                            }

                            if (childNodes[i] === clickedListItem) {
                                index = nodeIndex;
                                break;
                            }
                            nodeIndex++;
                        }

                        if (index >= 0) {
                            openPhotoSwipe(index, clickedGallery);
                        }
                        return false;
                    };

                    var photoswipeParseHash = function() {
                        var hash = window.location.hash.substring(1),
                                params = {};

                        if (hash.length < 5) { // pid=1
                            return params;
                        }

                        var vars = hash.split('&');
                        for (var i = 0; i < vars.length; i++) {
                            if (!vars[i]) {
                                continue;
                            }
                            var pair = vars[i].split('=');
                            if (pair.length < 2) {
                                continue;
                            }
                            params[pair[0]] = pair[1];
                        }

                        if (params.gid) {
                            params.gid = parseInt(params.gid, 10);
                        }

                        return params;
                    };

                    var openPhotoSwipe = function(index, galleryElement, disableAnimation, fromURL) {
                        var pswpElement = document.querySelectorAll('.pswp')[0],
                                gallery,
                                options,
                                items;

                        items = parseThumbnailElements(galleryElement);

                        // define options (if needed)
                        options = {galleryUID: galleryElement.getAttribute('data-pswp-uid'),
                            getThumbBoundsFn: function(index) {                                 // See Options->getThumbBoundsFn section of docs for more info
                                var thumbnail = items[index].el.children[0],
                                        pageYScroll = window.pageYOffset || document.documentElement.scrollTop,
                                        rect = thumbnail.getBoundingClientRect();
                                return {x: rect.left, y: rect.top + pageYScroll, w: rect.width};
                            },
                            addCaptionHTMLFn: function(item, captionEl, isFake) {
                                if (!item.title) {
                                    captionEl.children[0].innerText = '';
                                    return false;
                                }
                                captionEl.children[0].innerHTML = '名称: ' + item.title;
                                $(".pswp__caption__descript").html('<small>描述: ' + item.author + '</small>');
                                return true;
                            }

                        };


                        if (fromURL) {
                            if (options.galleryPIDs) {                                 // parse real index when custom PIDs are used 
                                // http://photoswipe.com/documentation/faq.html#custom-pid-in-url
                                for (var j = 0; j < items.length; j++) {
                                    if (items[j].pid == index) {
                                        options.index = j;
                                        break;
                                    }
                                }
                            } else {
                                options.index = parseInt(index, 10) - 1;
                            }
                        } else {
                            options.index = parseInt(index, 10);
                        }

                        // exit if index not found
                        if (isNaN(options.index)) {
                            return;
                        }



                        var radios = document.getElementsByName('gallery-style');
                        for (var i = 0, length = radios.length; i < length; i++) {
                            if (radios[i].checked) {
                                if (radios[i].id == 'radio-all-controls') {
                                } else if (radios[i].id == 'radio-minimal-black') {
                                    options.mainClass = 'pswp--minimal--dark';
                                    options.barsSize = {top: 0, bottom: 0};
                                    options.captionEl = false;
                                    options.fullscreenEl = false;
                                    options.shareEl = false;
                                    options.bgOpacity = 0.85;
                                    options.tapToClose = true;
                                    options.tapToToggleControls = false;
                                }
                                break;
                            }
                        }

                        if (disableAnimation) {
                            options.showAnimationDuration = 0;
                        }

                        // Pass data to PhotoSwipe and initialize it
                        gallery = new PhotoSwipe(pswpElement, PhotoSwipeUI_Default, items, options);

                        // see: http://photoswipe.com/documentation/responsive-images.html
                        var realViewportWidth,
                                useLargeImages = false,
                                firstResize = true,
                                imageSrcWillChange;

                        gallery.listen('beforeResize', function() {
                            var dpiRatio = window.devicePixelRatio ? window.devicePixelRatio : 1;
                            dpiRatio = Math.min(dpiRatio, 2.5);
                            realViewportWidth = gallery.viewportSize.x * dpiRatio;


                            if (realViewportWidth >= 1200 || (!gallery.likelyTouchDevice && realViewportWidth > 800) || screen.width > 1200) {
                                if (!useLargeImages) {
                                    useLargeImages = true;
                                    imageSrcWillChange = true;
                                }

                            } else {
                                if (useLargeImages) {
                                    useLargeImages = false;
                                    imageSrcWillChange = true;
                                }
                            }

                            if (imageSrcWillChange && !firstResize) {
                                gallery.invalidateCurrItems();
                            }

                            if (firstResize) {
                                firstResize = false;
                            }

                            imageSrcWillChange = false;

                        });

                        gallery.listen('gettingData', function(index, item) {
                            if (useLargeImages) {
                                item.src = item.o.src;
                                item.w = item.o.w;
                                item.h = item.o.h;
                            } else {
                                item.src = item.m.src;
                                item.w = item.m.w;
                                item.h = item.m.h;
                            }
                        });

                        gallery.init();
                    };

                    // select all gallery elements
                    var galleryElements = document.querySelectorAll(gallerySelector);
                    for (var i = 0, l = galleryElements.length; i < l; i++) {
                        galleryElements[i].setAttribute('data-pswp-uid', i + 1);
                        galleryElements[i].onclick = onThumbnailsClick;
                    }

                    // Parse URL and open gallery if it contains #&pid=3&gid=1
                    var hashData = photoswipeParseHash();
                    if (hashData.pid && hashData.gid) {
                        openPhotoSwipe(hashData.pid, galleryElements[ hashData.gid - 1 ], true, true);
                    }
                };

                initPhotoSwipeFromDOM('.demo-gallery');

            })();
        </script>
        <script type="text/javascript">

            (function() {
                var myScroll = new IScroll('#wrapper', {eventPassthrough: true, scrollX: true, scrollY: false, preventDefault: false});
            })();
            //            var myScroll;
            //            function loaded() {
            //                myScroll = new IScroll('#wrapper', {eventPassthrough: true, scrollX: true, scrollY: false, preventDefault: false});
            //            }

        </script>
    </body>
</html>