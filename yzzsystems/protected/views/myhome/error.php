<!DOCTYPE html>
<html>
    <head>
        <meta content="IE=11.0000" http-equiv="X-Ua-Compatible">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">

        <meta http-equiv="Content-Type" content="text/html; charset=utf-8"> 
        <TITLE>错误页</TITLE>   
        <link rel="stylesheet" type="text/css" href="./bootstrap/css/bootstrap.min.css"/>
        <link rel="stylesheet" href="./frontend/Font-Awesome/css/font-awesome.min.css">
        <style>
            img{width: 140px;}
            .btn{width: 50%;}
        </style>
        <script src="./frontend/login_js/jquery-1.9.1.min.js" type="text/javascript"></script>    
        <script>
            $(document).ready(function() {
                $("#refresh").click(function() {
                    window.location.href = "./index.php?r=Myhome/index&openid=<?php echo Yii::app()->session['openid']; ?>";
                });
            });
        </script>
    </head>

    <body>
        <div class="wrap-fluid">
            <div class="container-fluid paper-wrap bevel tlbr">
                <!-- CONTENT -->

                <div class="content-wrap">
                    <div class="row">
                        <div class="col-lg-12">
                            <form class="form-horizontal" style="text-align:center;">
                                <div class="form-group">
                                    <div class="col-sm-3">

                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="col-sm-offset-4 col-sm-3">
                                        <img src="./css/error.png">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="col-sm-offset-4 col-sm-3">
                                        <h4>hi,<?php echo $error_info; ?>！</h4>
                                    </div>   
                                </div>
                                <div class="form-group">
                                    <div class="col-sm-offset-4 col-sm-3">
                                        <button type="button" class="btn btn-info" id="refresh"><i class="icon-refresh"></i> &nbsp;刷新试试</button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>

                <!--  / DEVICE MANAGER -->
            </div>
        </div>
    </body>
</html>
