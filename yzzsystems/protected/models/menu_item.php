<?php
class menu_item extends CActiveRecord
{
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }
    public function tableName()
    {
        return '{{menu_item}}';
    }
}
